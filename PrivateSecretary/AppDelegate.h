//
//  AppDelegate.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/7/29.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ChatViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

-(ChatViewController *)getChatVC;

+(AppDelegate *)getDelegate;

@end


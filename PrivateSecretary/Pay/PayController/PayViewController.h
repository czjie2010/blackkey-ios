//
//  PayViewController.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/20.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SuperViewController.h"


@class ChatOrderModel;

@interface PayViewController : SuperViewController
{
    @public
    NSDictionary *s_chatOrderDict;
}

-(void)showAlertMessage:(NSString*)msg;
@end

//
//  PayViewController.m
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/20.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import "PayViewController.h"
#import "INNOTableView.h"
#import "Pingpp.h"
#import "ChatOrderModel.h"
#import "JSONKit.h"


#define KBtn_width        200
#define KBtn_height       40
#define KXOffSet          (self.view.frame.size.width - KBtn_width) / 2
#define KYOffSet          20

#define kWaiting          @"正在获取支付凭据,请稍后..."
#define kNote             @"提示"
#define kConfirm          @"确定"
#define kErrorNet         @"网络错误"
#define kResult           @"支付结果：%@"

#define kPlaceHolder      @"支付金额"
#define kMaxAmount        9999999

#define kUrlScheme      @"PrivateSecretaryZhanghao" // 这个是你定义的 URL Scheme，支付宝、微信支付和测试模式需要。
#define kUrl            @"http://42.121.145.76:9998/pay/api/get_pingpp_order/" // 你的服务端创建并返回 charge 的 URL 地址，此地址仅供测试用。


@interface PayViewController ()<UITableViewDataSource,UITableViewDelegate,INNOTableViewToDownRefreshDelegate>
{
    INNOTableView       *m_tabel;
    NSMutableArray      *m_listArray;
    NSString            *m_channel;
    UIAlertView         *m_alert;
}

@end

@implementation PayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    m_tabel = [[INNOTableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, [self.view getViewHigh]) style:UITableViewStylePlain adelegate:self topRefresh:NO bottomRefresh:NO];
    m_tabel.backgroundColor = VCBackgroundColor;
    //    m_tabel.separatorStyle = UITableViewCellSeparatorStyleNone;
    m_tabel.dataSource = self;
    m_tabel.delegate = self;
    [self.view addSubview:m_tabel];
    
    m_listArray = [NSMutableArray arrayWithObjects:@"支付宝",@"微信支付",@"银联支付", nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return m_listArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *ReuCell = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ReuCell];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ReuCell];
    }
    cell.textLabel.text = [m_listArray objectAtIndex:indexPath.row];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:
            m_channel = @"wx";
            break;
        case 1:
            m_channel = @"alipay";
            break;
        case 2:
            m_channel = @"upacp";
            break;
            
        default:
            return;
            break;
    }
    
//    long long amount = [[self.mTextField.text stringByReplacingOccurrencesOfString:@"." withString:@""] longLongValue];
//    if (amount == 0) {
//        return;
//    }
//    NSString *amountStr = [s_chatOrderDict objectForKey:@"amount"];
//    NSURL* url = [NSURL URLWithString:kUrl];
//    NSMutableURLRequest * postRequest=[NSMutableURLRequest requestWithURL:url];
//    
//    NSDictionary* dict = @{
//                           @"channel" : m_channel,
//                           @"amount"  : amountStr
//                           };
//    NSData* data = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
//    NSString *bodyData = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//
//    [postRequest setHTTPBody:[NSData dataWithBytes:[bodyData UTF8String] length:strlen([bodyData UTF8String])]];
//    [postRequest setHTTPMethod:@"POST"];
//    [postRequest setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    PayViewController * __weak weakSelf = self;
//    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
//    [self showAlertWait];
//    [NSURLConnection sendAsynchronousRequest:postRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
//        NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
//        [weakSelf hideAlert];
//        if (httpResponse.statusCode != 200) {
//            [weakSelf showAlertMessage:kErrorNet];
//            return;
//        }
//        if (connectionError != nil) {
//            INNLog(@"error = %@", connectionError);
//            [weakSelf showAlertMessage:kErrorNet];
//            return;
//        }
//        NSString* charge = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSString    *charge = [NSString stringWithFormat:@"%@",[s_chatOrderDict objectForKey:@"charge"]];
  charge = [charge stringByReplacingOccurrencesOfString:@"\n" withString:@""];
//    @"{\"order_no\": \"nPePq5n18yzHnLSq1111\", \"extra\": {}, \"app\": \"app_nfHO8SazXrTGzfDu\",\"livemode\": false, \"currency\":\"cny\", \"time_settle\": null, \"time_expire\": 1441967302, \"id\": \"ch_bjbjf5SSafXDW1mDaPW1q9aH\", \"subject\": \"Your Subject\", \"failure_msg\": null, \"channel\": \"alipay\", \"metadata\": {}, \"body\": \"\u4e03\u5915\u9c9c\u82b1\", \"credential\": {\"alipay\": {\"orderInfo\": \"_input_charset=\"utf-8\"&body=\"\u4e03\u5915\u9c9c\u82b1\"&it_b_pay=\"1440m\"&notify_url=\"https%3A%2F%2Fapi.pingxx.com%2Fnotify%2Fcharges%2Fch_bjbjf5SSafXDW1mDaPW1q9aH\"&out_trade_no=\"nPePq5n18yzHnLSq1111\"&partner=\"2008553778607066\"&payment_type=\"1\"&seller_id=\"2008553778607066\"&service=\"mobile.securitypay.pay\"&subject=\"Your Subject\"&total_fee=\"0.1\"&sign=\"MXF2alBTZnJYUHFUOEc0bTVLNWliSEc0\"&sign_type=\"RSA\"\"}, \"object\": \"credential\"}, \"client_ip\": \"127.0.0.1\", \"description\": \"\u5907\u6ce8\u4e03\u5915\u9c9c\u82b1\", \"amount_refunded\": 0, \"refunded\": false, \"object\": \"charge\", \"paid\": false, \"amount_settle\": 0, \"time_paid\": null, \"failure_code\": null, \"refunds\": {\"url\": \"/v1/charges/ch_bjbjf5SSafXDW1mDaPW1q9aH/refunds\", \"has_more\": false, \"object\": \"list\", \"data\": []}, \"created\": 1441880902, \"transaction_no\": null, \"amount\": 10}";
        NSLog(@"charge = %@", charge);
        dispatch_async(dispatch_get_main_queue(), ^{
            [Pingpp createPayment:charge viewController:weakSelf appURLScheme:kUrlScheme withCompletion:^(NSString *result, PingppError *error) {
                NSLog(@"completion block: %@", result);
                if (error == nil) {
                    INNLog(@"PingppError is nil");
                } else {
                    INNLog(@"PingppError: code=%lu msg=%@", (unsigned  long)error.code, [error getMsg]);
                }
                [weakSelf showAlertMessage:result];
            }];
        });
//    }];
}


- (void)showAlertWait
{
    m_alert = [[UIAlertView alloc] initWithTitle:kWaiting message:nil delegate:self cancelButtonTitle:nil otherButtonTitles: nil];
    [m_alert show];
    UIActivityIndicatorView* aiv = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    aiv.center = CGPointMake(m_alert.frame.size.width / 2.0f - 15, m_alert.frame.size.height / 2.0f + 10 );
    [aiv startAnimating];
    [m_alert addSubview:aiv];
}

- (void)showAlertMessage:(NSString*)msg
{
    m_alert = [[UIAlertView alloc] initWithTitle:kNote message:msg delegate:nil cancelButtonTitle:kConfirm otherButtonTitles:nil, nil];
    [m_alert show];
}

- (void)hideAlert
{
    if (m_alert != nil)
    {
        [m_alert dismissWithClickedButtonIndex:0 animated:YES];
        m_alert = nil;
    }
}


#pragma mark - INNOTableViewToDownRefreshDelegate
- (void)reloadTableViewDataSource{
    
}

@end

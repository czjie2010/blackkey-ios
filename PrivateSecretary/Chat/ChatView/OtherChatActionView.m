//
//  OtherChatActionView.m
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/13.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import "OtherChatActionView.h"
#import "LoadViewFromXib.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <MobileCoreServices/MobileCoreServices.h>

@interface OtherChatActionView()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

-(IBAction)openPhotoLibrary:(id)sender;
-(IBAction)openCamera:(id)sender;

@end

@implementation OtherChatActionView

-(id)init{
    if (self = [super init]) {
        self = (OtherChatActionView *)[LoadViewFromXib loadView:@"OtherChatActionView" theClass:[OtherChatActionView class] owner:nil];
        self.backgroundColor = VCBackgroundColor;
    }
    return self;
}

-(void)showOtherChatActionView:(UIViewController *)vc
{
    
}

#pragma -mark - IBAction
-(void)openPhotoLibrary:(id)sender
{
    if ([self isPhotoLibraryAvailable]) {
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        NSMutableArray *mediaTypes = [[NSMutableArray alloc] init];
        [mediaTypes addObject:(__bridge NSString *)kUTTypeImage];
        controller.mediaTypes = mediaTypes;
        controller.delegate = self;
        [m_superVC presentViewController:controller
                           animated:YES
                         completion:^(void){
                             INNLog(@"Picker View Controller is presented");
                         }]; 
    }

}

-(void)openCamera:(id)sender
{
    if ([self isCameraAvailable] && [self doesCameraSupportTakingPhotos]) {
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.sourceType = UIImagePickerControllerSourceTypeCamera;
        if ([self isFrontCameraAvailable]) {
            controller.cameraDevice = UIImagePickerControllerCameraDeviceRear;
        }
        NSMutableArray *mediaTypes = [[NSMutableArray alloc] init];
        [mediaTypes addObject:(__bridge NSString *)kUTTypeImage];
        controller.mediaTypes = mediaTypes;
        controller.delegate = self;
        [m_superVC presentViewController:controller
                           animated:YES
                         completion:^(void){
                             INNLog(@"Picker View Controller is presented");
                         }];
    }

}

#pragma mark - UIImagePickerControllerDelegate
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    [picker dismissViewControllerAnimated:YES completion:^() {
        UIImage *portraitImg = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        INNLog(@"UIImagePickerControllerDelegate info = %@",info);
        if ([_m_delegate respondsToSelector:@selector(sendImage:)]) {
            [_m_delegate sendImage:portraitImg];
        }
    }];

}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    //TODO:取消
     [picker dismissViewControllerAnimated:YES completion:^() {
     }];
}

#pragma mark camera utility
- (BOOL) isCameraAvailable{
    return [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
}

- (BOOL) isFrontCameraAvailable {
    return [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront];
}

- (BOOL) doesCameraSupportTakingPhotos {
    return [self cameraSupportsMedia:(__bridge NSString *)kUTTypeImage sourceType:UIImagePickerControllerSourceTypeCamera];
}

- (BOOL) isPhotoLibraryAvailable{
    return [UIImagePickerController isSourceTypeAvailable:
            UIImagePickerControllerSourceTypePhotoLibrary];
}

- (BOOL) cameraSupportsMedia:(NSString *)paramMediaType sourceType:(UIImagePickerControllerSourceType)paramSourceType{
    __block BOOL result = NO;
    if ([paramMediaType length] == 0) {
        return NO;
    }
    NSArray *availableMediaTypes = [UIImagePickerController availableMediaTypesForSourceType:paramSourceType];
    [availableMediaTypes enumerateObjectsUsingBlock: ^(id obj, NSUInteger idx, BOOL *stop) {
        NSString *mediaType = (NSString *)obj;
        if ([mediaType isEqualToString:paramMediaType]){
            result = YES;
            *stop= YES;
        }
    }];
    return result;
}

-(void)dealloc
{
    m_superVC = nil;
    _m_delegate = nil;
}

@end

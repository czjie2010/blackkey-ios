//
//  ChatCellDelegate.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/25.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ChatCellDelegate <NSObject>

@optional
-(void)cellUpdateImageFailedAfreshSend:(NSIndexPath *)index;
-(void)cellUpdateMessageSuccess:(NSIndexPath *)index;
-(void)cellUpdateMessageFail:(NSIndexPath *)index;
-(void)cellUpdateMessageUserCancel:(NSIndexPath *)index;
-(void)cellOrderSuccessIndx:(NSIndexPath *)index;
@end

//
//  ChatMyImageCell.m
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/13.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import "ChatMyImageCell.h"
#import "ChatFriendsAndMineModel.h"
#import "SaveFile.h"
#import "ShowBigImageViewController.h"
#import "UIImage+Scale.h"

@interface ChatMyImageCell()<UIAlertViewDelegate>
{
    IBOutlet    UIButton    *m_head;
    IBOutlet    UIButton    *m_imageMessage;
    IBOutlet    UIImageView *m_chatImageBg;
    IBOutlet    UILabel     *m_progressLabel;
    IBOutlet    UIImageView     *m_progressImagebg;
    IBOutlet    UIButton        *m_updateFailed;
    IBOutlet    NSLayoutConstraint      *m_imageMessage_width;
    IBOutlet    NSLayoutConstraint      *m_imageMessage_height;
    IBOutlet    NSLayoutConstraint      *m_imageMessageBg_width;
    SaveFile    *m_saveFile;
    
    UIAlertView     *m_alertView;
}

-(IBAction)showBigChatImage:(id)sender;
-(IBAction)aFreshSendChatImage:(id)sender;
@end

@implementation ChatMyImageCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setProperty:(ChatFriendsAndMineModel *)model
{
    m_FMmodel = model;
    self.contentView.backgroundColor = VCBackgroundColor;
    if (!m_saveFile) {
        m_saveFile = [[SaveFile alloc]init];
    }
    
    /***
     *  图片大小 动态计算
     ****/

    [m_imageMessage setImage:model->m_chatImage forState:UIControlStateNormal];
//    [m_imageMessage.imageView setContentMode:UIViewContentModeScaleAspectFill];

    m_imageMessage_width.constant = model->message_size.width;
    m_imageMessage_height.constant = model->message_size.height;
    m_imageMessageBg_width.constant =  model->message_size.width +5;
    [m_imageMessage setNeedsUpdateConstraints];
    [m_chatImageBg setNeedsUpdateConstraints];
    
    m_chatImageBg.image = [m_chatImageBg.image stretchableImageWithLeftCapWidth:30 topCapHeight:21];
    m_progressImagebg.image = [m_chatImageBg.image stretchableImageWithLeftCapWidth:30 topCapHeight:21];

    /***
     *  图片气泡
     ****/
    CALayer *layer = m_chatImageBg.layer;
    layer.frame = (CGRect){{0,0},model->message_size};
    m_imageMessage.layer.mask = layer;
    [m_imageMessage setNeedsDisplay];


//    m_progressLabel.hidden = YES;
//    m_progressImagebg.hidden = YES;
    m_updateFailed.hidden = YES;
}

-(void)updateImageSendProgress:(float)progress
{
    int progressInt = progress*100;
    m_progressLabel.text = [NSString stringWithFormat:@"%d%%",progressInt];
    if (progressInt == 100) {
        m_progressLabel.hidden = YES;
        m_progressImagebg.hidden = YES;
    }else{
        m_progressLabel.hidden = NO;
        m_progressImagebg.hidden = NO;
    }
}

-(void)updateImageSendFail:(NSDictionary *)dict
{
    m_updateFailed.hidden = NO;
    m_progressLabel.hidden = YES;
    m_progressImagebg.hidden = YES;
}

-(void)updateImageSendSuccss:(NSDictionary *)dict
{
    m_updateFailed.hidden = YES;
}

#pragma mark - IBAction
-(void)showBigChatImage:(id)sender
{
    ShowBigImageViewController *vc = [[ShowBigImageViewController alloc]initWithNibName:@"ShowBigImageViewController" bundle:nil];
    vc->m_model = m_FMmodel;
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    vc.view.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
    [m_superVC presentViewController:vc animated:YES completion:^{
        
    }];
}

-(void)aFreshSendChatImage:(id)sender{
    if (!m_alertView) {
        m_alertView = [[UIAlertView alloc]initWithTitle:@"" message:@"发送失败，重新发送" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    }
    [m_alertView show];
   
}

#pragma mark - UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        if ([_m_delegate respondsToSelector:@selector(cellUpdateImageFailedAfreshSend:)]) {
            [_m_delegate cellUpdateImageFailedAfreshSend:m_index];
        }
    }
}

-(void)dealloc{
    _m_delegate = nil;
    m_superVC = nil;
}

@end

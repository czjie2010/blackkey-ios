//
//  ChatTimeCell.m
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/5.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import "ChatTimeCell.h"
#import "ChatFriendsAndMineModel.h"
#import "TimeTool.h"
#import "CalculateViewHeight.h"

@interface ChatTimeCell()
{
    IBOutlet UILabel        *m_time;
    IBOutlet    UIImageView     *m_timeBg;
    IBOutlet    NSLayoutConstraint      *m_timeBGWidth;
}

@end

@implementation ChatTimeCell

- (void)awakeFromNib {
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)setProperty:(ChatFriendsAndMineModel *)model{
    self.contentView.backgroundColor = VCBackgroundColor;
    m_time.text = [TimeTool time:model->m_created_time];
    CGSize size = [CalculateViewHeight striSizeHeight:m_time.text withFont:m_time.font maxWith:kScreenWidth];
    m_timeBGWidth.constant = size.width +20;
    m_timeBg.image = [m_timeBg.image stretchableImageWithLeftCapWidth:8 topCapHeight:8];
}

@end

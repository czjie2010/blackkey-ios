//
//  ChatFriendsCell.m
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/5.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import "ChatFriendsCell.h"
#import "ChatFriendsAndMineModel.h"
#import "UIButton+RoundButton.h"

@interface ChatFriendsCell()
{
    IBOutlet UILabel        *m_chatText;
    IBOutlet UIImageView    *m_chatBgImage;
    IBOutlet UIButton       *m_head;
    IBOutlet UIView         *m_bgView;
    IBOutlet    UILabel     *m_name;
}

@end

@implementation ChatFriendsCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma -mark - 自定义方法

#pragma -mark   数据处理
-(void)setProperty:(ChatFriendsAndMineModel *)model{
    self.contentView.backgroundColor = VCBackgroundColor;
    
    m_name.text = model->m_friends_name;
    m_chatText.text = model->m_my_message;
    
    m_chatText.frame = CGRectMake([m_chatText getViewX], [m_chatText getViewY], model->message_size.width+1, model->message_size.height+1);
    
    m_chatBgImage.frame  = CGRectMake([m_chatBgImage getViewX], [m_chatBgImage getViewY], model->message_size.width+30, model->m_textBackHeight);
    
    m_chatBgImage.image = [UIImage imageNamed:@"FreindsChatbgImage"];
    m_chatBgImage.image = [m_chatBgImage.image stretchableImageWithLeftCapWidth:26 topCapHeight:26];
    
    [m_head setRoundButton];
    [m_head setRoundButtonImage:model->m_avatar];
}


@end

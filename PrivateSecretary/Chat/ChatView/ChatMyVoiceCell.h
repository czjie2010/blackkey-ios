//
//  ChatMyVoiceCell.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/12.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatCellDelegate.h"

@class  ChatFriendsAndMineModel;
@class RecordingPlay;

@interface ChatMyVoiceCell : UITableViewCell
{
    @public
    RecordingPlay   *m_recordingPlay;
    NSIndexPath     *m_index;
}

@property(nonatomic,assign)id<ChatCellDelegate> m_delegate;
-(void)setProperty:(ChatFriendsAndMineModel *)model;

-(void)updateImageSendFail:(NSDictionary *)dict;
-(void)updateImageSendSuccss:(NSDictionary *)dict;

@end

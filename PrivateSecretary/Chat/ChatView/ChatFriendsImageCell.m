//
//  ChatFriendsImageCell.m
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/14.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import "ChatFriendsImageCell.h"
#import "ChatFriendsAndMineModel.h"
#import "SaveFile.h"
#import "ShowBigImageViewController.h"
#import "UIImage+Scale.h"
#import "UIButton+RoundButton.h"
#import "UIButton+WebCache.h"


@interface ChatFriendsImageCell()
{
    IBOutlet    UIButton    *m_head;
    IBOutlet    UIButton    *m_imageMessage;
    IBOutlet    UIImageView *m_chatImageBg;
    IBOutlet    UILabel     *m_progressLabel;
    IBOutlet    UIImageView     *m_progressImagebg;
    IBOutlet    NSLayoutConstraint      *m_imageMessage_width;
    IBOutlet    NSLayoutConstraint      *m_imageMessage_height;
    IBOutlet    NSLayoutConstraint      *m_imageMessageBg_width;
    
    IBOutlet    UILabel     *m_name;
    SaveFile    *m_saveFile;
    
}

-(IBAction)showBigChatImage:(id)sender;

@end

@implementation ChatFriendsImageCell 

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)setProperty:(ChatFriendsAndMineModel *)model
{
    m_FMmodel = model;
    self.contentView.backgroundColor = VCBackgroundColor;
    if (!m_saveFile) {
        m_saveFile = [[SaveFile alloc]init];
    }
    
    /***
     *  图片大小 动态计算
     ****/
    
    [m_head setRoundButton];
    [m_head setRoundButtonImage:model->m_avatar];
    
    
    m_name.text = model->m_friends_name;
    [m_imageMessage sd_setImageWithURL:[NSURL URLWithString:model->m_my_message]  forState:UIControlStateNormal placeholderImage:nil];
    
    m_imageMessage_width.constant = model->message_size.width;
    m_imageMessage_height.constant = model->message_size.height;
    m_imageMessageBg_width.constant =  model->message_size.width+5;
    [m_imageMessage setNeedsUpdateConstraints];
    [m_chatImageBg setNeedsUpdateConstraints];
    
    m_chatImageBg.image = [m_chatImageBg.image stretchableImageWithLeftCapWidth:26 topCapHeight:26];
    m_progressImagebg.image = [m_chatImageBg.image stretchableImageWithLeftCapWidth:26 topCapHeight:26];
    
    /***
     *  图片气泡
     ****/
    CALayer *layer = m_chatImageBg.layer;
    layer.frame = (CGRect){{0,0},model->message_size};
    m_imageMessage.layer.mask = layer;
    [m_imageMessage setNeedsDisplay];
    
    
    m_progressLabel.hidden = YES;
    m_progressImagebg.hidden = YES;
}

-(void)updateImageSendProgress:(float)progress
{
    int progressInt = progress*100;
    m_progressLabel.text = [NSString stringWithFormat:@"%d%%",progressInt];
    if (progressInt == 100) {
        m_progressLabel.hidden = YES;
        m_progressImagebg.hidden = YES;
    }else{
        m_progressLabel.hidden = NO;
        m_progressImagebg.hidden = NO;
    }
}

#pragma mark - IBAction
-(void)showBigChatImage:(id)sender
{
    ShowBigImageViewController *vc = [[ShowBigImageViewController alloc]initWithNibName:@"ShowBigImageViewController" bundle:nil];
    vc->m_model = m_FMmodel;
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    vc.view.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
    [m_superVC presentViewController:vc animated:YES completion:^{
        
    }];
}

-(void)dealloc{
    m_superVC = nil;
}

@end

//
//  ChatTimeCell.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/5.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ChatFriendsAndMineModel;

@interface ChatTimeCell : UITableViewCell


-(void)setProperty:(ChatFriendsAndMineModel *)model;

@end

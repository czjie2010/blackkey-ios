//
//  ChatInPutStatusCell.m
//  PrivateSecretary
//
//  Created by 田沙 on 15/9/14.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import "ChatInPutStatusCell.h"
#import "UIButton+RoundButton.h"

@interface ChatInPutStatusCell()
{
    IBOutlet    UIButton    *s_headButton;
    IBOutlet    UIImageView     *s_chatImageBg;
    IBOutlet    UIImageView     *s_chatInputStatus;
}

@end

@implementation ChatInPutStatusCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setInputStatus
{
    self.contentView.backgroundColor = VCBackgroundColor;
    
    [s_headButton setRoundButton];
    [s_headButton setRoundButtonImage:s_avatar];
    
    s_chatImageBg.image = [s_chatImageBg.image stretchableImageWithLeftCapWidth:26 topCapHeight:25];
    s_chatInputStatus.image = [UIImage animatedImageNamed:@"chatInPut" duration:1.0];
}

@end

//
//  ChatMyCell.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/5.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatCellDelegate.h"


@class ChatViewController;
@class ChatFriendsAndMineModel;

@interface ChatMyCell : UITableViewCell
{
    @public
    NSIndexPath *m_indexPath;
    ChatViewController *m_chatVC;
}

@property(nonatomic,assign) id<ChatCellDelegate>m_delegate;


-(void)setPropertyChatMyCellData:(ChatFriendsAndMineModel *)aModel;
-(void)setShowActivityIndicatorView:(BOOL)flag;
-(void)sendMessageToSevice:(NSString *)content;
-(void)setSendMessgeStatus:(BOOL)flag;
@end

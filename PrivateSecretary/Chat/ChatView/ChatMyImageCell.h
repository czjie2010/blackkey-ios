//
//  ChatMyImageCell.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/13.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatCellDelegate.h"

@class ChatFriendsAndMineModel;

@interface ChatMyImageCell : UITableViewCell
{
    @public
    UIViewController    *m_superVC;
    NSIndexPath         *m_index;
    ChatFriendsAndMineModel *m_FMmodel;
}

@property(nonatomic,assign) id<ChatCellDelegate>m_delegate;

-(void)setProperty:(ChatFriendsAndMineModel *)model;

-(void)updateImageSendProgress:(float)progress;
-(void)updateImageSendFail:(NSDictionary *)dict;
-(void)updateImageSendSuccss:(NSDictionary *)dict;

@end

//
//  ChatInPutStatusCell.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/9/14.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatInPutStatusCell : UITableViewCell
{
  @public
    NSString    *s_avatar;
}

-(void)setInputStatus;

@end

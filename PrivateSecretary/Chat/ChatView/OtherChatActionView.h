//
//  OtherChatActionView.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/13.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OtherChatActionViewDelegate <NSObject>

@optional
-(void)sendImage:(UIImage *)image;

@end

@interface OtherChatActionView : UIView
{
    @public
    UIViewController    *m_superVC;
}

@property (nonatomic ,weak)id<OtherChatActionViewDelegate>m_delegate;

-(void)showOtherChatActionView:(UIViewController *)vc;

@end

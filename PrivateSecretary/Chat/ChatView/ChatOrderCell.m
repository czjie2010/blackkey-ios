//
//  ChatOrderCell.m
//  PrivateSecretary
//
//  Created by 田沙 on 15/9/2.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import "ChatOrderCell.h"
#import "ChatOrderModel.h"
#import "PayViewController.h"
#import "CustomNavigationControllerViewController.h"
#import "Pingpp.h"
#import "JSONKit.h"
#import "DefineKeyPing++.h"
#import "NSString+NULL.h"

@interface ChatOrderCell()
{
    IBOutlet    UILabel     *m_order_timeLabel;
    IBOutlet    UILabel     *m_contentLabel;
    IBOutlet    UILabel     *m_remarkLabel;
    IBOutlet    UILabel     *m_priceLabel;
    IBOutlet    UIImageView     *m_order_bgImageView;
    IBOutlet    UIImageView     *m_order_iconImageView;
    IBOutlet    UIImageView     *m_order_roundImageView;
    
    IBOutlet    UIImageView        *m_payImageView;
    
    IBOutlet    UIButton        *m_payButton;
    IBOutlet    UILabel         *m_typeTagLabel;
    
    IBOutlet    UIView         *m_orderIdView;
    IBOutlet    UILabel         *m_orderIdLabel;
    IBOutlet    UIView          *m_orderDetailView;
}

-(IBAction)payOrder:(id)sender;

@end

@implementation ChatOrderCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)setProperty:(ChatOrderModel *)model
{

    self.contentView.backgroundColor = VCBackgroundColor;
    
    m_order_bgImageView.image = [m_order_bgImageView.image stretchableImageWithLeftCapWidth:30 topCapHeight:30];
    
    
//    m_payImageView.image = [m_payImageView.image stretchableImageWithLeftCapWidth:50 topCapHeight:50];
//    m_payImageView.image = [UIImage imageNamed:@"waiting_pay"];
   
    @try {
        m_order_timeLabel.text = model->m_time;
        m_contentLabel.text = model->m_content;
        m_priceLabel.text = model->m_price;
        m_remarkLabel.text = model->m_remark;
        
//        model->m_payStatus = @"1";
        m_orderIdView.hidden = YES;
        m_payButton.hidden = NO;
        m_payButton.tag =[model->m_payStatus intValue];
       
        if ([model->m_payStatus intValue] == 0)
        {
            //等待付款
             [m_payButton setBackgroundImage:[UIImage imageNamed:@"waiting_pay"] forState:UIControlStateNormal];
            [m_payButton setTitle:@"等待支付" forState:UIControlStateNormal];
             [m_payButton setTitleColor:WaitingOrderTextColor forState:UIControlStateNormal];
            m_order_iconImageView.image = [UIImage imageNamed:@"chat_order_icon"];
            m_typeTagLabel.text = @"通用订单";
            
            CGRect r = m_order_bgImageView.frame;
            r.size.height = 250;
            m_order_bgImageView.frame = r;
            
        }else if([model->m_payStatus intValue] == 1)
        {
            //完成付款
           [m_payButton setBackgroundImage:[UIImage imageNamed:@"finish_pay"] forState:UIControlStateNormal];
            [m_payButton setTitle:@"支付完成" forState:UIControlStateNormal];
            [m_payButton setTitleColor:FinishOrderTextColor forState:UIControlStateNormal];
            m_order_iconImageView.image = [UIImage imageNamed:@"chat_order_icon"];
            m_typeTagLabel.text = @"通用订单";
            
            CGRect r = m_order_bgImageView.frame;
            r.size.height = 250;
            m_order_bgImageView.frame = r;
            
            
        }else if([model->m_payStatus intValue] == 2)
        {
            //订单详情
            m_orderIdView.hidden = NO;
            m_payButton.hidden = YES;
            m_order_iconImageView.image = [UIImage imageNamed:@"chat_order_detail_icon"];
            m_typeTagLabel.text = @"账单详情";
            m_orderIdLabel.text = model->m_order_id;
            
            m_orderDetailView.frame = CGRectMake([m_orderDetailView getViewX], [m_orderDetailView getViewY]+[m_orderIdView getViewHigh], [m_orderDetailView getViewWidth], [m_orderDetailView getViewHigh]);
            CGRect r = m_order_bgImageView.frame;
            r.size.height = 200+[m_orderIdView getViewHigh];
            m_order_bgImageView.frame = r;
        }
    }
    @catch (NSException *exception) {
        m_order_timeLabel.text = @"";
        m_contentLabel.text = @"";
        m_priceLabel.text = @"";
        m_remarkLabel.text = @"";

    }
    
}

#pragma  mark - IBAction
-(void)payOrder:(id)sender
{
    if(m_payButton.tag == 0)
    {
        NSMutableDictionary *dict = [m_chatOrderDict objectForKey:@"charge"];
        
//        dispatch_async(dispatch_get_main_queue(), ^{
        if([[dict objectForKey:@"livemode"]intValue]==1){
            [dict setObject:[NSNumber numberWithInt:1] forKey:@"livemode"];
        }else{
            [dict setObject:[NSNumber numberWithInt:0] forKey:@"livemode"];
        }
        
        NSError *parseError = nil;
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&parseError];
        
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
            [Pingpp createPayment:jsonString viewController:m_superVC appURLScheme:kUrlScheme withCompletion:^(NSString *result, PingppError *error) {
                NSLog(@"completion block: %@", result);
                if (error == nil) {
                    INNLog(@"PingppError is nil");
                    if ([m_delegate respondsToSelector:@selector(cellOrderSuccessIndx:)]) {
                        [m_delegate cellOrderSuccessIndx:m_index];
                    }
                    
                } else {
                    INNLog(@"PingppError: code=%lu msg=%@", (unsigned  long)error.code, [error getMsg]);
                }
                //            [weakSelf showAlertMessage:result];
            }];

//        });
    }
}

@end

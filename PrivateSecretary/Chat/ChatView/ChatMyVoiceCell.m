//
//  ChatMyVoiceCell.m
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/12.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import "ChatMyVoiceCell.h"
#import "ChatFriendsAndMineModel.h"
#import "RecordingPlay.h"


@interface ChatMyVoiceCell()<RecordingPlayDelegate,UIAlertViewDelegate>
{
    IBOutlet    UILabel     *m_time;
    IBOutlet    UIButton    *m_voiceBtn;
    IBOutlet    UIImageView    *m_soundSize;
    IBOutlet    UIImageView     *m_soundBgImage;
    IBOutlet    NSLayoutConstraint      *m_soundBgImage_width;
    IBOutlet    UIButton         *m_updateFailed;
    
    UIAlertView     *m_alertView;
    
    NSString    *voicePath;
}

-(IBAction)playVoice:(id)sender;
-(IBAction)aFreshSendChatImage:(id)sender;

@end

@implementation ChatMyVoiceCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setProperty:(ChatFriendsAndMineModel *)model{
    self.backgroundColor = VCBackgroundColor;
    self.contentView.backgroundColor = VCBackgroundColor;
    
    m_time.text = model->m_voice_duration;
    voicePath = model->m_my_message;
    m_soundSize.image = [UIImage imageNamed:@"my_sound_size_Finsh"];
    m_soundBgImage.image = [m_soundBgImage.image stretchableImageWithLeftCapWidth:20 topCapHeight:20];
    
    int width = 56; //语音基础宽度
//    model->m_voice_duration = @"15";
    if([model->m_voice_duration intValue] < 15){
        m_soundBgImage_width.constant = (kScreenWidth/2-width)/15*[model->m_voice_duration intValue]+width;
    }else{
         m_soundBgImage_width.constant = (kScreenWidth/2-width-50)/80*[model->m_voice_duration intValue]+kScreenWidth/2;
    }
   
   
    [m_soundBgImage setNeedsUpdateConstraints];
    
    //停止播放
    if ([m_recordingPlay getIsPlaying]){
         [m_recordingPlay stopPlay];
    }
    
}

-(void)updateImageSendFail:(NSDictionary *)dict
{
    m_updateFailed.hidden = NO;
}

-(void)updateImageSendSuccss:(NSDictionary *)dict
{
    m_updateFailed.hidden = YES;
}

#pragma mark - IBAction
-(void)playVoice:(id)sender{
    if (m_recordingPlay)
    {
        if ([m_recordingPlay getIsPlaying]) {
            [m_recordingPlay stopPlay];
            return;
        }
        m_recordingPlay.m_delegate = self;
        [m_recordingPlay play:voicePath];
        m_soundSize.image = [UIImage animatedImageNamed:@"my_sound_size_" duration:1.2];
    
    }
}

#pragma mark - RecordingPlayDelegate
-(void)audioPlayerDidFinishPlaying{
     m_soundSize.image = [UIImage imageNamed:@"my_sound_size_Finsh"];
}


-(void)aFreshSendChatImage:(id)sender{
    if (!m_alertView) {
        m_alertView = [[UIAlertView alloc]initWithTitle:@"" message:@"发送失败，重新发送" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    }
    [m_alertView show];
    
}

#pragma mark - UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        if ([_m_delegate respondsToSelector:@selector(cellUpdateImageFailedAfreshSend:)]) {
            [_m_delegate cellUpdateImageFailedAfreshSend:m_index];
        }
    }
}

-(void)dealloc{
    if (m_recordingPlay) {
        m_recordingPlay.m_delegate = nil;
        m_recordingPlay = nil;
    }
}
@end

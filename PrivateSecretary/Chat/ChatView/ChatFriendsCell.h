//
//  ChatFriendsCell.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/5.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ChatViewController;
@class ChatFriendsAndMineModel;

@interface ChatFriendsCell : UITableViewCell
{
    @public
    NSIndexPath *m_indexPath;
    ChatViewController *m_chatVC;
}

-(void)setProperty:(ChatFriendsAndMineModel *)model;

@end

//
//  ChatFriendsVoiceCell.m
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/13.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import "ChatFriendsVoiceCell.h"
#import "RecordingPlay.h"
#import "ChatFriendsAndMineModel.h"
#import "UIButton+RoundButton.h"

@interface ChatFriendsVoiceCell()<RecordingPlayDelegate>
{
    IBOutlet    UILabel     *m_time;
    IBOutlet    UIButton    *m_voiceBtn;
    IBOutlet    UIImageView    *m_soundSize;
    IBOutlet    UIImageView     *m_soundBgImage;
    IBOutlet    NSLayoutConstraint      *m_soundBgImage_width;
    IBOutlet    UIButton    *m_head;
    
    NSString    *voicePath;
}
-(IBAction)playVoice:(id)sender;

@end

@implementation ChatFriendsVoiceCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setProperty:(ChatFriendsAndMineModel *)model
{
    self.backgroundColor = VCBackgroundColor;
    self.contentView.backgroundColor = VCBackgroundColor;
    
    [m_head setRoundButton];
    [m_head setRoundButtonImage:model->m_avatar];
    
    m_time.text = model->m_voice_duration;
    voicePath = model->m_my_message;
    m_soundSize.image = [UIImage imageNamed:@"friend_sound_size_Finish"];
    m_soundBgImage.image = [m_soundBgImage.image stretchableImageWithLeftCapWidth:26 topCapHeight:25];
    
    int width = 56; //语音基础宽度
    //    model->m_voice_duration = @"15";
    if([model->m_voice_duration intValue] < 15){
        m_soundBgImage_width.constant = (kScreenWidth/2-width)/15*[model->m_voice_duration intValue]+width;
    }else{
        m_soundBgImage_width.constant = (kScreenWidth/2-width-50)/80*[model->m_voice_duration intValue]+kScreenWidth/2;
    }
    
    
    [m_soundBgImage setNeedsUpdateConstraints];
    
    if ([m_recordingPlay getIsPlaying]){
        [m_recordingPlay stopPlay];
    }
}

#pragma mark - IBAction
-(void)playVoice:(id)sender{
    if (m_recordingPlay)
    {
        if ([m_recordingPlay getIsPlaying]) {
             [m_recordingPlay stopPlay];
            return;
        }
        
        m_recordingPlay.m_delegate = self;
        [m_recordingPlay playWithUrl:voicePath];
        m_soundSize.image = [UIImage animatedImageNamed:@"friend_sound_size_" duration:1.2];
        
    }
}

#pragma mark - RecordingPlayDelegate
-(void)audioPlayerDidFinishPlaying{
    m_soundSize.image = [UIImage imageNamed:@"friend_sound_size_Finish"];
}

-(void)dealloc{
    if (m_recordingPlay) {
        m_recordingPlay.m_delegate = nil;
        m_recordingPlay = nil;
    }
}


@end

//
//  ChatFriendsVoiceCell.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/13.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ChatFriendsAndMineModel;
@class RecordingPlay;

@interface ChatFriendsVoiceCell : UITableViewCell
{
    @public
     RecordingPlay   *m_recordingPlay;
}
-(void)setProperty:(ChatFriendsAndMineModel *)model;

@end

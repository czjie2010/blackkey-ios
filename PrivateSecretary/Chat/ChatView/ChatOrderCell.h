//
//  ChatOrderCell.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/9/2.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatCellDelegate.h"


@class ChatOrderModel;

@interface ChatOrderCell : UITableViewCell
{
    @public
    UIViewController    *m_superVC;
    NSIndexPath         *m_index;
    NSDictionary        *m_chatOrderDict;
    id<ChatCellDelegate>   m_delegate;
    
}

-(void)setProperty:(ChatOrderModel *)model;
@end

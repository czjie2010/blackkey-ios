//
//  ChatMyCell.m
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/5.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import "ChatMyCell.h"
#import "ChatFriendsAndMineModel.h"
#import "URLProvider.h"
#import "RequestDataWithHttp.h"
#import "DeviceIdentifier.h"
#import "MyInfor.h"
#import "UserIdentityVerificationController.h"

@interface ChatMyCell()<RequestDataWithHttpDelegate>
{
    IBOutlet UILabel        *m_chatText;
    IBOutlet UIImageView    *m_chatBgImage;
    IBOutlet UIButton       *m_head;
    IBOutlet UIView         *m_bgView;
    IBOutlet    UIActivityIndicatorView     *m_activityIndicatorView;
    IBOutlet    UIButton    *m_sendSuccessStatus;
    
    IBOutlet    NSLayoutConstraint      *m_chatTextWidth;
    IBOutlet    NSLayoutConstraint      *m_chatTextHeght;
    
    IBOutlet    NSLayoutConstraint      *m_chatBgImageWidth;
    IBOutlet    NSLayoutConstraint      *m_chatBgImageHeght;
    
    RequestDataWithHttp     *m_request;
    
    UIAlertView     *m_alertView;
    
    ChatFriendsAndMineModel     *m_model;
}

-(IBAction)afreshSendMessage:(id)sender;

@end

@implementation ChatMyCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setPropertyChatMyCellData:(ChatFriendsAndMineModel *)aModel
{
    m_model = aModel;
    self.contentView.backgroundColor = VCBackgroundColor;
    m_chatText.text = aModel->m_my_message;
//    m_chatText.backgroundColor = TestColor;
    m_chatTextWidth.constant = aModel->message_size.width+1;
    m_chatTextHeght.constant = aModel->message_size.height+1;

    m_chatBgImageWidth.constant = aModel->message_size.width+20;
    m_chatBgImageHeght.constant = aModel->message_size.height+20;

    
    [m_chatText setNeedsUpdateConstraints];
    [m_chatBgImage setNeedsUpdateConstraints];
    
    m_chatBgImage.image = [UIImage imageNamed:@"MyChatBgImage"];
    m_chatBgImage.image = [m_chatBgImage.image stretchableImageWithLeftCapWidth:20 topCapHeight:20];
//    [mimi_head setRoundButtonImage:model->mimi_avatar];
}

-(void)setShowActivityIndicatorView:(BOOL)flag
{
    if (flag) {
        m_activityIndicatorView.hidden = NO;
        [m_activityIndicatorView startAnimating];
    }else{
        m_activityIndicatorView.hidden = YES;
        [m_activityIndicatorView stopAnimating];
    }
}

//发送信息到服务器
-(void)sendMessageToSevice:(NSString *)content{
    NSString *strUrl = [URLProvider getsend_chat_message];
    
    NSDictionary *dictinary = [NSDictionary dictionaryWithObjectsAndKeys:
                               [[DeviceIdentifier sharedDeviceInfo] getDeviceID],@"uuid1",
                               [[DeviceIdentifier sharedDeviceInfo] theNewUUID],@"uuid2",
                               nil];
    
    NSMutableDictionary *posdic = [NSMutableDictionary dictionary];
    [posdic setObject:content forKey:@"content"];
    [posdic setObject:@"0" forKey:@"message_type"];
    [posdic setObject:@"0" forKey:@"from"];
    
    if (m_request == nil)
    {
        m_request = [[RequestDataWithHttp alloc]init];
        m_request.m_delegate = self;
    }
    [m_request requestDataWithHttphead:dictinary
                              httpBody:posdic
                               withURL:strUrl
                            HttpMethod:POSTMETHOD];
}


#pragma mark - RequestDataWithHttpDelegate
-(void)successBackData:(id)backNetObj topDownFlag:(BOOL)flag{
    INNLog(@"发送消息成功 ＝＝ %@",backNetObj);
    if ([[backNetObj objectForKey:@"err"]intValue] == 0) {
        //消息发送成功
        [self setShowActivityIndicatorView:NO];
         [self setSendMessgeStatus:YES];
        if ([_m_delegate respondsToSelector:@selector(cellUpdateMessageSuccess:)]) {
            [_m_delegate cellUpdateMessageSuccess:m_indexPath];
        }
       
    }else if([[backNetObj objectForKey:@"err"]intValue] == 403)
    {
        //用户不存在
        if ([[MyInfor shareMyInfor]get_user_id].length >0) {
            [[MyInfor shareMyInfor]removeUserInfo];
            UserIdentityVerificationController *vc = [[UserIdentityVerificationController alloc] initWithNibName:@"UserIdentityVerificationController" bundle:nil];
            vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            vc->m_allowDelete = YES;
            [self.window.rootViewController presentViewController:vc animated:YES completion:^{
            }];
            if ([_m_delegate respondsToSelector:@selector(cellUpdateMessageUserCancel:)]) {
                [_m_delegate cellUpdateMessageUserCancel:m_indexPath];
            }
        }
        
    }else{
        [self failBack:backNetObj topDownFlag:flag];
    }
}

-(void)failBack:(id)failObj topDownFlag:(BOOL)flag{
    INNLog(@"发送消息失败 == %@",failObj);
    [self setShowActivityIndicatorView:YES];
    [self setSendMessgeStatus:NO];
    if ([_m_delegate respondsToSelector:@selector(cellUpdateMessageFail:)]) {
        [_m_delegate cellUpdateMessageFail:m_indexPath];
    }
}

-(void)setSendMessgeStatus:(BOOL)flag
{
    if (flag) {
        m_sendSuccessStatus.hidden = YES;
    }else{
        m_sendSuccessStatus.hidden = NO;
    }
}

-(void)afreshSendMessage:(id)sender
{
    if (!m_alertView) {
        m_alertView = [[UIAlertView alloc]initWithTitle:@"" message:@"发送失败，重新发送" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    }
    [m_alertView show];
}


#pragma mark - UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        [self setSendMessgeStatus:YES];
        [self setShowActivityIndicatorView:YES];
        [self sendMessageToSevice:m_chatText.text];
    }
}

@end

//
//  ChatFriendsAndMineModel.m
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/5.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import "ChatFriendsAndMineModel.h"
#import "FormatNeworkObj.h"
#import "CalculateViewHeight.h"
#import "SaveFile.h"
#import "UIImage+Scale.h"
#import "MyInfor.h"
#import "TimeTool.h"
#import "SDWebImageManager.h"
#import "UIButton+WebCache.h"

@interface ChatFriendsAndMineModel()
{
    SaveFile    *m_saveFile;
}

@end

@implementation ChatFriendsAndMineModel

-(void)setPropertyChatFriendsAndMineModel:(NSDictionary *)dict
{
    
    m_friends_id = [FormatNeworkObj formatObjFrom:dict byKey:@"fuid" needBase64:NO];
    m_friends_name = [FormatNeworkObj formatObjFrom:dict byKey:@"assistant_name" needBase64:NO];
    m_my_message = [FormatNeworkObj formatObjFrom:dict byKey:@"content" needBase64:NO];
    m_message_type = [FormatNeworkObj formatObjFrom:dict byKey:@"message_type" needBase64:NO];
    m_message_from = [FormatNeworkObj formatObjFrom:dict byKey:@"from" needBase64:NO];
    m_voice_duration = [FormatNeworkObj formatObjFrom:dict byKey:@"voice_duration" needBase64:NO];
    m_isSendSuccess = [FormatNeworkObj formatObjFrom:dict byKey:@"isSendSuccess" needBase64:NO];
    
    m_avatar = [FormatNeworkObj formatObjFrom:dict byKey:@"avatar" needBase64:NO];
    m_messgeStatus = [FormatNeworkObj formatObjFrom:dict byKey:@"read_status" needBase64:NO];
    
    m_cid = [FormatNeworkObj formatObjFrom:dict byKey:@"cid" needBase64:NO];
    m_mid = [FormatNeworkObj formatObjFrom:dict byKey:@"mid" needBase64:NO];
    m_user_id = [FormatNeworkObj formatObjFrom:dict byKey:@"user_id" needBase64:NO];
    
    m_created_time = [FormatNeworkObj formatObjFrom:dict byKey:@"created_time" needBase64:NO];
    //    if (m_created_time.length >0) {
    //        INNLog(@"%@",[TimeTool time:m_created_time]);
    //        m_created_time = [TimeTool time:m_created_time];
    //        INNLog(@"%@",m_created_time);
    //    }
    
    if ([m_message_type intValue] == 3) {
        m_order_id = [FormatNeworkObj formatObjFrom:dict byKey:@"order_id" needBase64:NO];
    }
    
    [self recalculateCellSize];
}

-(void)recalculateCellSize
{
    if ([m_message_type intValue] == 0) //信息
    {
        message_size = [CalculateViewHeight striSizeHeight:m_my_message withFont:ChatTextFont maxWith:ChatMaxWith];
        
    }else if ([m_message_type intValue] == 2) //图片
    {
        if([m_message_from intValue] == 0){
            if (!m_saveFile) {
                m_saveFile = [[SaveFile alloc]init];
            }
            NSString *path = [m_saveFile getSaveFilePath:[NSString stringWithFormat:@"%@/%@",[[MyInfor shareMyInfor]get_user_id],@"Image"] FileType:@"jpg" FileName:m_my_message];
            m_chatImage = [UIImage imageWithContentsOfFile:path];
            
        }else if ([m_message_from intValue] == 1){
            
            NSURL *url = [NSURL URLWithString:m_my_message];
            
            m_chatImage = [[SDImageCache sharedImageCache] imageFromMemoryCacheForKey:url.absoluteString];
            if (!m_chatImage) {
                NSData *imgData = [NSData dataWithContentsOfURL:url];
                //              NSData *imgData=[NSURLConnection sendSynchronousRequest:[NSURLRequest requestWithURL:url] returningResponse:nil error:nil];
                //得到图像数据
                m_chatImage = [UIImage imageWithData:imgData];
                [[SDImageCache sharedImageCache] storeImage:m_chatImage recalculateFromImage:YES imageData:imgData forKey:url.absoluteString toDisk:YES];
            }
            
        }
        m_chatImage = [m_chatImage imageByScalingToMaxSize:240.0];
        
        message_size = CGSizeMake(m_chatImage.size.width/2, m_chatImage.size.height/2);
        //          message_size = CGSizeMake(100, 100);
        if(message_size.height >200){
            message_size.height = 200;
        }
        
    }
    m_textBackHeight = message_size.height+20;
    
    if([m_message_from intValue] == 1){
        m_cellHeight = m_textBackHeight+45;
        
    }else{
        m_cellHeight = m_textBackHeight+30;
    }
}

@end

//
//  ChatFriendsAndMineModel.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/5.
//  Copyright (c) 2015年 田沙. All rights reserved.
//


//{
//    cid = 3;
//    content = "\U5218\U6b23 testss";
//    from = 1;
//    "message_type" = 0;
//    mid = 73;
//    "read_status" = 1;
//    "user_id" = 12;
//}


#import <Foundation/Foundation.h>

#define ChatTextFont [UIFont systemFontOfSize:16]
#define ChatMaxWith 220

@interface ChatFriendsAndMineModel : NSObject
{
    @public
    NSString    *m_friends_id,
                *m_friends_name,
                *m_my_message,
//                *m_friends_message,
                *m_message_type,    //0: 信息  1：语音 2:图片 3:订单 4:时间戳 5、正在输入 6、停止输入
                *m_message_from,    // 0-》(0: 我的信息  1：客服回复) 1->(0:我的 1：客服的)
                *m_voice_duration,
                *m_isSendSuccess,   // 0:不成功 1：成功
                *m_avatar,
                *m_cid,
                *m_mid,
                *m_user_id,
                *m_created_time,    
                *m_order_id;
    
    NSString    *m_messgeStatus;    //0:未读 1:已读
    
    int         m_cellHeight;
    CGSize      message_size;
    int         m_textBackHeight;
    
    UIImage     *m_chatImage;
    
    BOOL        isUploading;
}

-(void)setPropertyChatFriendsAndMineModel:(NSDictionary *)dict;

@end

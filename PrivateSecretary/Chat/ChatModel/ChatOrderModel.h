//
//  ChatOrderModel.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/9/2.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChatOrderModel : NSObject
{
    @public
    NSString    *m_message_type,
                *m_content,
                *m_time,
                *m_remark,
                *m_price,
                *m_order_id;
    
    NSString    *m_payStatus; //0：没有支付，1：支付 2:订单详情
}

-(void)setPropertyChatOrderModel:(NSDictionary *)dict;
@end

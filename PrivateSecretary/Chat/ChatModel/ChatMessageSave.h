//
//  ChatMessageSave.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/9/29.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TCPSegmentation;

@interface ChatMessageSave : NSObject
{
    @public
    TCPSegmentation   *m_tcpsegmention;
}

-(void)saveMessage:(NSDictionary *)dictionary;
-(void)removeMessage:(NSDictionary *)dictionary index:(NSInteger)index;
-(void)replaceMessage:(NSDictionary *)dictionary index:(NSInteger)index;
@end

//
//  ChatMessageSave.m
//  PrivateSecretary
//
//  Created by 田沙 on 15/9/29.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import "ChatMessageSave.h"
#import "TCPSegmentation.h"
#import "TCPUDPCONFIG.h"

@interface ChatMessageSave() {
    
}

@end

@implementation ChatMessageSave


//-(id)init{
//    if (self = [super init]) {
//        m_tcpsegmention = [[TCPSegmentation alloc] init];
//    }
//    return self;
//}

-(void)saveMessage:(NSDictionary *)dictionary
{
    NSDictionary *codingDic = [m_tcpsegmention codIng:dictionary];
    NSString *chatSavePath = [m_tcpsegmention getSavePath:ChatCoument];
    
    BOOL fileExis = [m_tcpsegmention haveFileAtPath:chatSavePath];
    if (fileExis)
    {
        NSMutableArray *chatSavedArray = [m_tcpsegmention readArrayFromPath:chatSavePath];
        if (chatSavedArray)
        {
            [chatSavedArray addObject:codingDic];
            [m_tcpsegmention saveArray:chatSavedArray path:chatSavePath];
        }
        else
        {
            INNLog(@"聊天记录创建索引失败");
        }
    }
    else
    {
        BOOL createpath =  [m_tcpsegmention createSavePath:ChatCoument];
        if (createpath)
        {
            NSMutableDictionary *savDicInArray = [m_tcpsegmention codIng:dictionary];
            NSMutableArray *arrayForKey = [NSMutableArray arrayWithObject:savDicInArray];
            
            BOOL flag =[m_tcpsegmention saveArray:arrayForKey path:chatSavePath];
            if (flag)
            {
                INNLog(@"第一次存聊天记录成功");
            }
            else
            {
                INNLog(@"第一次存聊天记录失败");
            }
        }
        else
        {
            INNLog(@"聊天记录第一次创建文件失败");
        }
    }
    
}


-(void)replaceMessage:(NSDictionary *)dictionary index:(NSInteger)index
{
    NSDictionary *codingDic = [m_tcpsegmention codIng:dictionary];
    NSString *chatSavePath = [m_tcpsegmention getSavePath:ChatCoument];
    
    BOOL fileExis = [m_tcpsegmention haveFileAtPath:chatSavePath];
    if (fileExis)
    {
        NSMutableArray *chatSavedArray = [m_tcpsegmention readArrayFromPath:chatSavePath];
        if (chatSavedArray)
        {
            [chatSavedArray replaceObjectAtIndex:index withObject:codingDic];
            [m_tcpsegmention saveArray:chatSavedArray path:chatSavePath];
        }
        else
        {
            INNLog(@"聊天记录创建索引失败");
        }
    }
}

-(void)removeMessage:(NSDictionary *)dictionary index:(NSInteger)index
{
    NSDictionary *codingDic = [m_tcpsegmention codIng:dictionary];
    NSString *chatSavePath = [m_tcpsegmention getSavePath:ChatCoument];
    
    BOOL fileExis = [m_tcpsegmention haveFileAtPath:chatSavePath];
    if (fileExis)
    {
        NSMutableArray *chatSavedArray = [m_tcpsegmention readArrayFromPath:chatSavePath];
        if (chatSavedArray)
        {
            [chatSavedArray removeObjectAtIndex:index];
            [chatSavedArray addObject:codingDic];
            [m_tcpsegmention saveArray:chatSavedArray path:chatSavePath];
        }
        else
        {
            INNLog(@"聊天记录创建索引失败");
        }
    }
}

-(void)replaceOrderMessage:(NSDictionary *)dictionary index:(NSInteger)index
{
    NSDictionary *codingDic = [m_tcpsegmention codIng:dictionary];
    NSString *chatSavePath = [m_tcpsegmention getSavePath:ChatOrderCoument];
    
    BOOL fileExis = [m_tcpsegmention haveFileAtPath:chatSavePath];
    if (fileExis)
    {
        NSMutableDictionary *chatSavedDict = [m_tcpsegmention readFromPath:chatSavePath];
        if (chatSavedDict)
        {
            [chatSavedDict setObject:codingDic forKey:[codingDic objectForKey:@"id"]];
            [m_tcpsegmention saveDicKeyArray:chatSavedDict path:chatSavePath];
        }
        else
        {
            INNLog(@"聊天记录创建索引失败");
        }
    }
}
@end

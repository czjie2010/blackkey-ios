//
//  ChatOrderModel.m
//  PrivateSecretary
//
//  Created by 田沙 on 15/9/2.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

/*******************
 *      "created": 1441780041 , #订单创建时间戳
 *      "body": "9999玫瑰花",  #内容
 *      "description": "备注 七夕情人节鲜花", #备注
 *      "amount": 10,  #价格
 *      "message_type": 3,
 *
 ********************************/

#import "ChatOrderModel.h"
#import "FormatNeworkObj.h"
#import "TimeTool.h"


@implementation ChatOrderModel

-(void)setPropertyChatOrderModel:(NSDictionary *)dict

{
    m_content = [FormatNeworkObj formatObjFrom:dict byKey:@"body" needBase64:NO];
    m_message_type  = [FormatNeworkObj formatObjFrom:dict byKey:@"message_type" needBase64:NO];
    m_time = [FormatNeworkObj formatObjFrom:dict byKey:@"created" needBase64:NO];
    m_time = [TimeTool time:m_time];
    m_remark = [FormatNeworkObj formatObjFrom:dict byKey:@"description" needBase64:NO];
    m_price = [FormatNeworkObj formatObjFrom:dict byKey:@"amount" needBase64:NO];
    m_payStatus = [FormatNeworkObj formatObjFrom:dict byKey:@"payStatus" needBase64:NO];
    m_order_id = [FormatNeworkObj formatObjFrom:dict byKey:@"id" needBase64:NO];
}

@end


//
//  ChatViewController.m
//  PrivateSecretary
//
//  Created by 田沙 on 15/7/29.
//  Copyright (c) 2015年 田沙. All rights reserved.
//
#import "ChatViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "CustomKeyBoardAnimation.h"
#import "ChatPersonModel.h"
#import "ChatFriendsAndMineModel.h"
#import "ChatFriendsCell.h"
#import "ChatMyCell.h"
#import "ChatTimeCell.h"
#import "LoadViewFromXib.h"
#import "TCPSegmentation.h"
#import "TCPUDPCONFIG.h"
#import "INNOTableView.h"
#import "RecordingPlay.h"
#import "ChatMyVoiceCell.h"
#import "TopBarButtonItem.h"
#import "OtherChatActionView.h"
#import "ChatFriendsVoiceCell.h"
#import "ChatMyImageCell.h"
#import "SaveFile.h"
#import "UIImage+Scale.h"
#import "UploadingImageToUpYun.h"
#import "DeviceInfor.h"
#import "SettingViewController.h"
#import "CustomNavigationControllerViewController.h"
#import "ChatFriendsImageCell.h"
#import "URLProvider.h"
#import "DeviceIdentifier.h"
#import "RequestDataWithHttp.h"
#import "AlertMessageTool.h"
#import "Base64.h"
#import "UDPHeartbeat.h"
#import "TCP.h"
#import  "MyInfor.h"
#import "BaiDuLocation.h"
#import "ChatOrderCell.h"
#import "ChatOrderModel.h"
#import "LockSettingView.h"
#import "CalculateViewHeight.h"
#import "TimeTool.h"
#import "ChatInPutStatusCell.h"
#import "UserIdentityVerificationController.h"
#import "ChatMessageSave.h"


@interface ChatViewController ()<UITextViewDelegate,UITableViewDataSource,UITableViewDelegate,INNOTableViewToDownRefreshDelegate,CustomKeyBoardAnimationDelegate,OtherChatActionViewDelegate,UploadingImageToUpYunDelegate,RecordingPlayDelegate,ChatCellDelegate,RequestDataWithHttpDelegate>
{
    IBOutlet    UITextView      *m_chatTextView;
    IBOutlet    UIButton        *m_pressSpeakingBtn;
    IBOutlet    UIView          *m_chatTextBgView;
    IBOutlet    UIImageView     *m_chatImagBg;
    
    float         m_keyboardHeight;
//    IBOutlet    NSLayoutConstraint  *chatTextBgView_y;
//    IBOutlet    NSLayoutConstraint  *chatTextViewHeight;
//    IBOutlet    NSLayoutConstraint  *chatViewBgHeight;
    
    INNOTableView               *m_tabel;
    CustomKeyBoardAnimation     *m_customKeyBoardAnimation;
    LockSettingView             *m_lockSetingView;
    
    NSMutableArray              *listArray;
    NSMutableArray              *listModelArray;
    
    /********
     *  订单plist read array
     *  订单 key 为ChatFriendsAndMineModel or listarray 中订单id
     ******************/
    NSMutableDictionary         *listOrderDict;
    NSMutableDictionary         *listOrderModelDict;   //订单model
    
    
    TCPSegmentation             *m_tcpsegmention;
    
    RecordingPlay               *m_recordingPlay; //录音
    
    OtherChatActionView         *m_otherChatActionView;
    SaveFile                    *m_saveFile;
//    UploadingImageToUpYun       *m_uploadingImageToUpYun;
    
    UIButton                    *m_rightBarButton;
    UIButton                    *m_leftBarButton;
    
    RequestDataWithHttp         *m_request;
    
    BOOL        isGestureRecognizer;
    BOOL        isInPutStatus;  //yes 正在输入中 之行动画
    int         inputCellRow;
    NSIndexPath     *m_orderSuccessIndex;
    
    ChatMessageSave     *m_chatMessageSave;
    
}

-(IBAction)sendMessage:(id)sender;

-(IBAction)speakingOrWriting:(id)sender;
-(IBAction)speaking:(id)sender;


-(IBAction)sendSpeaking:(id)sender;
-(IBAction)willCancelSpeaking:(id)sender;
-(IBAction)cancelSpeaking:(id)sender;
-(IBAction)continueSpeaking:(id)sender;

-(IBAction)otherChatActionView:(id)sender;
@end

@implementation ChatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.frame = CGRectMake(0, 0, kScreenWidth, [DeviceInfor getMaxHiddenTabBarViewHight]);
    // Do any additional setup after loading the view from its nib.
    UIImageView  *navImageView = [[UIImageView alloc]init];
    
    navImageView.frame = CGRectMake(50, 11, kScreenWidth-100, 22);
    navImageView.image = [UIImage imageNamed:@"navLogo"];
    navImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.navigationController.navigationBar addSubview:navImageView];
    
    
    self.view.backgroundColor = VCBackgroundColor;
    [m_chatTextView.layer setCornerRadius:3];
    m_chatTextView.delegate =self;
//    m_chatTextView.
    
    m_tabel = [[INNOTableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, [self.view getViewHigh] -[m_chatTextBgView getViewHigh]) style:UITableViewStylePlain adelegate:self topRefresh:NO bottomRefresh:NO];
    m_tabel.backgroundColor = VCBackgroundColor;
    m_tabel.separatorStyle = UITableViewCellSeparatorStyleNone;
    m_tabel.dataSource = self;
    m_tabel.delegate = self;
    [self.view addSubview:m_tabel];
    
    //自定义键盘动画
    m_customKeyBoardAnimation = [[CustomKeyBoardAnimation alloc]init];
    m_customKeyBoardAnimation.m_delegate = self;
    m_customKeyBoardAnimation->showKeyBoardView = self.view;
    
    //初始化
    listArray = [[NSMutableArray alloc]init];
    listModelArray =  [[NSMutableArray alloc]init];
    
    listOrderDict = [[NSMutableDictionary alloc]init];
    listOrderModelDict = [[NSMutableDictionary alloc]init];
    
    
    m_tcpsegmention = [[TCPSegmentation alloc] init];
    m_chatMessageSave = [[ChatMessageSave alloc]init];
    m_chatMessageSave->m_tcpsegmention = m_tcpsegmention;
    
    m_recordingPlay = [[RecordingPlay alloc]init];
    m_recordingPlay->recordingView = self.view;
    

    [self readFromLocation];
 
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(sensorStateChange:)
                                                 name:UIDeviceProximityStateDidChangeNotification
     
                                               object:nil];
//    m_rightBarButton = [TopBarButtonItem buttonItemBackgroundImage:@"callService"
//                                                                            select:@selector(callService:)
//                                                                                VC:self
//                                                                             frame:CGRectMake(0, 0, 44, 44)
//                                                                             title:@""
//                                                                         titleFont:[UIFont systemFontOfSize:14]
//                                                                        titleColor:nil
//                                                                            offset:kScreenWidth-44-1];
    
    m_leftBarButton = [TopBarButtonItem buttonItemBackgroundImage:@"setting"
                                                                            select:@selector(gotoSettingVC:)
                                                                                VC:self
                                                                             frame:CGRectMake(0, 0, 44, 44)
                                                                             title:@""
                                                                         titleFont:[UIFont systemFontOfSize:14]
                                                                        titleColor:nil
                                                                            offset:1];
   
    m_chatImagBg.image =[m_chatImagBg.image stretchableImageWithLeftCapWidth:20 topCapHeight:20];

    m_request = [[RequestDataWithHttp alloc]init];
    m_request.m_delegate = self;
    

    
    /****
     手动滑动界面 键盘消失
     *********************/
    UITapGestureRecognizer  *tapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(closeKeyboard)];
    [self.view addGestureRecognizer:tapGestureRecognizer];
    
//    INNLog(@"[[BaiDuLocation shareBaiDuLocation]getProvince] = %@",[[BaiDuLocation shareBaiDuLocation]getProvince]);
//     INNLog(@"[[BaiDuLocation shareBaiDuLocation]getAddress] = %@",[[BaiDuLocation shareBaiDuLocation]getAddress]);
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getUserLocation) name:RefreshUserLocationInfor object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    [self.navigationController.navigationBar addSubview:m_rightBarButton];
    [self.navigationController.navigationBar addSubview:m_leftBarButton];
    if (m_lockSetingView) {
        [m_lockSetingView currentLockSettingStatusCommit];
    }
    
//    [m_tcpsegmention segmentTCPArrayPackagToOBJ:nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [m_chatTextView resignFirstResponder];
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    [m_leftBarButton removeFromSuperview];
//    [m_rightBarButton removeFromSuperview];
}

#pragma mark - touches
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
//    [m_chatTextView sizeToFit];
    [m_chatTextView resignFirstResponder];
}

#pragma mark - 自定义方法
//发送信息到服务器
-(void)sendMessageToSevice:(NSDictionary *)dict{
    NSString *strUrl = [URLProvider getsend_chat_message];
    
    NSDictionary *dictinary = [NSDictionary dictionaryWithObjectsAndKeys:
                               [[DeviceIdentifier sharedDeviceInfo] getDeviceID],@"uuid1",
                               [[DeviceIdentifier sharedDeviceInfo] theNewUUID],@"uuid2",
                               nil];
    
    [m_request requestDataWithHttphead:dictinary
                                 httpBody:dict
                                  withURL:strUrl
                               HttpMethod:POSTMETHOD];
}

/*****
 刷新聊天 信息查看状态
 *****/
-(void)getMessageRloadData:(NSMutableDictionary *)dict
{
    [self addTimeTag];
    [dict setObject:@"1" forKey:@"read_status"];
    [listArray addObject:dict];
    
    ChatFriendsAndMineModel *model = [[ChatFriendsAndMineModel alloc]init];
    model->m_messgeStatus = @"1";
    [model setPropertyChatFriendsAndMineModel:dict];

    [m_request requestDataWithHttphead:nil httpBody:nil withURL:[URLProvider get_read_messages:model->m_mid] HttpMethod:GETMETHOD];

    
    [listModelArray addObject:model];
    [m_tabel reloadData];
    [self scroToBottom];
}

/*****
 刷新数据结构
 在缓存数据中插入收到的订单数据
 *****/
-(void)getOrderRloadData:(NSMutableDictionary *)dict
{
    [self addTimeTag];
    
    [listOrderDict setObject:dict forKey:[dict objectForKey:@"id"]];
    
    ChatOrderModel *model = [[ChatOrderModel alloc]init];
    [model setPropertyChatOrderModel:dict];
    [listOrderModelDict setObject:model forKey:[dict objectForKey:@"id"]];
}

/*****
 * 刷新客服状态，是否正在输入中
 ***************/

-(void)getServiceInPutStatus
{
    if (listModelArray.count >=1) {
        if(!isInPutStatus){
            isInPutStatus = YES;
            ChatFriendsAndMineModel *model = [[ChatFriendsAndMineModel alloc]init];
            NSDictionary    *dict = [NSDictionary dictionaryWithObjectsAndKeys:@"5",@"message_type", nil];
            [listArray addObject:dict];
            [model setPropertyChatFriendsAndMineModel:dict];
            [listModelArray addObject:model];
            [self performSelector:@selector(stopInPutStatus) withObject:nil afterDelay:5];
            inputCellRow = (int)listModelArray.count-1;
            [m_tabel reloadData];
            [self scroToBottom];
        }
    }
}

-(void)stopInPutStatus
{
    if (isInPutStatus) {
        ChatFriendsAndMineModel *model = [listModelArray objectAtIndex:inputCellRow];
        if ([model->m_message_type intValue] == 5) {
            isInPutStatus = NO;
            [listModelArray removeObjectAtIndex:inputCellRow];
            [listArray removeObjectAtIndex:inputCellRow];
            [m_tabel reloadData];
        }
    }
 
}

-(void)getOrderStatus{
#warning 因为现在没有 黑钥支付id 暂时使用秘密 所以无法跳转会黑钥，暂无测试 屏蔽
//    ChatOrderModel *model = [listModelArray objectAtIndex:m_orderSuccessIndex.row];
//    model->m_payStatus = @"1";
//    
//    NSMutableDictionary *orderDict = [listOrderDict objectForKey:model->m_order_id];
//    [orderDict setObject:@"1" forKey:@"payStatus"];
//    [m_tabel reloadData];
//    [m_chatMessageSave replaceOrderMessage:orderDict index:m_orderSuccessIndex.row];
}

/****
 消息发送是先删除原来的 正在输入cell
 重新获取 如果正在输入中则继续
 ****************/
-(void)refreshServiceInPutStatus
{
    if (isInPutStatus) {
        isInPutStatus = NO;
        [listModelArray removeObjectAtIndex:inputCellRow];
        [listArray removeObjectAtIndex:inputCellRow];
        [m_tabel reloadData];
    }
}
-(void)closeKeyboard
{
    [m_chatTextView resignFirstResponder];
    [self closeOtherChatActionView];
    isGestureRecognizer = NO;
}


-(void)sendLocation{
    [[BaiDuLocation shareBaiDuLocation]reLocation];
}

/*****
 *在数据结构中，添加时间标签，
 ***************/

-(void)addTimeTag{
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    if (listModelArray.count>0)
    {
        ChatFriendsAndMineModel *tempModel = [listModelArray objectAtIndex:listModelArray.count-1];
        if ([[TimeTool timeToNow:tempModel->m_created_time] intValue]>2) {
            //时间间隔大于2分钟则添加时间标签
            [dict setObject:[TimeTool getNowSecondsTo1970] forKey:@"created_time"];
            [dict setObject:@"4"forKey:@"message_type"];
        }else{
           // 否则不添加
            return;
        }
    }else{
        //如果 是第一次发则直接添加时间标签
        [dict setObject:[TimeTool getNowSecondsTo1970] forKey:@"created_time"];
        [dict setObject:@"4"forKey:@"message_type"];
    }
    
    [m_chatMessageSave saveMessage:dict];
    m_chatTextView.text = @"";
    
    ChatFriendsAndMineModel *addModel = [[ChatFriendsAndMineModel alloc]init];
    [addModel setPropertyChatFriendsAndMineModel:dict];
    [listModelArray addObject:addModel];
    [listArray addObject:dict];
    
}

#pragma mark - RequestDataWithHttpDelegate

-(void)successBackData:(id)backNetObj topDownFlag:(BOOL)flag{
    INNLog(@"发送消息成功 ＝＝ %@",backNetObj);
    if ([[backNetObj objectForKey:@"err"]intValue] == 0) {
        return;
    }else if([[backNetObj objectForKey:@"err"]intValue] == 403)
    {
        //用户不存在
        if ([[MyInfor shareMyInfor]get_user_id].length >0) {
            [[MyInfor shareMyInfor]removeUserInfo];
            UserIdentityVerificationController *vc = [[UserIdentityVerificationController alloc] initWithNibName:@"UserIdentityVerificationController" bundle:nil];
            vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            vc->m_allowDelete = YES;
            [self presentViewController:vc animated:YES completion:^{
            }];
            [self readFromLocation];
        }
    }else{
        [self failBack:backNetObj topDownFlag:flag];
    }

}

-(void)failBack:(id)failObj topDownFlag:(BOOL)flag{
    INNLog(@"发送消息失败 == %@",failObj);
}

-(void)aFreshSaveMessage:(NSMutableArray *)array
{

//    NSString *chatSavePath = [m_tcpsegmention getSavePath:ChatCoument];
//    
//    BOOL fileExis = [m_tcpsegmention haveFileAtPath:chatSavePath];
//    if (fileExis)
//    {
//        NSMutableArray *chatSavedArray = [m_tcpsegmention readArrayFromPath:chatSavePath];
//        if (chatSavedArray)
//        {
//            [chatSavedArray replaceObjectAtIndex:index withObject:codingDic];
//            [m_tcpsegmention saveArray:chatSavedArray path:chatSavePath];
//        }
//        else
//        {
//            INNLog(@"聊天记录创建索引失败");
//        }
//    }
//    m_chatTextView.text = @"";
}

#pragma mark 读取本地聊天内容缓存
-(void)readFromLocation{
    [listArray removeAllObjects];
    [listModelArray removeAllObjects];
    [listOrderModelDict removeAllObjects];
    [listOrderDict removeAllObjects];
    
    NSString *chatPath = [m_tcpsegmention getSavePath:ChatCoument];
    [listArray addObjectsFromArray:[NSArray arrayWithContentsOfFile:chatPath]];
    
    
    NSString *chatOrderPath = [m_tcpsegmention getSavePath:ChatOrderCoument];
    [listOrderDict addEntriesFromDictionary:[NSDictionary dictionaryWithContentsOfFile:chatOrderPath]];
    int count = (int)[listArray count];
    for (int i = 0; i< count; i++)
    {
        NSMutableDictionary *saveDic = [listArray objectAtIndex:i];
        ChatFriendsAndMineModel *model = [[ChatFriendsAndMineModel alloc] init];
        [model setPropertyChatFriendsAndMineModel:saveDic];

        
        /**********
         *  执行该方法后 现实消息，则示为消息已读
         *  更新读取状态，并通知服务器 该条消息已读
         ********************/
        if([@"0" isEqualToString:model->m_messgeStatus])
        {
            [m_request requestDataWithHttphead:nil httpBody:nil withURL:[URLProvider get_read_messages:model->m_mid] HttpMethod:GETMETHOD];
            model->m_messgeStatus = @"1";
            [saveDic setObject:@"1" forKey:@"read_status"];
            
            [m_chatMessageSave replaceMessage:saveDic index:i];
            m_chatTextView.text = @"";
//            [listArray replaceObjectAtIndex:i withObject:saveDic];

        }
        
        if ([model->m_message_type intValue] == 3) {
            //订单
            ChatOrderModel *order_model = [[ChatOrderModel alloc]init];
            [order_model setPropertyChatOrderModel:[listOrderDict objectForKey:model->m_order_id]];
            [listOrderModelDict setObject:order_model forKey:model->m_order_id];
        
        }
//        else if ([model->m_message_type intValue]== 4)
//        {
//            NSDictionary *timeDict = [NSDictionary dictionaryWithObjectsAndKeys:@"4",@"message_type",model->m_created_time,@"created_time" ,nil];
//            NSArray *timeArray = [NSArray arrayWithObject:timeDict];
//            
//            [listArray insertObject:timeArray atIndex:i];
//            
//            ChatFriendsAndMineModel *timeModel = [[ChatFriendsAndMineModel alloc] init];
//            [timeModel setPropertyChatFriendsAndMineModel:timeDict];
//            
//            [listModelArray addObject:timeModel];
//        }
        
        [listModelArray addObject:model];
    }

    
    if (m_tabel)
    {
        [m_tabel reloadData];
    }
    
    if (m_tabel.visibleCells.count)
    {
        [self scroToBottom];
    }

}

-(void)scroToBottom
{
    isGestureRecognizer = NO;
    if([listModelArray count]< 1){
        return;
    }
    NSIndexPath *patTemp= [NSIndexPath indexPathForRow:listModelArray.count-1 inSection:0];
    [m_tabel scrollToRowAtIndexPath:patTemp atScrollPosition:UITableViewScrollPositionNone animated:YES];
}

/***
 *  聊天框的位置变化（跟随键盘、相册功能框等）
 *  调整table 大小 以及滚动到最后一条
 *****/
-(void)chatTextOriginY:(int)y
{
    m_chatTextBgView.frame = CGRectMake(0, [self.view getViewHigh]-y-[m_chatTextBgView getViewHigh], kScreenWidth, [m_chatTextBgView getViewHigh]);
    
    m_tabel.frame = CGRectMake(0, [m_tabel getViewY], kScreenWidth, [self.view getViewHigh]-y-[m_chatTextBgView getViewHigh]);
    //键盘出现后 滚动到最新一条的内容
    if (m_tabel.contentSize.height - m_tabel.bounds.size.height > 0) {
        [m_tabel setContentOffset:CGPointMake(0,m_tabel.contentSize.height - m_tabel.bounds.size.height) animated:YES];
    }
    m_keyboardHeight = y;
//    [m_chatTextBgView setNeedsUpdateConstraints];
}

-(void)closeOtherChatActionView
{
    if (m_otherChatActionView) {
        [UIView animateWithDuration:.25 animations:^{
            m_otherChatActionView.frame = CGRectMake(0, [self.view getViewHigh],kScreenWidth, 0);
            [self chatTextOriginY:0];
        } completion:^(BOOL finished) {
            
        }];
    }
}
#pragma mark - IBAction
#pragma mark 切换输入或者语音

-(void)speakingOrWriting:(id)sender{
    
//    if (m_otherChatActionView) {
//        [UIView animateWithDuration:.25 animations:^{
//            m_otherChatActionView.frame = CGRectMake(0, [self.view getViewHigh],kScreenWidth, 0);
//            [self chatTextOriginY:0];
//        } completion:^(BOOL finished) {
//            
//        }];
//    }
    UIButton *btn = sender;
    btn.selected = !btn.selected;
    if (btn.isSelected)
    {
        //点击    按钮变为语音  进入输入模式
        [btn setImage:[UIImage imageNamed:@"chat_speak"] forState:UIControlStateNormal];
        m_chatTextView.hidden = NO;
        m_pressSpeakingBtn.hidden = YES;
        [m_chatTextView becomeFirstResponder];
        
    }else
    {
        //点击 按钮变为输入状态 进入语音模式
        [btn setImage:[UIImage imageNamed:@"chat_write"] forState:UIControlStateNormal];
        m_chatTextView.hidden = YES;
        m_pressSpeakingBtn.hidden = NO;
        [m_chatTextView resignFirstResponder];
        [self closeOtherChatActionView];

    }
    
}

#pragma mark -开始录音
-(void)speaking:(id)sender
{
    if(m_recordingPlay){
        m_recordingPlay.m_delegate = self;
        //录音开始 则停止任何播放 刷新停止不同cell的动画
        if ([m_recordingPlay getIsPlaying]) {
            [m_tabel reloadData];
        }
    }
    m_chatImagBg.image =[UIImage imageNamed:@"chatRecordingSelected"];
    m_chatImagBg.image =[m_chatImagBg.image stretchableImageWithLeftCapWidth:20 topCapHeight:20];
    [m_recordingPlay record];
}

#pragma mark -发送录音
-(void)sendSpeaking:(id)sender
{
    [self refreshServiceInPutStatus];
    
    [m_recordingPlay stopRecord];
    m_chatImagBg.image =[UIImage imageNamed:@"chat_TextBg"];
    m_chatImagBg.image =[m_chatImagBg.image stretchableImageWithLeftCapWidth:20 topCapHeight:20];
    if ([m_recordingPlay canSendRecording])
    {
    
        NSMutableDictionary *posdic = [NSMutableDictionary dictionary];
        [posdic setObject:[m_recordingPlay getVoiceName] forKey:@"content"];
        [posdic setObject:[TimeTool getNowSecondsTo1970] forKey:@"created_time"];
        [posdic setObject:@"1" forKey:@"message_type"];
        [posdic setObject:@"0" forKey:@"from"];
        [posdic setObject:[m_recordingPlay getRecordingTime] forKey:@"voice_duration"];
        
        [self addTimeTag];
        
        [listArray addObject:posdic];
        
        ChatFriendsAndMineModel *model = [[ChatFriendsAndMineModel alloc]init];
        [model setPropertyChatFriendsAndMineModel:posdic];
        model->isUploading = YES;
        [listModelArray addObject:model];
        
        [m_tabel reloadData];
        [self scroToBottom];
        m_chatTextView.text = @"";
//        [self saveMessage:posdic];
//        [self readFromLocation];
        
    }
}


/****
 手指拖动到按钮之外并松开 取消录制
 *****/
-(void)cancelSpeaking:(id)sender
{
    [m_recordingPlay stopRecord];
    m_chatImagBg.image =[UIImage imageNamed:@"chat_TextBg"];
    m_chatImagBg.image =[m_chatImagBg.image stretchableImageWithLeftCapWidth:20 topCapHeight:20];
}

/****
 手指拖动到按钮之外 提示 松手取消录制
 *****/
-(void)willCancelSpeaking:(id)sender
{
    //TODO:取消录制语音
    [m_recordingPlay cancelRecodingView];
    m_chatImagBg.image =[UIImage imageNamed:@"chat_TextBg"];
    m_chatImagBg.image =[m_chatImagBg.image stretchableImageWithLeftCapWidth:20 topCapHeight:20];
}

-(void)continueSpeaking:(id)sender
{
    //TODO:继续录制语音
    [m_recordingPlay continueRecodingView];
    m_chatImagBg.image =[UIImage imageNamed:@"chatRecordingSelected"];
    m_chatImagBg.image =[m_chatImagBg.image stretchableImageWithLeftCapWidth:20 topCapHeight:20];
}

-(void)sendMessage:(id)sender
{
    [self refreshServiceInPutStatus];
    
    //TODO:发送文本消息
    NSMutableDictionary *posdic = [NSMutableDictionary dictionary];
    [posdic setObject:m_chatTextView.text  forKey:@"content"];
    [posdic setObject:@"0" forKey:@"message_type"];
    [posdic setObject:@"0" forKey:@"from"];
    [posdic setObject:[TimeTool getNowSecondsTo1970] forKey:@"created_time"];
    
    [self addTimeTag];
    [listArray addObject:posdic];
    
    ChatFriendsAndMineModel *model = [[ChatFriendsAndMineModel alloc]init];
    [model setPropertyChatFriendsAndMineModel:posdic];
    model->isUploading = YES;
    [listModelArray addObject:model];
    
    
//    [self saveMessage:posdic];
    [m_tabel reloadData];
//    [self readFromLocation];
//    [self sendMessageToSevice:posdic];
    [self scroToBottom];
    m_chatTextView.text = @"";
    
    [self textViewDidChange:m_chatTextView];

}

-(void)soundSizeChange
{
    [m_recordingPlay ChangeSoundSize];
}

#pragma mark -rigthBarItem IBAction
-(void)callService:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel://10086"]];
}

-(void)gotoSettingVC:(id)sender
{
    [m_chatTextView resignFirstResponder];
    [self closeOtherChatActionView];
    if (!m_lockSetingView)
    {
        m_lockSetingView = [[LockSettingView alloc]init];
        m_lockSetingView->m_superVC = self;
    }
 
    [self.navigationController.view addSubview:m_lockSetingView];
    [m_lockSetingView showLockSettingViewAnimation];
    
    
//    [[BaiDuLocation shareBaiDuLocation]reLocation];
//    SettingViewController *vc = [[SettingViewController alloc]initWithNibName:@"SettingViewController" bundle:nil];
//    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//    CustomNavigationControllerViewController  *nav = [[CustomNavigationControllerViewController alloc]initWithRootViewController:vc];
//    [self presentViewController:nav animated:YES completion:^{
//        
//    }];
}

#pragma mark -其他点击事件页面
-(void)otherChatActionView:(id)sender
{
    [m_chatTextView resignFirstResponder];
    if (!m_otherChatActionView) {
        m_otherChatActionView = [[OtherChatActionView alloc]init];
        m_otherChatActionView.frame = CGRectMake(0, [self.view getViewHigh],kScreenWidth, 0);
        m_otherChatActionView->m_superVC =self;
        m_otherChatActionView.m_delegate = self;
        [self.view addSubview:m_otherChatActionView];
    }
    
    if([m_otherChatActionView getViewHigh] == 0){
        [UIView animateWithDuration:.25 animations:^{
            m_otherChatActionView.frame = CGRectMake(0, [self.view getViewHigh]-112,kScreenWidth, 112);
            [self chatTextOriginY:112];
        } completion:^(BOOL finished) {
            
        }];
    }else{
        [self closeOtherChatActionView];
    }
}

#pragma -mark - OtherChatActionViewDelegate
-(void)sendImage:(UIImage *)image
{
    //TODO:发送图片
    if (!m_saveFile) {
        m_saveFile = [[SaveFile alloc]init];
    }
    [m_saveFile setFileName];
    
    [self refreshServiceInPutStatus];
    
    NSString *path = [m_saveFile getSaveFilePath:[NSString stringWithFormat:@"%@/%@",[[MyInfor shareMyInfor]get_user_id],@"Image"] FileType:@"jpg" FileName:[m_saveFile getFileName]];
    image = [image imageByScalingToMaxSize];
    
    NSData *data = UIImageJPEGRepresentation(image, 1);
    if ([m_saveFile createSavePath:[NSString stringWithFormat:@"%@/%@",[[MyInfor shareMyInfor]get_user_id],@"Image"]]) {
        BOOL flag = [[NSFileManager defaultManager] createFileAtPath:path contents:data attributes:nil];
        if (flag) {
            NSMutableDictionary *posdic = [NSMutableDictionary dictionary];
            [posdic setObject:[m_saveFile getFileName] forKey:@"content"];
            [posdic setObject:[TimeTool getNowSecondsTo1970] forKey:@"created_time"];
            [posdic setObject:@"2" forKey:@"message_type"];
            [posdic setObject:@"0" forKey:@"from"];
            
            [self addTimeTag];
            
            [listArray addObject:posdic];
            
            ChatFriendsAndMineModel *model = [[ChatFriendsAndMineModel alloc]init];
            [model setPropertyChatFriendsAndMineModel:posdic];
            model->isUploading = YES;
            [listModelArray addObject:model];
            
            [m_tabel reloadData];
            [self scroToBottom];
            m_chatTextView.text = @"";
            
        }
    }
   
}


#pragma mark - UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ChatFriendsAndMineModel *chatModel = [listModelArray objectAtIndex:indexPath.row];
    
    if ([chatModel->m_message_type intValue] == 1)
    {
        return 60;
    }else if([chatModel->m_message_type intValue] == 3)
    {
        return 300;
        
    }else if([chatModel->m_message_type intValue] == 4)
    {
        return 42;
    }else if([chatModel->m_message_type intValue] == 5)
    {
        return 60;
    }
    else
    {
        return ((ChatFriendsAndMineModel *)chatModel)->m_cellHeight;
    }
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return listModelArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = indexPath.row;
    ChatFriendsAndMineModel *model = [listModelArray objectAtIndex:row];
    
    if ([model->m_message_type intValue] == 0)
    {
        /*********
         *  信息
         *
         ****************/
        if ([model->m_message_from intValue] == 0)
        {
            static NSString *ReuChatMyCell = @"ChatMyCell";
            ChatMyCell *chatMyCell = [tableView dequeueReusableCellWithIdentifier:ReuChatMyCell];
            if (!chatMyCell)
            {
                chatMyCell =  (ChatMyCell *)[LoadViewFromXib loadView:@"ChatMyCell" theClass:[ChatMyCell class] owner:nil];
                chatMyCell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            
            chatMyCell->m_indexPath = indexPath;
            [chatMyCell setPropertyChatMyCellData:model];
            chatMyCell.m_delegate = self;
            [chatMyCell setShowActivityIndicatorView:NO];
            if (model->isUploading) {
                [chatMyCell setShowActivityIndicatorView:YES];
                [chatMyCell sendMessageToSevice:model->m_my_message];
                model->isUploading = NO;
            }
    
            if ([@"0" isEqualToString:model ->m_isSendSuccess]) {
                [chatMyCell setSendMessgeStatus:NO];
            }else{
                [chatMyCell setSendMessgeStatus:YES];
            }
           
            return chatMyCell;
            
        }else  if ([model->m_message_from intValue] == 1)
        {
            static NSString *FriendsReusable = @"ChatFriendsCell";
            ChatFriendsCell *chatFriendsCell = [tableView dequeueReusableCellWithIdentifier:FriendsReusable];
            if (!chatFriendsCell)
            {
                chatFriendsCell =  (ChatFriendsCell *)[LoadViewFromXib loadView:@"ChatFriendsCell" theClass:[ChatFriendsCell class] owner:nil];
                chatFriendsCell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            [chatFriendsCell setProperty:model];
            
            return chatFriendsCell;
        }
    }else if([model->m_message_type intValue] == 1)
    {
        /*********
         *  语音
         *
         ****************/
        if ([model->m_message_from intValue] == 0){
            static NSString *voiceCellReu = @"ChatMyVoiceCell";
            ChatMyVoiceCell *voiceCell = [tableView dequeueReusableCellWithIdentifier:voiceCellReu];
            if (!voiceCell)
            {
                voiceCell = (ChatMyVoiceCell *)[LoadViewFromXib loadView:@"ChatMyVoiceCell" theClass:[ChatMyVoiceCell class] owner:nil];
                voiceCell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            voiceCell->m_recordingPlay = m_recordingPlay;
            voiceCell->m_index = indexPath;
            voiceCell.m_delegate = self;
            [voiceCell setProperty:model];
            
            if (model->isUploading)
            {
                UploadingImageToUpYun *uploadingImageToUpYun = [[UploadingImageToUpYun alloc]init];
                uploadingImageToUpYun.m_delegate = self;
                uploadingImageToUpYun->uploadType = UploadSounds;
                [uploadingImageToUpYun getUploadingFile:[m_recordingPlay getVoiceData] inView:voiceCell];
            }
            
            if ([@"0" isEqualToString:model ->m_isSendSuccess]) {
                [voiceCell updateImageSendFail:nil];
            }else{
                [voiceCell updateImageSendSuccss:nil];
            }
            
            return voiceCell;
            
        }else if([model->m_message_from intValue] == 1)
        {
            static NSString *cellReu = @"ChatFriendsVoiceCell";
            ChatFriendsVoiceCell *cell = [tableView dequeueReusableCellWithIdentifier:cellReu];
            if (!cell)
            {
                cell = (ChatFriendsVoiceCell *)[LoadViewFromXib loadView:@"ChatFriendsVoiceCell" theClass:[ChatFriendsVoiceCell class] owner:nil];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            
            cell->m_recordingPlay = m_recordingPlay;
            [cell setProperty:model];
            
            return cell;
        }
       
    }else if([model->m_message_type intValue] == 2)
    {
        if([model->m_message_from intValue] == 0)
        {
            static NSString *cellReu = @"ChatMyImageCell";
            ChatMyImageCell *cell = [tableView dequeueReusableCellWithIdentifier:cellReu];
            if (!cell)
            {
                cell = (ChatMyImageCell *)[LoadViewFromXib loadView:@"ChatMyImageCell" theClass:[ChatMyImageCell class] owner:nil];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            
            cell->m_superVC = self;
            cell->m_index = indexPath;
            cell.m_delegate = self;
            [cell setProperty:model];
            if (model->isUploading)
            {
                UploadingImageToUpYun *uploadingImageToUpYun = [[UploadingImageToUpYun alloc]init];
                uploadingImageToUpYun.m_delegate = self;
                uploadingImageToUpYun->uploadType = UploadImage;
                [uploadingImageToUpYun getUploadingFile:model->m_chatImage inView:cell];
            }
            
            if ([@"0" isEqualToString:model ->m_isSendSuccess]) {
                [cell updateImageSendFail:nil];
            }else{
                [cell updateImageSendSuccss:nil];
            }
            return cell;
            
        }else if([model->m_message_from intValue] == 1)
        {
            static NSString *cellReu = @"ChatMyImageCell";
            ChatFriendsImageCell *cell = [tableView dequeueReusableCellWithIdentifier:cellReu];
            if (!cell)
            {
                cell = (ChatFriendsImageCell *)[LoadViewFromXib loadView:@"ChatFriendsImageCell" theClass:[ChatFriendsImageCell class] owner:nil];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            
            cell->m_superVC = self;
            [cell setProperty:model];
            
            return cell;
        }
    }else if([model->m_message_type intValue] == 3)
    {
        /************
         *  订单
         ****************/
       ChatOrderModel   *orderModel = [listOrderModelDict objectForKey:model->m_order_id];
        static NSString *cellReu = @"ChatOrderCell";
        ChatOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:cellReu];
        if (!cell)
        {
            cell = (ChatOrderCell *)[LoadViewFromXib loadView:@"ChatOrderCell" theClass:[ChatOrderCell class] owner:nil];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        cell->m_superVC = self;
        cell->m_index = indexPath;
        cell->m_delegate = self;
        cell->m_chatOrderDict = [listOrderDict objectForKey:model->m_order_id];
        [cell setProperty:orderModel];

        return cell;
        
    }else if([model->m_message_type intValue] == 4)
    {
        static NSString *timeReu = @"ChatTimeCell";
        ChatTimeCell *timeCell = [tableView dequeueReusableCellWithIdentifier:timeReu];
        if (!timeCell)
        {
            timeCell = (ChatTimeCell *)[LoadViewFromXib loadView:@"ChatTimeCell" theClass:[ChatTimeCell class] owner:nil];
        }
        
        [timeCell setProperty:model];
        
        return timeCell;
        
    }else if([model->m_message_type intValue] == 5)
    {
        static NSString *cellReu = @"ChatInPutStatusCell";
        ChatInPutStatusCell *cell = [tableView dequeueReusableCellWithIdentifier:cellReu];
        if (!cell)
        {
            cell = (ChatInPutStatusCell *)[LoadViewFromXib loadView:@"ChatInPutStatusCell" theClass:[ChatInPutStatusCell class] owner:nil];
        }
        
        [cell setInputStatus];
        return cell;
    }



//    NSObject *obj  = [listModelArray objectAtIndex:row];
//    if ([obj isKindOfClass:[NSString class]])
//    {
//        [timeCell setTime:(NSString *)obj];
//        return timeCell;
//    }
//    else
//    {
//        if ([obj isKindOfClass:[chatview class]])
//        {
//            BOOL flagIsFriends = ((ChatFriendsAndMineModel *)obj)->mimi_isFriendType;
//            if (flagIsFriends)
//            {
//                [mimiChatFriendCell setProperty:(ChatFriendsAndMineModel *)obj];
//                mimiChatFriendCell->m_chatVC = self;
//                mimiChatFriendCell.mimi_path = indexPath;
//                return mimiChatFriendCell;
//            }
//            else
//            {
//                [mimiChatMyCell setProper:(ChatFriendsAndIModel *)obj];
//                mimiChatMyCell->mimi_chatVC = self;
//                mimiChatMyCell.mimi_path = indexPath;
//                return mimiChatMyCell;
//            }
//        }
//    }
    return nil;
}

#pragma -mark - UIScrollViewDelegate

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    [m_chatTextView resignFirstResponder];
    
    if (scrollView == m_chatTextView)
    {

    }else if(scrollView == m_tabel)
    {
        if(isGestureRecognizer){
            [self closeKeyboard];
        }
    }
        
}

-(void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
    if(scrollView == m_tabel){
        if ([m_chatTextView isFirstResponder]||[m_otherChatActionView getViewHigh]>0) {
            isGestureRecognizer = YES;
        }
    }
}



#pragma mark - UITextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    if ([text isEqualToString:@"\n"])
    {
        [self sendMessage:nil];
        return NO;
    }
    
    return YES;
}


-(void)textViewDidChange:(UITextView *)textView{
    INNLog(@"textViewDidChange");
  
    
    int height = [CalculateViewHeight striSizeHeight:m_chatTextView.text withFont:m_chatTextView.font maxWith:[m_chatTextView getViewWidth]].height+10;
    
    /****
     * 大于100 后 聊天框不在变化
     ******/
    if (height > 100) {
        return;
    }
    
    int refreshHeight = height - [textView getViewHigh];
    if (refreshHeight > 0) {
        //正在输入 加大对话框
        if(refreshHeight < 10){
            return;
        }
    }else if (refreshHeight < 0){
        if (refreshHeight > -10) {
            return;
        }
    }else if(refreshHeight == 0){
        return;
    }

    INNLog(@"\n\n\n height = %d",height);
    [UIView animateWithDuration:.3f animations:^{
        
        CGRect chatTextBgView_r = m_chatTextBgView.frame ;
        INNLog(@"\n\n\n animateWithDuration ");
        if (height <37) {
            chatTextBgView_r.origin.y = (kScreenHeight-64 - m_keyboardHeight- 37-12);
            chatTextBgView_r.size.height =  37+12.0;
            //                m_chatTextBgView.frame = chatTextBgView_r;
            
        }else if (height < 100) {
            
            chatTextBgView_r.origin.y = (kScreenHeight-64 - m_keyboardHeight- height-12);
            chatTextBgView_r.size.height =  height+12;
            //                m_chatTextBgView.frame = chatTextBgView_r;
            m_chatTextView.contentSize = CGSizeMake(0,height);
            
        }else{
            chatTextBgView_r.origin.y = (kScreenHeight-64 - m_keyboardHeight- 100-12);
            chatTextBgView_r.size.height =  100+12;
        }
        
        CGRect r = m_tabel.frame;
        r.size.height = chatTextBgView_r.origin.y;
        m_tabel.frame = r;
        m_chatTextBgView.frame = chatTextBgView_r;
        [self scroToBottom];
        
        
    } completion:^(BOOL finished) {
        
    }];

}


#pragma -mark - CustomKeyBoardAnimationDelegate
-(void)keyboardWillShowViewFrame:(float)keyboardHeight
{
    [self chatTextOriginY:keyboardHeight];
//    [m_tabel reloadData];
    if (m_otherChatActionView) {
        //如果m_otherChatActionView 存在 键盘升起后 隐藏 m_otherChatActionView
        m_otherChatActionView.frame = CGRectMake(0, [self.view getViewHigh],kScreenWidth, 0);
    }
    
}

-(void)keyboardWillHiddenViewFrame:(float)keyboardHeight
{
    m_chatTextBgView.frame = CGRectMake(0, [self.view getViewHigh]-[m_chatTextBgView getViewHigh], kScreenWidth, [m_chatTextBgView getViewHigh]);
     m_tabel.frame = CGRectMake(0, [m_tabel getViewY], kScreenWidth, [self.view getViewHigh]-[m_chatTextBgView getViewHigh]);

    m_keyboardHeight = 0;
    [m_tabel reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//处理监听触发事件

-(void)sensorStateChange:(NSNotificationCenter *)notification;

{
    //如果此时手机靠近面部放在耳朵旁，那么声音将通过听筒输出，并将屏幕变暗（省电啊）
    if ([[UIDevice currentDevice] proximityState] == YES)
    {
        INNLog(@"Device is close to user");
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    }
    
    else
        
    {
        INNLog(@"Device is not close to user");
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
        if (![m_recordingPlay getIsPlaying])
        {
            //没有播放了，也没有在黑屏状态下，就可以把距离传感器关了
            [[UIDevice currentDevice] setProximityMonitoringEnabled:NO];
        }
    }
}

#pragma mark - UploadingImageToUpYunDelegate
-(void)getUploadingImageToUpYunProgress:(float)progress sendBytes:(long long)requestDidSendBytes inView:(UIView *)view
{
        if ([view isKindOfClass:[ChatMyImageCell class]])
        {
            [(ChatMyImageCell *) view updateImageSendProgress:progress];
        }
}

-(void)UploadingImageToUpYunFail:(NSDictionary *)dict inView:(UIView *)view
{
    if ([view isKindOfClass:[ChatMyImageCell class]])
    {
        ChatMyImageCell *cell = (ChatMyImageCell *) view;
        [cell updateImageSendFail:dict];
        NSIndexPath  *index = cell->m_index;
        NSMutableDictionary *dictionary = [listArray objectAtIndex:index.row];
       
        //已经存储过该条消息
        if([@"0" isEqualToString:[dictionary objectForKey:@"isSendSuccess"]]){
            
        }else
        {
            [dictionary setObject:@"0" forKey:@"isSendSuccess"];
            [m_chatMessageSave saveMessage:dictionary];
            m_chatTextView.text = @"";
        }
       
        ChatFriendsAndMineModel *model = [listModelArray objectAtIndex:index.row];
        model->m_isSendSuccess = @"0";
        
    }else if ([view isKindOfClass:[ChatMyVoiceCell class]])
    {
        ChatMyVoiceCell *cell = (ChatMyVoiceCell *) view;
        [cell updateImageSendFail:dict];
        NSIndexPath  *index = cell->m_index;
        NSMutableDictionary *dictionary = [listArray objectAtIndex:index.row];
        
        //已经存储过该条消息
        if([@"0" isEqualToString:[dictionary objectForKey:@"isSendSuccess"]]){
            
        }else{
            [dictionary setObject:@"0" forKey:@"isSendSuccess"];
            [m_chatMessageSave saveMessage:dictionary];
            m_chatTextView.text = @"";
        }
        
        ChatFriendsAndMineModel *model = [listModelArray objectAtIndex:index.row];
        model->m_isSendSuccess = @"0";

    }
}

-(void)UploadingImageToUpYunSuccess:(NSDictionary *)dict inView:(UIView *)view
{
    if ([view isKindOfClass:[ChatMyImageCell class]])
    {
        ChatMyImageCell *cell = (ChatMyImageCell *) view;
        [cell updateImageSendSuccss:dict];
        NSIndexPath  *index = cell->m_index;
        NSMutableDictionary *dictionary = [listArray objectAtIndex:index.row];
        
        //已经存过该消息
        if([@"0" isEqualToString:[dictionary objectForKey:@"isSendSuccess"]]){
            [dictionary setObject:@"1" forKey:@"isSendSuccess"];
            [m_chatMessageSave replaceMessage:dictionary index:index.row];
            m_chatTextView.text = @"";
            
        }else{
            [dictionary setObject:@"1" forKey:@"isSendSuccess"];
            //本地不存储 upyun 上声音名字
            [m_chatMessageSave saveMessage:dictionary];
            m_chatTextView.text = @"";
        }
       
        
        [dictionary setObject:[dict objectForKey:@"url"] forKey:@"content"];
        [self sendMessageToSevice:dictionary];
        
        ChatFriendsAndMineModel *model = [listModelArray objectAtIndex:index.row];
        model->isUploading = NO;
    }else if ([view isKindOfClass:[ChatMyVoiceCell class]])
    {
        ChatMyVoiceCell *cell = (ChatMyVoiceCell *) view;
        [cell updateImageSendSuccss:dict];
        NSIndexPath  *index = cell->m_index;
        NSMutableDictionary *dictionary = [listArray objectAtIndex:index.row];
        [dictionary setObject:@"1" forKey:@"isSendSuccess"];
        
        [m_chatMessageSave saveMessage:dictionary];
        m_chatTextView.text = @"";
        //本地不存储 upyun 上声音名字
        [dictionary setObject:[dict objectForKey:@"url"] forKey:@"content"];
        [self sendMessageToSevice:dictionary];
        
        ChatFriendsAndMineModel *model = [listModelArray objectAtIndex:index.row];
        model->isUploading = NO;
    }
}

#pragma mark - ChatCellDelegate
-(void)cellUpdateImageFailedAfreshSend:(NSIndexPath *)index
{
    ChatFriendsAndMineModel *model = [listModelArray objectAtIndex:index.row];
    model->isUploading = YES;
    NSArray *indexArray=[NSArray arrayWithObject:index];
    [m_tabel reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationNone];
}

-(void)cellUpdateMessageSuccess:(NSIndexPath *)index
{
    //消息发送成功
     NSMutableDictionary *dictionary = [listArray objectAtIndex:index.row];
    
    ChatFriendsAndMineModel *model = [listModelArray objectAtIndex:index.row];
    model->isUploading = NO;
    
    //已经存过该消息
    if([@"0" isEqualToString:[dictionary objectForKey:@"isSendSuccess"]]){
        [dictionary setObject:@"1" forKey:@"isSendSuccess"];
        [listArray removeObjectAtIndex:index.row];
        [listModelArray removeObjectAtIndex:index.row];
        [listModelArray addObject:model];
        [listArray addObject:dictionary];
        [m_chatMessageSave removeMessage:dictionary index:index.row];
        m_chatTextView.text = @"";
        [m_tabel reloadData];
        
    }else{
        [dictionary setObject:@"1" forKey:@"isSendSuccess"];
        [m_chatMessageSave saveMessage:dictionary];
        m_chatTextView.text = @"";
    }
    
    model->m_isSendSuccess = @"1";
    
}

-(void)cellUpdateMessageFail:(NSIndexPath *)index
{
    //消息发送失败
    NSMutableDictionary *dictionary = [listArray objectAtIndex:index.row];
    
    ChatFriendsAndMineModel *model = [listModelArray objectAtIndex:index.row];
    model->isUploading = NO;
    
    //已经存过该消息
    if([@"0" isEqualToString:[dictionary objectForKey:@"isSendSuccess"]]){
        
    }else{
        [dictionary setObject:@"0" forKey:@"isSendSuccess"];
        [m_chatMessageSave saveMessage:dictionary];
        m_chatTextView.text = @"";
    }
    
    model->m_isSendSuccess = @"0";
    NSArray *indexArray=[NSArray arrayWithObject:index];
    [m_tabel reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationNone];
}

//用户不存在
-(void)cellUpdateMessageUserCancel:(NSIndexPath *)index
{
    [self readFromLocation];
}

-(void)cellOrderSuccessIndx:(NSIndexPath *)index{
    m_orderSuccessIndex = index;
}


#pragma mark - RecordingPlayDelegate
-(void)audioTimeOutAutomaticSend
{
    [m_recordingPlay stopRecord];
    m_chatImagBg.image =[UIImage imageNamed:@"chat_TextBg"];
    m_chatImagBg.image =[m_chatImagBg.image stretchableImageWithLeftCapWidth:20 topCapHeight:20];
    
    NSMutableDictionary *posdic = [NSMutableDictionary dictionary];
    [posdic setObject:[m_recordingPlay getVoiceName] forKey:@"content"];
    [posdic setObject:@"1" forKey:@"message_type"];
    [posdic setObject:@"0" forKey:@"from"];
    [posdic setObject:[m_recordingPlay getRecordingTime] forKey:@"voice_duration"];
    
    [m_chatMessageSave saveMessage:posdic];
    m_chatTextView.text = @"";
    [self readFromLocation];
    INNLog(@"path = %@",[m_recordingPlay getPath]);

}


-(void)getUserLocation
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    NSDictionary  *dict = [NSDictionary dictionaryWithObjectsAndKeys:[ud objectForKey:BaiDuLongitude],@"longitude",
                           [ud objectForKey:BaiDuLatitude],@"latitude",
                           [ud objectForKey:BaiDuCity],@"city",
                           [ud objectForKey:BaiDuProvince],@"province",
                           [[BaiDuLocation shareBaiDuLocation]getDistrict],@"district",
                           [ud objectForKey:BaiDuAdress],@"address",
                           nil];
    
    [m_request requestDataWithHttphead:nil httpBody:dict withURL:[URLProvider get_member_lbs:[[MyInfor shareMyInfor]get_user_id]] HttpMethod:POSTMETHOD];
}

#pragma mark - INNOTableViewToDownRefreshDelegate
- (void)reloadTableViewDataSource
{
    
}

//-(BOOL)prefersStatusBarHidden{
//    return NO;
//}

-(void)dealloc{
    m_customKeyBoardAnimation.m_delegate = nil;
    m_customKeyBoardAnimation = nil;
    m_tabel.delegate = nil;
    
    m_otherChatActionView = nil;
    m_chatTextView.delegate =nil;
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    if (m_recordingPlay) {
        m_recordingPlay.m_delegate = nil;
        m_recordingPlay = nil;
    }
}

@end

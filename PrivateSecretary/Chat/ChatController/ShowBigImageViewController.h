//
//  ShowBigImageViewController.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/14.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ChatFriendsAndMineModel;

@interface ShowBigImageViewController : UIViewController
{
    @public
    ChatFriendsAndMineModel        *m_model;
}
@end

//
//  ChatViewController.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/7/29.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "SuperViewController.h"

@interface ChatViewController : UIViewController
{
    @public
    NSString        *m_fuid;
}

-(void)getMessageRloadData:(NSMutableDictionary *)dict;
-(void)getOrderRloadData:(NSMutableDictionary *)dict;

-(void)getServiceInPutStatus;
-(void)stopInPutStatus;
-(void)getServiceStopInPutStatus;

-(void)getOrderStatus;
-(void)sendLocation;
@end

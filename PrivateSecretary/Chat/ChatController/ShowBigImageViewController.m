//
//  ShowBigImageViewController.m
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/14.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import "ShowBigImageViewController.h"
#import "ImageScale.h"
#import "SaveFile.h"
#import "MyInfor.h"
#import "ChatFriendsAndMineModel.h"

@interface ShowBigImageViewController ()<ImageScaleDelegate>
{
    SaveFile        *m_saveFile;
}

@end

@implementation ShowBigImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImage *bigImage;
    
    // Do any additional setup after loading the view from its nib.
    if([m_model->m_message_from intValue] ==1){
        
        NSURL *url = [NSURL URLWithString:m_model->m_my_message];
        NSData *imgData = [NSData dataWithContentsOfURL:url];
        //得到图像数据
        bigImage = [UIImage imageWithData:imgData];

//        bigImage = m_model->m_chatImage;
        
        
    }else{
        m_saveFile = [[SaveFile alloc]init];
        NSString *path = [m_saveFile getSaveFilePath:[NSString stringWithFormat:@"%@/%@",[[MyInfor shareMyInfor]get_user_id],@"Image"] FileType:@"jpg" FileName:m_model->m_my_message];
        bigImage = [UIImage imageWithContentsOfFile:path];
    }
    
    ImageScale *imageScale = [[ImageScale alloc] initWithImage:bigImage];
    imageScale.m_delegate = self;
    [self.view addSubview:imageScale];
}

//-(BOOL)prefersStatusBarHidden{
//    return YES;
//}

#pragma mark - ImageScaleDelegate
-(void)imageScale:(ImageScale *)cropper didFinishScalingWithImage:(UIImage *)image{
    
}

-(void)imageScaleDidCancel:(ImageScale *)cropper{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc{
    
}
@end

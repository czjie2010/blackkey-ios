//
//  MainViewController.m
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/6.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import "MainViewController.h"
#import "ChatViewController.h"
#import "SettingViewController.h"
#import "UIButton+RoundButton.h"
#import "CustomNavigationControllerViewController.h"


#import "PayViewController.h"
#import "LockAppViewController.h"

@interface MainViewController ()
{
    IBOutlet    UIButton    *m_callAssistant;
    IBOutlet    UIButton    *m_setting;
}

-(IBAction)callAssistant:(id)sender;
-(IBAction)setting:(id)sender;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = VCBackgroundColor;
    // Do any additional setup after loading the view from its nib.
    [m_callAssistant setRoundButton];
    [m_setting setRoundButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBAction
-(void)callAssistant:(id)sender
{
    ChatViewController *chatVC = [[ChatViewController alloc]initWithNibName:@"ChatViewController" bundle:nil];
    chatVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    CustomNavigationControllerViewController  *chatNav = [[CustomNavigationControllerViewController alloc]initWithRootViewController:chatVC];
    [self presentViewController:chatNav animated:YES completion:^{
        
    }];
    chatVC = nil;
}

-(void)setting:(id)sender
{
    SettingViewController *vc = [[SettingViewController alloc]initWithNibName:@"SettingViewController" bundle:nil];
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    CustomNavigationControllerViewController  *nav = [[CustomNavigationControllerViewController alloc]initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:^{
        
    }];

}

-(IBAction)testPay:(id)sender
{
    PayViewController *vc = [[PayViewController alloc]initWithNibName:@"PayViewController" bundle:nil];
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    CustomNavigationControllerViewController  *nav = [[CustomNavigationControllerViewController alloc]initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:^{
        
    }];
    
//    LockAppViewController *vc = [[LockAppViewController alloc]initWithNibName:@"LockAppViewController" bundle:nil];
//    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//    vc->s_titleName = @"设置密码";
//    vc->m_allowDelete = YES;
//    CustomNavigationControllerViewController  *nav = [[CustomNavigationControllerViewController alloc]initWithRootViewController:vc];
//    [self presentViewController:nav animated:YES completion:^{
//        
//    }];

}

@end

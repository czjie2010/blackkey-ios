//
//  SuperViewController.m
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/6.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import "SuperViewController.h"

@interface SuperViewController (){
    UIButton    *backButton;
}


@end

@implementation SuperViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addBackButton];
}

#pragma mark - 自定义方法
-(void)addBackButton
{
    if (backButton == nil) {
        backButton = [[UIButton alloc]init];
        backButton.frame = CGRectMake(0, 0,40, 40);
        [self.navigationController.navigationBar addSubview:backButton];
    }
    [backButton addTarget:self action:@selector(superBackToTop:) forControlEvents:UIControlEventTouchUpInside];
    
    [backButton setImage:[UIImage imageNamed:@"lockBackSuper"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBttuontouchDown:) forControlEvents:UIControlEventTouchDown];
    [backButton addTarget:self action:@selector(backBttuontouchDownDragOutside:) forControlEvents:UIControlEventTouchDragOutside];
    [backButton setAdjustsImageWhenHighlighted:NO];
    
    [self.navigationItem setHidesBackButton:YES];
}

-(void)hiddenBackButton:(BOOL)flag{
    backButton.hidden = flag;
}

#pragma mark - IBAction
-(void)superBackToTop:(id)sender
{
    INNLog(@"Self=:%@",self);
    INNLog(@"NZVI=:%@",self.navigationController);
    [backButton removeFromSuperview];
    backButton = nil;
    [self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

-(void)backBttuontouchDown:(id)sender{
    backButton.alpha = 0.7;
}

-(void)backBttuontouchDownDragOutside:(id)sender{
    backButton.alpha = 1.0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc
{

}

@end

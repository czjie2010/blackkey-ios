//
//  SuperViewController.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/6.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SuperViewController : UIViewController

-(void)hiddenBackButton:(BOOL)flag;
-(void)superBackToTop:(id)sender;
@end

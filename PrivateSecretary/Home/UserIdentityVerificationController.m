//
//  LockAppViewController.m
//  Secrets
//
//  Created by 胡 庆贺 on 14-6-16.
//  Copyright (c) 2014年 huqinghe. All rights reserved.
//

#import "UserIdentityVerificationController.h"
#import "DeviceInfor.h"
#import "AlertMessageTool.h"
#import "MyInfor.h"
#import "RequestDataWithHttp.h"
#import "URLProvider.h"
#import "DeviceIdentifier.h"
#import "TCP.h"
#import "NSString+NULL.h"
#import "INNPushNoticeCenter.h"

@interface UserIdentityVerificationController ()<RequestDataWithHttpDelegate>
{
    IBOutlet UIView* keyboardBack;
    IBOutlet UIView* fillBoardBac;
    
    NSMutableString *mimi_firstInput;
    NSMutableString *mimi_confirm;
    
    IBOutlet UIView *mimi_iputImagBack;
    NSArray *mimi_inputImage;
    
    IBOutlet UIView *mimi_inputButtBack;
//    UIButton *mimi_backButton;
    // 数字按钮
    IBOutlet UIButton *zero;
    IBOutlet UIButton *one;
    IBOutlet UIButton *two;
    IBOutlet UIButton *three;
    IBOutlet UIButton *four;
    IBOutlet UIButton *five;
    IBOutlet UIButton *six;
    IBOutlet UIButton *seven;
    IBOutlet UIButton *eight;
    IBOutlet UIButton *nine;
    IBOutlet UIButton *ten;
    IBOutlet UIButton *delete;
    IBOutlet UIImageView *deleteImageView;
    
    /**
     *  秘密显示 imageView
     */
    IBOutlet    UILabel     *pwsTagLabel;
    IBOutlet UIButton *pwsOneButton;
    IBOutlet UIButton *pwsTwoButton;
    IBOutlet UIButton *pwsThreeButton;
    IBOutlet UIButton *pwsFourButton;
    IBOutlet UIButton *pwsFiveButton;
    IBOutlet UIButton *pwsSixButton;
    
    IBOutlet    UIImageView *balckKeyIcon;
    
    RequestDataWithHttp     *m_request;
    

    IBOutlet    NSLayoutConstraint      *m_ImageTopY;
    
    IBOutlet    UIView      *m_welcomeBgView;
    IBOutlet    UILabel     *m_welcomeUserName;
    
    INNPushNoticeCenter     *m_push;
}
-(IBAction)lock:(UIButton *)sender;
-(IBAction)backToSuper:(id)sender;
-(void)inputButtUserInter:(BOOL)flag;
-(IBAction)deleteLock:(id)sender;
-(void)deleteStrAndImageState:(NSMutableString *)str;
-(void)deleteLocationFromLocation;
@end

@implementation UserIdentityVerificationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
//    keyboardBack.frame = CGRectMake(keyboardBack.frame.origin.x,[[UIScreen mainScreen] bounds].size.height - keyboardBack.frame.size.height -14 - NavigationgHihgt -StateBarHight , keyboardBack.frame.size.width, keyboardBack.frame.size.height);
//    
//    fillBoardBac.frame = CGRectMake(fillBoardBac.frame.origin.x, (keyboardBack.frame.origin.y - fillBoardBac.frame.size.height)/2, fillBoardBac.frame.size.width, fillBoardBac.frame.size.height);
    

//    NSUserDefaults *_ud =  [NSUserDefaults standardUserDefaults];
//    NSString *lockStr = [_ud objectForKey:LockKey];
//    if (lockStr)
//    {
//        mimi_firstInput = [[NSMutableString alloc] initWithString:lockStr];
//    }
//    else
//    {
        mimi_firstInput = [[NSMutableString alloc] init];
//    }
    
    mimi_confirm = [[NSMutableString alloc] init];
    
    mimi_inputImage = mimi_iputImagBack.subviews;
    self.navigationItem.leftBarButtonItem = nil;
    
    [self skin:nil];
//    NSLog(@"%f",kScreenHeight);
    if (kScreenHeight == 480) {
        m_ImageTopY.constant = -60;
        balckKeyIcon.hidden = YES;

    }else if (kScreenHeight >568){
        m_ImageTopY.constant = 64;
        balckKeyIcon.hidden = NO;
    }
}

-(void)viewWillAppear:(BOOL)animated{
//    [self.navigationController.navigationBar addSubview:mimi_backButton];
}

-(void)viewWillDisappear:(BOOL)animated{
//    [mimi_backButton removeFromSuperview];
}

-(void)hiddenBackButton:(BOOL)flag{

}

-(void)skin:(NSNotification *)sender{
    keyboardBack.backgroundColor = VCBackgroundColor;
    
    [zero setBackgroundColor:LockAppNomal];
    [zero setBackgroundImage:[UIImage imageNamed:@"KeyboardHighlight"] forState:UIControlStateHighlighted];
    [zero setTitleColor:LockButtonTextColor forState:UIControlStateNormal];
    [zero setTitleColor:LockButtonSelectedTextColor forState:UIControlStateHighlighted];
    
    [one setBackgroundColor:LockAppNomal];
    [one setBackgroundImage:[UIImage imageNamed:@"KeyboardHighlight"] forState:UIControlStateHighlighted];
    [one setTitleColor:LockButtonTextColor forState:UIControlStateNormal];
    [one setTitleColor:LockButtonSelectedTextColor forState:UIControlStateHighlighted];
    
    [two setBackgroundColor:LockAppNomal];
    [two setBackgroundImage:[UIImage imageNamed:@"KeyboardHighlight"] forState:UIControlStateHighlighted];
    [two setTitleColor:LockButtonTextColor forState:UIControlStateNormal];
    [two setTitleColor:LockButtonSelectedTextColor forState:UIControlStateHighlighted];
    
    [three setBackgroundColor:LockAppNomal];
    [three setBackgroundImage:[UIImage imageNamed:@"KeyboardHighlight"] forState:UIControlStateHighlighted];
    [three setTitleColor:LockButtonTextColor forState:UIControlStateNormal];
    [three setTitleColor:LockButtonSelectedTextColor forState:UIControlStateHighlighted];
    
    [four setBackgroundColor:LockAppNomal];
    [four setBackgroundImage:[UIImage imageNamed:@"KeyboardHighlight"] forState:UIControlStateHighlighted];
    [four setTitleColor:LockButtonTextColor forState:UIControlStateNormal];
    [four setTitleColor:LockButtonSelectedTextColor forState:UIControlStateHighlighted];
    
    [five setBackgroundColor:LockAppNomal];
    [five setBackgroundImage:[UIImage imageNamed:@"KeyboardHighlight"] forState:UIControlStateHighlighted];
    [five setTitleColor:LockButtonTextColor forState:UIControlStateNormal];
    [five setTitleColor:LockButtonSelectedTextColor forState:UIControlStateHighlighted];
    
    [six setBackgroundColor:LockAppNomal];
    [six setBackgroundImage:[UIImage imageNamed:@"KeyboardHighlight"] forState:UIControlStateHighlighted];
    [six setTitleColor:LockButtonTextColor forState:UIControlStateNormal];
    [six setTitleColor:LockButtonSelectedTextColor forState:UIControlStateHighlighted];
    
    [seven setBackgroundColor:LockAppNomal];
    [seven setBackgroundImage:[UIImage imageNamed:@"KeyboardHighlight"] forState:UIControlStateHighlighted];
    [seven setTitleColor:LockButtonTextColor forState:UIControlStateNormal];
    [seven setTitleColor:LockButtonSelectedTextColor forState:UIControlStateHighlighted];
    
    [eight setBackgroundColor:LockAppNomal];
    [eight setBackgroundImage:[UIImage imageNamed:@"KeyboardHighlight"] forState:UIControlStateHighlighted];
    [eight setTitleColor:LockButtonTextColor forState:UIControlStateNormal];
    [eight setTitleColor:LockButtonSelectedTextColor forState:UIControlStateHighlighted];
    
    [nine setBackgroundColor:LockAppNomal];
    [nine setBackgroundImage:[UIImage imageNamed:@"KeyboardHighlight"] forState:UIControlStateHighlighted];
    [nine setTitleColor:LockButtonTextColor forState:UIControlStateNormal];
    [nine setTitleColor:LockButtonSelectedTextColor forState:UIControlStateHighlighted];
    
    [ten setBackgroundColor:LockAppNomal];
    [ten setBackgroundImage:[UIImage imageNamed:@"KeyboardHighlight"] forState:UIControlStateHighlighted];
    
    [delete setBackgroundColor:LockAppNomal];
    [delete setBackgroundImage:[UIImage imageNamed:@"KeyboardHighlight"] forState:UIControlStateHighlighted];
    
    [deleteImageView setImage:[UIImage imageNamed:@"deleteSecret"]];
    
}


#pragma -mark IBAction

-(void)backToSuper:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

-(IBAction)lock:(UIButton *)sender
{
    INNLog(@"tag=%@",sender.titleLabel.text);
    INNLog(@"firstInput=:%lu",(unsigned long)mimi_firstInput.length);
    if (mimi_firstInput.length<6)
    {
        [mimi_firstInput appendString:sender.titleLabel.text];
        INNLog(@"=:%@",mimi_firstInput);
        
        for (UIView *_inputView in mimi_inputImage)
        {
            for(UIButton   *_inputFlag in _inputView.subviews)
            {
                if(_inputFlag.tag ==mimi_firstInput.length){
                    [_inputFlag setTitle:sender.titleLabel.text forState:UIControlStateNormal];
                }

            }
        }
        if (mimi_firstInput.length==6)
        {
            [self inputButtUserInter:NO];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
//                 sleep(1);

                 dispatch_async(dispatch_get_main_queue(), ^{
//                     [AlertMessageTool showMBProgressInView:self.view WithMessage:@"请再输入一次" withHiddenTime:1];
                     [self inputButtUserInter:YES];
                     [self getUserIdentifyNum];
                     
                });  
            });
        }
    }
}

-(IBAction)deleteLock:(id)sender
{
    if (m_allowDelete)
    {
        
        if (mimi_firstInput.length<=6&&mimi_firstInput.length>=1)
        {
            
            INNLog(@"first=:%@",mimi_firstInput);
            [self deleteStrAndImageState:mimi_firstInput];
        }
    }
}

-(void)deleteStrAndImageState:(NSMutableString *)str
{
//    [str deleteCharactersInRange:NSMakeRange(str.length-1,1)];
    for (UIView *_inputView in mimi_inputImage)
    {
        for(UIButton   *_inputFlag in _inputView.subviews)
        {
            if (_inputFlag.tag==str.length)
            {
                [_inputFlag setTitle:@"" forState:UIControlStateNormal];
                [str deleteCharactersInRange:NSMakeRange(str.length-1,1)];
            }
        }
    }

}


-(void)inputButtUserInter:(BOOL)flag
{
//    for (UIButton *_iputButt in mimi_inputButtBack.subviews)
//    {
//        _iputButt.userInteractionEnabled = flag;
//        
//    }

}
-(void)deleteLocationFromLocation
{
//    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
//    [ud removeObjectForKey:LockKey];
//    [ud synchronize];
}

-(void)userIdentifyEorr{
    pwsTagLabel.textColor = UserIdentityEorrTagColor;
    pwsTagLabel.text = @"验证码有误，请重新输入";
//    [pwsOneButton setBackgroundImage:[UIImage imageNamed:@"userIdentifyNumEorrBox"] forState:UIControlStateNormal];
//    [pwsTwoButton setBackgroundImage:[UIImage imageNamed:@"userIdentifyNumEorrBox"] forState:UIControlStateNormal];
//    [pwsThreeButton setBackgroundImage:[UIImage imageNamed:@"userIdentifyNumEorrBox"] forState:UIControlStateNormal];
//    [pwsFourButton setBackgroundImage:[UIImage imageNamed:@"userIdentifyNumEorrBox"] forState:UIControlStateNormal];
//    [pwsFiveButton setBackgroundImage:[UIImage imageNamed:@"userIdentifyNumEorrBox"] forState:UIControlStateNormal];
//    [pwsSixButton setBackgroundImage:[UIImage imageNamed:@"userIdentifyNumEorrBox"] forState:UIControlStateNormal];
    
    for (UIView *_inputView in mimi_inputImage)
    {
        for(UIButton   *_inputFlag in _inputView.subviews)
        {
            [_inputFlag setTitle:@"" forState:UIControlStateNormal];
            [_inputFlag setBackgroundImage:[UIImage imageNamed:@"userIdentifyNumEorrBox"] forState:UIControlStateNormal];
        }
    }
    [mimi_firstInput setString:@""];
}

- (void) getUserIdentifyNum{
    NSString *strUrl = [URLProvider get_activation_verification_code_url];
    
    NSDictionary *dictinary = [NSDictionary dictionaryWithObjectsAndKeys:
                               [[DeviceIdentifier sharedDeviceInfo] getDeviceID],@"uuid1",
                               [[DeviceIdentifier sharedDeviceInfo] theNewUUID],@"uuid2",
                               nil];
    
    NSMutableDictionary *posdic = [NSMutableDictionary dictionary];
    [posdic setObject:mimi_firstInput forKey:@"verification_code"];
    
    if (m_request == nil)
    {
        m_request = [[RequestDataWithHttp alloc]init];
        m_request.m_delegate = self;
    }
    [m_request requestDataWithHttphead:dictinary
                              httpBody:posdic
                               withURL:strUrl
                            HttpMethod:POSTMETHOD];

}

#pragma mark - RequestDataWithHttpDelegate
-(void)successBackData:(id)backNetObj topDownFlag:(BOOL)flag{
    INNLog(@"发送消息成功 ＝＝ %@",backNetObj);
    if ([[backNetObj objectForKey:@"err"]intValue] == 0)
    {
        NSString    *userName = [[backNetObj objectForKey:@"userName"]NSStringNull];
        if (userName.length > 0) {
            userName = [NSString stringWithFormat:@"%@,",userName];
        }
        
        m_welcomeUserName.text = userName;
        if ([[MyInfor shareMyInfor] get_user_id].length >0) {
            //老用户
            [[MyInfor shareMyInfor] setUserInfo:backNetObj];
            //                 }
        }else{
            //新用户
            if (backNetObj != nil) {
                [[MyInfor shareMyInfor] setUserInfo:backNetObj];
                [[TCP getTCPSingleton] openTcp];
            }else{
                INNLog(@"请求错误 无法获得用户id");
            }
        }
        
        m_welcomeBgView.hidden = NO;
        m_welcomeBgView.alpha = 0.0f;
        [UIView animateWithDuration:0.5f animations:^{
            m_welcomeBgView.alpha = 1.0f;
        } completion:^(BOOL finished) {
    
        }];
        [self performSelector:@selector(pleaseWait) withObject:nil afterDelay:1.5];
        
    

    }else{
        [self userIdentifyEorr];
    }
    
}

-(void)failBack:(id)failObj topDownFlag:(BOOL)flag{
    INNLog(@"发送消息失败 == %@",failObj);
    [self userIdentifyEorr];
}


-(void)pleaseWait{
    [self backToSuper:nil];
    //开启推送通知
    m_push = [[INNPushNoticeCenter alloc] init];
    [m_push openPushNotice];
}

-(void)dealloc
{
    keyboardBack = nil;
    fillBoardBac = nil;
    
//    if (![mimi_firstInput isEqualToString:mimi_confirm])
//    {
//        [self deleteLocationFromLocation];
//    }
    
    if (mimi_firstInput)
    {
        mimi_firstInput = nil;
    }
    
    mimi_iputImagBack = nil;
    if (mimi_inputImage)
    {
        mimi_inputImage = nil;
    }
    
    mimi_inputButtBack = nil;
    m_ImageTopY =nil;
    m_push = nil;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

//
//  LockAppViewController.h
//  Secrets
//
//  Created by 胡 庆贺 on 14-6-16.
//  Copyright (c) 2014年 huqinghe. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SuperViewController.h"

typedef enum{
    CommitPassword = 0,
    RemovedPssword,
    HasPassword
    
}LockStatus;


@interface LockAppViewController : UIViewController
{
@public
    BOOL m_allowDelete;
    NSString        *s_titleName;
    LockStatus      s_LockStatus;
}

-(void)hiddenBackButton:(BOOL)flag;
@end

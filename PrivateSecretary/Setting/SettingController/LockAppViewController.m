//
//  LockAppViewController.m
//  Secrets
//
//  Created by 胡 庆贺 on 14-6-16.
//  Copyright (c) 2014年 huqinghe. All rights reserved.
//

#import "LockAppViewController.h"
#import "DeviceInfor.h"
#import "AlertMessageTool.h"

@interface LockAppViewController ()
{
    IBOutlet UIView* keyboardBack;
    IBOutlet UIView* fillBoardBac;
    
    NSMutableString *mimi_firstInput;
    NSMutableString *mimi_confirm;
    
    IBOutlet UIView *mimi_iputImagBack;
    NSArray *mimi_inputImage;
    
    IBOutlet UIView *mimi_inputButtBack;
//    UIButton *mimi_backButton;
    // 数字按钮
    IBOutlet UIButton *zero;
    IBOutlet UIButton *one;
    IBOutlet UIButton *two;
    IBOutlet UIButton *three;
    IBOutlet UIButton *four;
    IBOutlet UIButton *five;
    IBOutlet UIButton *six;
    IBOutlet UIButton *seven;
    IBOutlet UIButton *eight;
    IBOutlet UIButton *nine;
    IBOutlet UIButton *ten;
    IBOutlet UIButton *delete;
    IBOutlet UIImageView *deleteImageView;
    
    /**
     *  秘密显示 imageView
     */
    IBOutlet UIImageView *pwsOneImageView;
    IBOutlet UIImageView *pwsTwoImageView;
    IBOutlet UIImageView *pwsThreeImageView;
    IBOutlet UIImageView *pwsFourImageView;
    
    IBOutlet    UIButton    *m_backToSuperBtn;
    IBOutlet    NSLayoutConstraint      *m_TagLabelY;
    IBOutlet    NSLayoutConstraint      *m_TagLabelBottomSpace;
    IBOutlet    NSLayoutConstraint      *m_errorLabelTopY;
    
    IBOutlet    UILabel     *tagLabel;
    IBOutlet    UILabel     *errorLabel;
}
-(IBAction)lock:(UIButton *)sender;
-(IBAction)backToSuper:(id)sender;
-(void)inputButtUserInter:(BOOL)flag;
-(IBAction)deleteLock:(id)sender;
-(void)deleteStrAndImageState:(NSMutableString *)str;
-(void)deleteLocationFromLocation;
@end

@implementation LockAppViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    keyboardBack.frame = CGRectMake(keyboardBack.frame.origin.x,[[UIScreen mainScreen] bounds].size.height - keyboardBack.frame.size.height -14 - NavigationgHihgt -StateBarHight , keyboardBack.frame.size.width, keyboardBack.frame.size.height);
    
    fillBoardBac.frame = CGRectMake(fillBoardBac.frame.origin.x, (keyboardBack.frame.origin.y - fillBoardBac.frame.size.height)/2, fillBoardBac.frame.size.width, fillBoardBac.frame.size.height);
    

    NSUserDefaults *_ud =  [NSUserDefaults standardUserDefaults];
    NSString *lockStr = [_ud objectForKey:LockKey];
    if (lockStr)
    {
        mimi_firstInput = [[NSMutableString alloc] initWithString:lockStr];
    }
    else
    {
        mimi_firstInput = [[NSMutableString alloc] init];
    }
                           
    mimi_confirm = [[NSMutableString alloc] init];
    
    mimi_inputImage = mimi_iputImagBack.subviews;
    self.navigationItem.leftBarButtonItem = nil;
    
    [self skin:nil];
    
    if (kScreenHeight == 480) {
        m_TagLabelY.constant = 50;
        m_TagLabelBottomSpace.constant = 20;
    }else if(kScreenHeight >=600){
        m_errorLabelTopY.constant = 30;
    }
    if (s_LockStatus == HasPassword) {
        tagLabel.text =@"请输入应用密码";
    }else if(s_LockStatus == CommitPassword){
        tagLabel.text =@"设置您的密码开启应用锁";
    }else if(s_LockStatus == RemovedPssword){
        tagLabel.text = @"输入密码后关闭应用锁";
    }
}

-(void)viewWillAppear:(BOOL)animated{
//    [self.navigationController.navigationBar addSubview:mimi_backButton];
}

-(void)viewWillDisappear:(BOOL)animated{
//    [mimi_backButton removeFromSuperview];
}

-(void)hiddenBackButton:(BOOL)flag{
    m_backToSuperBtn.hidden = flag;
}

-(void)skin:(NSNotification *)sender{
    keyboardBack.backgroundColor = VCBackgroundColor;
    
    [zero setBackgroundColor:LockAppNomal];
    [zero setBackgroundImage:[UIImage imageNamed:@"KeyboardHighlight"] forState:UIControlStateHighlighted];
    [zero setTitleColor:LockButtonTextColor forState:UIControlStateNormal];
    [zero setTitleColor:LockButtonSelectedTextColor forState:UIControlStateHighlighted];
    
    [one setBackgroundColor:LockAppNomal];
    [one setBackgroundImage:[UIImage imageNamed:@"KeyboardHighlight"] forState:UIControlStateHighlighted];
    [one setTitleColor:LockButtonTextColor forState:UIControlStateNormal];
    [one setTitleColor:LockButtonSelectedTextColor forState:UIControlStateHighlighted];
    
    [two setBackgroundColor:LockAppNomal];
    [two setBackgroundImage:[UIImage imageNamed:@"KeyboardHighlight"] forState:UIControlStateHighlighted];
    [two setTitleColor:LockButtonTextColor forState:UIControlStateNormal];
    [two setTitleColor:LockButtonSelectedTextColor forState:UIControlStateHighlighted];
    
    [three setBackgroundColor:LockAppNomal];
    [three setBackgroundImage:[UIImage imageNamed:@"KeyboardHighlight"] forState:UIControlStateHighlighted];
    [three setTitleColor:LockButtonTextColor forState:UIControlStateNormal];
    [three setTitleColor:LockButtonSelectedTextColor forState:UIControlStateHighlighted];
    
    [four setBackgroundColor:LockAppNomal];
    [four setBackgroundImage:[UIImage imageNamed:@"KeyboardHighlight"] forState:UIControlStateHighlighted];
    [four setTitleColor:LockButtonTextColor forState:UIControlStateNormal];
    [four setTitleColor:LockButtonSelectedTextColor forState:UIControlStateHighlighted];
    
    [five setBackgroundColor:LockAppNomal];
    [five setBackgroundImage:[UIImage imageNamed:@"KeyboardHighlight"] forState:UIControlStateHighlighted];
    [five setTitleColor:LockButtonTextColor forState:UIControlStateNormal];
    [five setTitleColor:LockButtonSelectedTextColor forState:UIControlStateHighlighted];
    
    [six setBackgroundColor:LockAppNomal];
    [six setBackgroundImage:[UIImage imageNamed:@"KeyboardHighlight"] forState:UIControlStateHighlighted];
    [six setTitleColor:LockButtonTextColor forState:UIControlStateNormal];
    [six setTitleColor:LockButtonSelectedTextColor forState:UIControlStateHighlighted];
    
    [seven setBackgroundColor:LockAppNomal];
    [seven setBackgroundImage:[UIImage imageNamed:@"KeyboardHighlight"] forState:UIControlStateHighlighted];
    [seven setTitleColor:LockButtonTextColor forState:UIControlStateNormal];
    [seven setTitleColor:LockButtonSelectedTextColor forState:UIControlStateHighlighted];
    
    [eight setBackgroundColor:LockAppNomal];
    [eight setBackgroundImage:[UIImage imageNamed:@"KeyboardHighlight"] forState:UIControlStateHighlighted];
    [eight setTitleColor:LockButtonTextColor forState:UIControlStateNormal];
    [eight setTitleColor:LockButtonSelectedTextColor forState:UIControlStateHighlighted];
    
    [nine setBackgroundColor:LockAppNomal];
    [nine setBackgroundImage:[UIImage imageNamed:@"KeyboardHighlight"] forState:UIControlStateHighlighted];
    [nine setTitleColor:LockButtonTextColor forState:UIControlStateNormal];
    [nine setTitleColor:LockButtonSelectedTextColor forState:UIControlStateHighlighted];
    
    [ten setBackgroundColor:LockAppNomal];
    [ten setBackgroundImage:[UIImage imageNamed:@"KeyboardHighlight"] forState:UIControlStateHighlighted];
    
    [delete setBackgroundColor:LockAppNomal];
    [delete setBackgroundImage:[UIImage imageNamed:@"KeyboardHighlight"] forState:UIControlStateHighlighted];
    
    [deleteImageView setImage:[UIImage imageNamed:@"deleteSecret"]];
    
//    [pwsOneImageView setImage:[UIImage imageNamed:@"PWSNomal"]];
    [pwsOneImageView setHighlightedImage:[UIImage imageNamed:@"PWShightLight"]];
    
//    [pwsTwoImageView setImage:[UIImage imageNamed:@"PWSNomal"]];
    [pwsTwoImageView setHighlightedImage:[UIImage imageNamed:@"PWShightLight"]];
    
//    [pwsThreeImageView setImage:[UIImage imageNamed:@"PWSNomal"]];
    [pwsThreeImageView setHighlightedImage:[UIImage imageNamed:@"PWShightLight"]];
    
//    [pwsFourImageView setImage:[UIImage imageNamed:@"PWSNomal"]];
    [pwsFourImageView setHighlightedImage:[UIImage imageNamed:@"PWShightLight"]];
}


#pragma -mark IBAction

-(void)backToSuper:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

-(IBAction)lock:(UIButton *)sender
{
    INNLog(@"tag=%@",sender.titleLabel.text);
    INNLog(@"firstInput=:%lu",(unsigned long)mimi_firstInput.length);
    if (mimi_firstInput.length<4)
    {
        [mimi_firstInput appendString:sender.titleLabel.text];
        INNLog(@"=:%@",mimi_firstInput);
        
        for (UIView *_inputView in mimi_inputImage)
        {
            for(UIImageView   *_inputFlag in _inputView.subviews)
            {
                _inputFlag.highlighted = _inputFlag.tag<=mimi_firstInput.length;
            }
        }
        if (mimi_firstInput.length==4)
        {
            [self inputButtUserInter:NO];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                 sleep(0.5);
                 dispatch_async(dispatch_get_main_queue(), ^{
//                     [AlertMessageTool showMBProgressInView:self.view WithMessage:@"请再输入一次" withHiddenTime:1];
                     tagLabel.text = @"请再次确认您的密码";

                     for (UIView *_inputView in mimi_inputImage)
                     {
                         for(UIImageView   *_nomal in _inputView.subviews)
                         {
                             _nomal.highlighted = NO;
                         }
                      }
                     [self inputButtUserInter:YES];
                });  
            });
        }
    }
    else
    {
        if (mimi_confirm.length<4)
        {
            [mimi_confirm appendString:sender.titleLabel.text];
            INNLog(@"confirm=:%@",mimi_confirm);
            for (UIView *_inputView in mimi_inputImage)
            {
                for(UIImageView   *_inputFlag in _inputView.subviews)
                {
                    _inputFlag.highlighted = _inputFlag.tag<=mimi_confirm.length;
                }
            }
            
            if (mimi_confirm.length==4)
            {
                if ([mimi_firstInput isEqualToString:mimi_confirm])
                {
                    NSUserDefaults *_ud = [NSUserDefaults standardUserDefaults];
                    if (s_LockStatus == RemovedPssword)
                    {
                        [_ud removeObjectForKey:LockKey];
//                        [AlertMessageTool showMBProgressInView:self.view.window WithMessage:@"密码锁已关闭" withHiddenTime:1];
//                        [[NSNotificationCenter defaultCenter]postNotificationName:@"RefrshLockSettingViewStatus" object:nil];
                    }else{
                        [_ud setObject:mimi_confirm forKey:LockKey];
                    }
                    [_ud synchronize];
        
                    [self backToSuper:nil];
                }
                else
                {
//                    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
//                    [ud setObject:mimi_firstInput forKey:LockKey];
//                    [ud synchronize];
                    if(s_LockStatus == RemovedPssword){
                      errorLabel.text = @"密码输入有误，请重试";
                    }else if (s_LockStatus == CommitPassword){
                        errorLabel.text = @"两次输入密码不一致，请重新输入";
                    }
                    
                    errorLabel.hidden = NO;
//                    [AlertMessageTool showMBProgressInView:self.view WithMessage:@"密码不一致,请重新输入" withHiddenTime:1];
                    [mimi_confirm deleteCharactersInRange:NSMakeRange(0, mimi_confirm.length)];
                    INNLog(@"confirm=:%@",mimi_confirm);
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        sleep(0.5);
                        dispatch_async(dispatch_get_main_queue(), ^{
                            for (UIView *_inputView in mimi_inputImage)
                            {
                                for(UIImageView   *_noma in _inputView.subviews)
                                {
                                    _noma.highlighted = NO;
                                    if (_noma.tag == -1) {
                                        [_noma setImage:[UIImage imageNamed:@"userIdentifyNumEorrBox"]];
                                    }
                                    
                                }
                            }
                        });
                    });
       
                }
            }

        }
    }
    
}
-(IBAction)deleteLock:(id)sender
{
    if (m_allowDelete)
    {
        
        if (mimi_firstInput.length<4&&mimi_firstInput.length>=1)
        {
            
            INNLog(@"first=:%@",mimi_firstInput);
            [self deleteStrAndImageState:mimi_firstInput];
        }
        else
        {
            if (mimi_confirm.length<4&&mimi_confirm.length>=1)
            {
                [self deleteStrAndImageState:mimi_confirm];
            }
        }
    }
}

-(void)deleteStrAndImageState:(NSMutableString *)str
{
    [str deleteCharactersInRange:NSMakeRange(str.length-1,1)];
    for (UIView *_inputView in mimi_inputImage)
    {
        for(UIImageView   *_noma in _inputView.subviews)
        {
            if (_noma.tag<=str.length)
            {
                _noma.highlighted = YES;
            }
            else
            {
                _noma.highlighted = NO;
            }
        }
    }

}


-(void)inputButtUserInter:(BOOL)flag
{
    for (UIButton *_iputButt in mimi_inputButtBack.subviews)
    {
        _iputButt.userInteractionEnabled = flag;
        
    }

}
-(void)deleteLocationFromLocation
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud removeObjectForKey:LockKey];
    [ud synchronize];
}
-(void)dealloc
{
    keyboardBack = nil;
    fillBoardBac = nil;
    
//    if (![mimi_firstInput isEqualToString:mimi_confirm])
//    {
//        [self deleteLocationFromLocation];
//    }
    
    if (mimi_firstInput)
    {
        mimi_firstInput = nil;
    }
    
    mimi_iputImagBack = nil;
    if (mimi_inputImage)
    {
        mimi_inputImage = nil;
    }
    
    mimi_inputButtBack = nil;
    m_TagLabelY =nil;
    m_TagLabelBottomSpace = nil;
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

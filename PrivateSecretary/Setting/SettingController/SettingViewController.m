//
//  SettingViewController.m
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/6.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import "SettingViewController.h"
#import "INNOTableView.h"

@interface SettingViewController ()<UITableViewDataSource,UITableViewDelegate,INNOTableViewToDownRefreshDelegate>
{
    INNOTableView       *m_tabel;
    NSMutableArray      *m_listArray;
}

@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    m_tabel = [[INNOTableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, [self.view getViewHigh]) style:UITableViewStylePlain adelegate:self topRefresh:NO bottomRefresh:NO];
    m_tabel.backgroundColor = VCBackgroundColor;
//    m_tabel.separatorStyle = UITableViewCellSeparatorStyleNone;
    m_tabel.dataSource = self;
    m_tabel.delegate = self;
    [self.view addSubview:m_tabel];
    
    m_listArray = [NSMutableArray arrayWithObjects:@"支付宝",@"微信支付",@"银联支付", nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return m_listArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *ReuCell = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ReuCell];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ReuCell];
    }
    cell.textLabel.text = [m_listArray objectAtIndex:indexPath.row];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:
            
            break;
        case 1:
            
            break;
        case 2:
            
            break;
            
        default:
            break;
    }
}

#pragma mark - INNOTableViewToDownRefreshDelegate
- (void)reloadTableViewDataSource{
    
}

-(void)dealloc{
    
}
@end

//
//  LockSettingView.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/9/6.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DeviceInfor.h"

@interface LockSettingView : UIView
{
    @public
    UIViewController    *m_superVC;
}

-(void)showLockSettingViewAnimation;
-(void)currentLockSettingStatusCommit;
-(void)currentLockSettingStatus;

@end

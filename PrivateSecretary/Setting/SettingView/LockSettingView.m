//
//  LockSettingView.m
//  PrivateSecretary
//
//  Created by 田沙 on 15/9/6.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import "LockSettingView.h"
#import "LoadViewFromXib.h"
#import "LockAppViewController.h"


@interface LockSettingView()
{
    IBOutlet    UIView      *m_lockOpenViewBg;
    IBOutlet    UIButton    *m_LockOpenStatusBg;
    IBOutlet    UIButton    *m_LockOpenStatus;
    IBOutlet    UILabel     *m_LockOpenStatusLabel;
    IBOutlet    UILabel     *m_LockOpenStatusTagLabel;
    
}

-(IBAction)pressCloseLockSettingViewButton:(id)sender;
-(IBAction)pressOpenLockButton:(id)sender;

@end

@implementation LockSettingView

-(id)init
{
    self = (LockSettingView *)[LoadViewFromXib loadView:@"LockSettingView" theClass:[LockSettingView class] owner:nil];
    self.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
    self.alpha = 0.0f;
    
    return self;
}

#pragma mark - IBAction
#pragma mark -关闭应用锁卡片view
-(void)pressCloseLockSettingViewButton:(id)sender
{
    [self closeLockSettingAnimation];
}

-(IBAction)pressOpenLockButtonDown:(id)sender{
    [UIView animateWithDuration:.1 animations:^{
        m_LockOpenStatusBg.transform = CGAffineTransformScale(m_LockOpenStatusBg.transform, 0.8, 0.8);
        m_LockOpenStatus.transform = CGAffineTransformScale(m_LockOpenStatus.transform, 0.8, 0.8);
    } completion:^(BOOL finished) {
        
    }];

}

-(IBAction)pressOpenLockButtonTouchUpOutSide:(id)sender{
    [UIView animateWithDuration:.1 animations:^{
        m_LockOpenStatusBg.transform = CGAffineTransformIdentity;
        m_LockOpenStatus.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        
    }];
}

#pragma mark -打开应用锁功能
-(void)pressOpenLockButton:(id)sender
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSString *str = [ud objectForKey:LockKey];
    
    if (str.length > 0)
    {
//        [self closeLockSettingAnimation];
        //输入密码后 确认无误删除密码锁
        LockAppViewController *vc = [[LockAppViewController alloc] initWithNibName:@"LockAppViewController" bundle:nil];
        vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        vc->s_titleName = @"";
        vc->m_allowDelete = YES;
        vc->s_LockStatus = RemovedPssword;
        [m_superVC presentViewController:vc animated:YES completion:^{
        }];
        
//        [ud removeObjectForKey:LockKey];
//        [self currentLockSettingStatus];
        
    }else
    {
//        [self closeLockSettingAnimation];
        LockAppViewController *vc = [[LockAppViewController alloc]initWithNibName:@"LockAppViewController" bundle:nil];
         vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        vc->s_titleName = @"";
        vc->m_allowDelete = YES;
        vc->s_LockStatus = CommitPassword;
        [m_superVC presentViewController:vc animated:YES completion:^{
          
        }];
    }
    [UIView animateWithDuration:.1 animations:^{
        m_LockOpenStatusBg.transform = CGAffineTransformIdentity;
        m_LockOpenStatus.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        
    }];
}

#pragma mark - 自定义方法

-(void)currentLockSettingStatus
{
    CGRect r = m_LockOpenStatusLabel.frame;
    r.size.height = 21;
    m_LockOpenStatusLabel.frame = r;
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSString *str = [ud objectForKey:LockKey];
    //启动过应用锁
    if (str.length > 0) {
        [m_LockOpenStatusBg setImage:[UIImage imageNamed:@"stopLockRoundBg"] forState:UIControlStateNormal];
        [m_LockOpenStatus setImage:[UIImage imageNamed:@"closeLock7"] forState:UIControlStateNormal];
        m_LockOpenStatusLabel.text = @"应用锁已启用";
        m_LockOpenStatusTagLabel.text = @"关闭应用锁，会对您的信息安全造成风险";
    }else{
        [m_LockOpenStatusBg setImage:[UIImage imageNamed:@"startLockRoundBg"] forState:UIControlStateNormal];
        [m_LockOpenStatus setImage:[UIImage imageNamed:@"openLock7"] forState:UIControlStateNormal];
        m_LockOpenStatusLabel.text = @"应用锁未启用";
        m_LockOpenStatusTagLabel.text = @"开启应用锁，可以更好的保护您的隐私";
    }
}

-(void)currentLockSettingStatusCommit
{
    
    CGRect r = m_LockOpenStatusLabel.frame;
    r.size.height = 84;
    m_LockOpenStatusLabel.frame = r;
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSString *str = [ud objectForKey:LockKey];
    //启动过应用锁
    if (str.length > 0) {
        [m_LockOpenStatusBg setImage:[UIImage imageNamed:@"stopLockRoundBg"] forState:UIControlStateNormal];
        [m_LockOpenStatus setImage:[UIImage animatedImageNamed:@"closeLock" duration:1.2] forState:UIControlStateNormal];
        [self performSelector:@selector(pleaseWait) withObject:nil afterDelay:1.2];
        m_LockOpenStatusLabel.text = @"您的应用锁已成功开启";
        m_LockOpenStatusTagLabel.text = @"";
    }else{
        [m_LockOpenStatusBg setImage:[UIImage imageNamed:@"startLockRoundBg"] forState:UIControlStateNormal];
        [m_LockOpenStatus setImage:[UIImage animatedImageNamed:@"openLock" duration:1.6] forState:UIControlStateNormal];
        [self performSelector:@selector(pleaseWait) withObject:nil afterDelay:1.6];
        m_LockOpenStatusLabel.text = @"您的应用锁已成功关闭";
        m_LockOpenStatusTagLabel.text = @"";
    }
    
    
}

-(void)showLockSettingViewAnimation
{
    
    [self currentLockSettingStatus];
    m_lockOpenViewBg.frame = CGRectMake(kScreenWidth/2-[m_lockOpenViewBg getViewWidth]/2, kScreenHeight, [m_lockOpenViewBg getViewWidth], [m_lockOpenViewBg getViewHigh]);
    
    [UIView animateWithDuration:.4 animations:^{
        self.alpha = 1.0f;
        m_lockOpenViewBg.frame = CGRectMake(kScreenWidth/2-[m_lockOpenViewBg getViewWidth]/2, kScreenHeight/2-[m_lockOpenViewBg getViewHigh]/2, [m_lockOpenViewBg getViewWidth], [m_lockOpenViewBg getViewHigh]);
    } completion:^(BOOL finished) {
       
    }];
}

-(void)closeLockSettingAnimation
{
    [UIView animateWithDuration:.3 animations:^{
        self.alpha = .0f;
        m_lockOpenViewBg.frame = CGRectMake(kScreenWidth/2-[m_lockOpenViewBg getViewWidth]/2, kScreenHeight, [m_lockOpenViewBg getViewWidth], [m_lockOpenViewBg getViewHigh]);
    } completion:^(BOOL finished) {
         [self removeFromSuperview];
    }];
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self closeLockSettingAnimation];
}


-(void)pleaseWait{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSString *str = [ud objectForKey:LockKey];
    //启动过应用锁
    if (str.length > 0) {
        [m_LockOpenStatus setImage:[UIImage imageNamed:@"closeLock7"] forState:UIControlStateNormal];
    }else{
        [m_LockOpenStatus setImage:[UIImage imageNamed:@"openLock7"] forState:UIControlStateNormal];
    }
     [self closeLockSettingAnimation];

}

-(void)dealloc{
    m_superVC = nil;
}
@end

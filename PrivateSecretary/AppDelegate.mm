//
//  AppDelegate.m
//  PrivateSecretary
//
//  Created by 田沙 on 15/7/29.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import "AppDelegate.h"
#import "ChatViewController.h"
#import "MainViewController.h"
#import "OLImage.h"
#import "OLImageView.h"
#import "Pingpp.h"
#import "LockAppViewController.h"
#import "CustomNavigationControllerViewController.h"
#import "AlertMessageTool.h"
#import "TCP.h"
#import "MyInfor.h"
#import "MobClick.h"
#import <Bugly/CrashReporter.h>
#import <CloudwiseMAgent/SmartAgent.h>
#import "INNPushNoticeCenter.h"
#import "UserIdentityVerificationController.h"


@interface AppDelegate ()
{
    ChatViewController      *m_chatVC;
    INNPushNoticeCenter     *m_push;
}
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    
    

    m_chatVC = [[ChatViewController alloc]initWithNibName:@"ChatViewController" bundle:nil];
    m_chatVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    CustomNavigationControllerViewController  *chatNav = [[CustomNavigationControllerViewController alloc]initWithRootViewController:m_chatVC];
    self.window.rootViewController = chatNav;
    [self.window makeKeyAndVisible];
    
    if ([[MyInfor shareMyInfor]get_user_id].length > 0) {
        [self lockApp];
    }else{
        [self gotoUserIdentify];
    }
    
    
    UIViewController *magicAnimatedVC = [UIViewController new];
    magicAnimatedVC.view.backgroundColor = VCBackgroundColor;
    magicAnimatedVC.view.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
    magicAnimatedVC.title = @"OLImageView";
    OLImageView *Aimv = [OLImageView new];
    Aimv->isOpenCustomization = YES;
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"welcome1" ofType:@"gif"];
    NSData *GIFDATA = [NSData dataWithContentsOfFile:filePath];
    Aimv.image = [OLImage imageWithData:GIFDATA];
    [Aimv setFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
    Aimv.contentMode = UIViewContentModeScaleAspectFill;
    [magicAnimatedVC.view addSubview:Aimv];
    [Aimv addOpenCustomizationView];
    
    [self.window addSubview:magicAnimatedVC.view];
    
//    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(gotoUserIdentify) name:NewUser object:nil];
    [Pingpp setDebugMode:YES];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {

}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    if ([[MyInfor shareMyInfor]get_user_id].length > 0) {
        [self lockApp];
    }
   [[TCP getTCPSingleton] closeTcp];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    application.applicationIconBadgeNumber = 0;
    
    [self performSelector:@selector(pleaseWaitOpenTcp) withObject:nil afterDelay:6.5];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    INNLog(@"崩溃.................");
    [[TCP getTCPSingleton] closeTcp];
}

-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSString *token = [[[[deviceToken description] stringByReplacingOccurrencesOfString:@"<" withString: @""]
                        stringByReplacingOccurrencesOfString: @">" withString: @""]
                       stringByReplacingOccurrencesOfString: @" " withString: @""];
    [[MyInfor shareMyInfor]setPushToken:token];
//    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
//    [user setObject:token forKey:@"ios_token"];
//    [user synchronize];
    
}

 - (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    [application registerForRemoteNotifications];
}

-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error{
    INNLog(@"err = %@",error);
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    [Pingpp handleOpenURL:url withCompletion:^(NSString *result, PingppError *error) {
        // result : success, fail, cancel, invalid
        NSString *msg;
        if (error == nil) {
           [[self getChatVC]getOrderStatus];
            
        } else {
            INNLog(@"PingppError: code=%lu msg=%@", (unsigned long)error.code, [error getMsg]);
            msg = [NSString stringWithFormat:@"result=%@ PingppError: code=%lu msg=%@", result, (unsigned long)error.code, [error getMsg]];
            INNLog(@"msg = %@",msg);
//            [AlertMessageTool showMBProgressInView:self.window WithMessage:msg withHiddenTime:1];
        }
        
   }];
    return  YES;
}

#pragma mark - 自定义方法
#pragma  mark -应用锁
-(void)lockApp
{
    NSUserDefaults *_ud = [NSUserDefaults standardUserDefaults];
    INNLog(@"locobj=:%@",[_ud objectForKey:LockKey]);
    if ([_ud objectForKey:LockKey])
    {
        LockAppViewController *vc = [[LockAppViewController alloc] initWithNibName:@"LockAppViewController" bundle:nil];
        vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        vc->s_titleName = @"";
        vc->m_allowDelete = YES;
        vc->s_LockStatus = HasPassword;
        [self.window.rootViewController presentViewController:vc animated:NO completion:^{
        }];
        [vc hiddenBackButton:YES];
    }
    
}

-(void)gotoUserIdentify{
    UserIdentityVerificationController *vc = [[UserIdentityVerificationController alloc] initWithNibName:@"UserIdentityVerificationController" bundle:nil];
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    vc->m_allowDelete = YES;
    [self.window.rootViewController presentViewController:vc animated:YES completion:^{
    }];

}

#pragma  mark -友盟统计

- (void)umengTrack
{
    [MobClick setCrashReportEnabled:NO]; // 如果不需要捕捉异常，注释掉此行
    [MobClick setLogEnabled:DebugFlag];  // 打开友盟sdk调试，注意Release发布时需要注释掉此行,减少io消耗
    [MobClick setAppVersion:XcodeAppVersion]; //参数为NSString * 类型,自定义app版本信息，如果不设置，默认从CFBundleVersion里取
    //
    [MobClick startWithAppkey:UMENG_APPKEY reportPolicy:(ReportPolicy) BATCH channelId:nil];
    //   reportPolicy为枚举类型,可以为 REALTIME, BATCH,SENDDAILY,SENDWIFIONLY几种
    //   channelId 为NSString * 类型，channelId 为nil或@""时,默认会被被当作@"App Store"渠道
    
    //      [MobClick checkUpdate];   //自动更新检查, 如果需要自定义更新请使用下面的方法,需要接收一个(NSDictionary *)appInfo的参数
    //    [MobClick checkUpdateWithDelegate:self selector:@selector(updateMethod:)];
    
    [MobClick updateOnlineConfig];  //在线参数配置
    
    //    1.6.8之前的初始化方法
    //    [MobClick setDelegate:self reportPolicy:REALTIME];  //建议使用新方法
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onlineConfigCallBack:) name:UMOnlineConfigDidFinishedNotification object:nil];
    
}

- (void)onlineConfigCallBack:(NSNotification *)note {
    
    NSLog(@"online config has fininshed and note = %@", note.userInfo);
}

-(ChatViewController *)getChatVC{
    return m_chatVC;
}

+(AppDelegate *)getDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

//为了优化启动速度 app启动后再打开TcP
-(void)pleaseWaitOpenTcp
{
    if([[MyInfor shareMyInfor]get_user_id].length >0){
        m_push = [[INNPushNoticeCenter alloc] init];
        [m_push openPushNotice];
        [[TCP getTCPSingleton] openTcp];
    }
    
    //统计
    [self umengTrack];
    
    [[CrashReporter sharedInstance] installWithAppId:@"GUqK6rSOfbs7G20z"];
    
    [[SmartAgent sharedInstance] startOnCloudWithAppKey:TOUSHIBAOKEY];
}
@end
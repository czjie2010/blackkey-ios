//
//  OLImageView.m
//  OLImageViewDemo
//
//  Created by Diego Torres on 9/5/12.
//  Copyright (c) 2012 Onda Labs. All rights reserved.
//

#import "OLImageView.h"
#import "OLImage.h"
#import <QuartzCore/QuartzCore.h>

@interface OLImageView ()

@property (nonatomic) NSUInteger currentFrameIndex;
@property (nonatomic) NSTimeInterval currentKeyframeElapsedTime;
@property (nonatomic) OLImage *animatedImage;
@property (nonatomic, strong) NSTimer *keyFrameTimer;
//@property (nonatomic, retain) UILabel   *s_logoLabel;
@property (nonatomic, retain) UIImageView   *s_logoImageView;

@end

@implementation OLImageView

@synthesize currentFrameIndex;
@synthesize s_logoImageView;
-(id)init
{
    self = [super init];
    if (self) {
        self.currentFrameIndex = 0;
        self.animatedImage = nil;
        self.keyFrameTimer = nil;
    }
    return self;
}


-(void)addOpenCustomizationView{
    if (isOpenCustomization) {
        //图片
        if(s_logoImageView == nil)
        {
            s_logoImageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"slogan"]];
            s_logoImageView.frame = CGRectMake(30,130, kScreenWidth-60, 20);
            s_logoImageView.contentMode = UIViewContentModeScaleAspectFit;
            s_logoImageView.alpha = .0f;
            [self.superview addSubview:s_logoImageView];
        }
        
//        if (s_logoLabel == nil) {
//            s_logoLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, kScreenHeight/2+40-20, kScreenWidth, 22)];
//            s_logoLabel.textAlignment = NSTextAlignmentCenter;
//            s_logoLabel.textColor = [UIColor whiteColor];
//            s_logoLabel.text = @"仅 为 万 分 一 的 你";
//            s_logoLabel.font = [UIFont fontWithName:@"STHeitiSC-Medium" size:22.0];
//            
//            s_logoLabel.alpha = .0f;
////            [self.superview addSubview:s_logoLabel];
//        }
    }
}
-(void)setImage:(UIImage *)image
{
    [self stopAnimating];
    self.currentFrameIndex = 0;
    self.animatedImage = nil;
    
    if ([image isKindOfClass:[OLImage class]] && image.images) {
        self.animatedImage = (OLImage *)image;
        self.layer.contents = (__bridge id)([(UIImage *)[self.animatedImage.images objectAtIndex:0] CGImage]);
        [self startAnimating];
    } else {
        [super setImage:image];
    }
}

-(BOOL)isAnimating {
    return (self.keyFrameTimer != nil);
}

-(void)stopAnimating
{
    if (!self.animatedImage) {
        [super stopAnimating];
        return;
    }
    
    if (self.keyFrameTimer) {
        [self.keyFrameTimer invalidate];
        self.keyFrameTimer = nil;
    }
}

-(void)startAnimating
{    
    if (!self.animatedImage) {
        [super startAnimating];
        return;
    }
    if (!self.keyFrameTimer) {
        self.keyFrameTimer = [NSTimer scheduledTimerWithTimeInterval:0.08 target:self selector:@selector(changeKeyframe) userInfo:nil repeats:YES];
    }
}

- (void)changeKeyframe
{
    self.currentKeyframeElapsedTime += self.keyFrameTimer.timeInterval;
    if (self.currentKeyframeElapsedTime >= self.animatedImage.frameDurations[self.currentFrameIndex]) {
        NSUInteger newIndex = self.currentFrameIndex + 1;
        //定制效果添加logo和文字
        if (isOpenCustomization) {
//            if (newIndex == 23) {
//                [UIView animateWithDuration:.5 animations:^{
//                   s_logoLabel.alpha = 1.0;
//                } completion:^(BOOL finished) {
//
//                }];
//            }else
            if(newIndex == 20){
                [UIView animateWithDuration:.8 animations:^{
                    s_logoImageView.alpha = 1.0;
                } completion:^(BOOL finished) {
                    
                }];
            }

        }
        if (newIndex >= [self.animatedImage.images count]) {
            newIndex = 0;
            if (!isRepeatsPlay) {

                [self performSelector:@selector(pleaseWait) withObject:nil afterDelay:0.8];
                return;
            }
       
        }
        self.currentFrameIndex = newIndex;
        self.currentKeyframeElapsedTime = 0.0f;
        [self.layer setNeedsDisplay];
    }
}

/****
 *  播放完成后停留1秒钟 消失
 ******/
-(void)pleaseWait{
    if (!isRepeatsPlay) {
        [UIView animateWithDuration:1.0 animations:^{
            self.superview.alpha = 0.0;
        } completion:^(BOOL finished) {
            
            [self performSelector:@selector(removeSuperView) withObject:nil afterDelay:1];
        }];
        
        return;
    }
    
}

-(void)removeSuperView
{
    [self.superview removeFromSuperview];
}

- (void)displayLayer:(CALayer *)layer {
    layer.contents = (__bridge id)([(UIImage *)[self.animatedImage.images objectAtIndex:self.currentFrameIndex] CGImage]);
}

- (void)didMoveToSuperview {
    if (!self.superview) {
        [self stopAnimating];
    } else {
        [self startAnimating];
    }
}

- (UIImage *)image {
    if (self.animatedImage != nil) {
        return self.animatedImage;
    }
    return [super image];
}

-(void)dealloc{
//    s_logoLabel = nil;
    s_logoImageView = nil;
}
@end

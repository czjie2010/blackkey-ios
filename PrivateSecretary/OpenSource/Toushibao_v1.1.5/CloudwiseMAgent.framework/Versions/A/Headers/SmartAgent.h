//
//  SmartAgent.h
//  CloudwiseMAgent
//
//  Created by Alvin on 14/1/17.
//  Copyright (c) 2014年 cloudwise. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SmartAgent : NSObject
/**
 * 获取单利方法
 */
+ (instancetype)sharedInstance;

/**
 * 通过appkey启动sdk
 * param appKey：从portal.toushibao.com获取到的appkey
 */
- (void)startOnCloudWithAppKey:(NSString *)appKey;

/**
 * 通过appkey启动sdk
 * param appKey：从portal.toushibao.com获取到的appkey
 * param traceH5：是否监控H5页面用户行为。如果默认监控H5页面用户行为，请用上面的接口。
 */
- (void)startOnCloudWithAppKey:(NSString *)appKey traceH5:(BOOL)traceH5;

/**
 * 记录事件。稍后推出用户自定义事件，暂不支持用户调用。
 */
- (void)recordEvent:(NSString *)key segmentation:(NSDictionary *)segmentation count:(int)count;

@end

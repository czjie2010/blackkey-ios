//
//  UpYunModel.m
//  Secrets
//
//  Created by 胡庆贺 on 15/3/6.
//  Copyright (c) 2015年 tiansha. All rights reserved.
//
/**
 {
  = 200;
  = ok;
  = 68f4b02f2559111753274d6403e5401a;
 time = 1425608702;
  }
 **/
#import "UpYunModel.h"
#import "UpYunPath.h"
@interface UpYunModel()
{
    NSString *mimi_code;
    NSString *mimi_message;
    NSString *mimi_sign;
    NSString *mimi_url;
}

@end

@implementation UpYunModel

-(void)setProperty:(NSDictionary *)adic
{
    mimi_code = [[NSString stringWithFormat:@"%@",[adic objectForKey:@"code"]] copy];
    mimi_message = [[NSString stringWithFormat:@"%@",[adic objectForKey:@"message"]] copy];
    mimi_sign = [[NSString stringWithFormat:@"%@",[adic objectForKey:@"sign"]] copy];
    mimi_url = [[NSString stringWithFormat:@"%@",[adic objectForKey:@"url"]] copy];
    
}


-(NSString *)getNetUrl
{
    return [NSString stringWithFormat:@"%@%@",@"http://secret-sound.b0.upaiyun.com",mimi_url];
}

-(BOOL)getNetFlag
{
    return [mimi_code isEqualToString:@"200"];
}

@end

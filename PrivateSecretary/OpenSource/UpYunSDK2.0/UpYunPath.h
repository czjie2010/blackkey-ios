//
//  UpYunPath.h
//  Secrets
//
//  Created by 胡庆贺 on 15/3/5.
//  Copyright (c) 2015年 tiansha. All rights reserved.
//

#import <Foundation/Foundation.h>
#define UpYunSoundKey @"1z140I53/+PI4A4e8dZo6vdQ+yU="
#define UpYunSoundBUCKET @"secret-sound"

@interface UpYunPath : NSObject
+(NSString *)getUpsoundUpYunPath;
+(NSString *)getUpPicPath;
+(NSTimeInterval)getTimeOut;
@end

//
//  UpYunModel.h
//  Secrets
//
//  Created by 胡庆贺 on 15/3/6.
//  Copyright (c) 2015年 tiansha. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UpYunModel : NSObject
-(void)setProperty:(NSDictionary *)adic;
-(NSString *)getNetUrl;
-(BOOL)getNetFlag;
@end

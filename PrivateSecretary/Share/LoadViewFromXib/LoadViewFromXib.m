//
//  LoadViewFromXib.m
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/5.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import "LoadViewFromXib.h"

@implementation LoadViewFromXib

+(UIView *)loadView:(NSString *)xib
           theClass:(Class)aClass
              owner:(NSObject *)owner
{
    NSArray *viewArray = [[NSBundle mainBundle] loadNibNamed:xib owner:owner options:nil];;
    
    for (UIView * tempView in viewArray)
    {
        if ([tempView isKindOfClass:aClass])
        {
            return tempView;
        }
    }
    
    return nil;
}


@end

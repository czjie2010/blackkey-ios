//
//  LoadViewFromXib.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/5.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoadViewFromXib : NSObject

+(UIView *)loadView:(NSString *)xib
           theClass:(Class)aClass
              owner:(NSObject *)owner;

@end

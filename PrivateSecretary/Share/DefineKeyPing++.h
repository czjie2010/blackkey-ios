//
//  DefineKeyPing++.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/9/11.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#ifndef PrivateSecretary_DefineKeyPing___h
#define PrivateSecretary_DefineKeyPing___h


#define KBtn_width        200
#define KBtn_height       40
#define KXOffSet          (self.view.frame.size.width - KBtn_width) / 2
#define KYOffSet          20

#define kWaiting          @"正在获取支付凭据,请稍后..."
#define kNote             @"提示"
#define kConfirm          @"确定"
#define kErrorNet         @"网络错误"
#define kResult           @"支付结果：%@"

#define kPlaceHolder      @"支付金额"
#define kMaxAmount        9999999

#define kUrlScheme      @"wx9d4ea34762f6a135" //@"PrivateSecretaryZhanghao" // 这个是你定义的 URL Scheme，支付宝、微信支付和测试模式需要。
#define kUrl            @"http://42.121.145.76:9998/pay/api/get_pingpp_order/" // 你的服务端创建并返回 charge 的 URL 地址，此地址仅供测试用。


#endif

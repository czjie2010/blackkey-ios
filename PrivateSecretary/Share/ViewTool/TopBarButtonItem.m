//
//  PostItem.m
//  Secrets
//
//  Created by 胡 庆贺 on 14-6-12.
//  Copyright (c) 2014年 huqinghe. All rights reserved.
//

#import "TopBarButtonItem.h"

@implementation TopBarButtonItem
+(UIBarButtonItem *)barButtonItemBackgroundImage:(NSString *)imageName
                                 select:(SEL)aselect
                                     VC:(UIViewController *)avc
                                  frame:(CGRect)afrme
                                  title:(NSString *)title
                              titleFont:(UIFont *)titleFont
                             titleColor:(UIColor *)titleColor
                                 offset:(int)offset
{
    UIButton *buttonItem = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonItem addTarget:avc action:aselect forControlEvents:UIControlEventTouchUpInside];
    buttonItem.frame = afrme;
    if (imageName)
    {
//        if (offset==TopItemFlag)
//        {
//            [buttonItem setImage:[UIImage secretsImageName:imageName] forState:UIControlStateNormal];
//        }
//        else
//        {
//            [buttonItem setBackgroundImage:[UIImage secretsImageName:imageName] forState:UIControlStateNormal];
//        }
        [buttonItem setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    }
    if (title)
    {
        [buttonItem setTitle:title forState:UIControlStateNormal];

    }
    if (titleFont)
    {
        [buttonItem.titleLabel setFont:titleFont];

    }
    if (titleColor)
    {
        [buttonItem setTitleColor:titleColor forState:UIControlStateNormal];

    }
    
    if (offset)
    {
        UIView *tempViewForBarButtonView = [[UIView alloc] init];
        tempViewForBarButtonView.backgroundColor = [UIColor grayColor];
        tempViewForBarButtonView.frame = CGRectMake(offset, 0, afrme.size.width, afrme.size.height);
        
        buttonItem.frame = CGRectMake(0, 0, buttonItem.frame.size.width, buttonItem.frame.size.height);
        buttonItem.backgroundColor = [UIColor redColor];
        tempViewForBarButtonView.userInteractionEnabled = YES;
        [tempViewForBarButtonView addSubview:buttonItem];
        
        UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:tempViewForBarButtonView];
        
        
         return barButtonItem;
        
    }
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:buttonItem];
    
    return barButtonItem;
    
}

+(UIButton *)buttonItemBackgroundImage:(NSString *)imageName
                                 select:(SEL)aselect
                                     VC:(UIViewController *)avc
                                  frame:(CGRect)afrme
                                  title:(NSString *)title
                              titleFont:(UIFont *)titleFont
                             titleColor:(UIColor *)titleColor
                                 offset:(int)offset
{
    UIButton *buttonItem = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonItem addTarget:avc action:aselect forControlEvents:UIControlEventTouchUpInside];
    buttonItem.frame = afrme;
    if (imageName)
    {
        [buttonItem setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    }
    if (title)
    {
        [buttonItem setTitle:title forState:UIControlStateNormal];
        
    }
    if (titleFont)
    {
        [buttonItem.titleLabel setFont:titleFont];
        
    }
    if (titleColor)
    {
        [buttonItem setTitleColor:titleColor forState:UIControlStateNormal];
        
    }
    
    if (offset)
    {
        buttonItem.frame = CGRectMake(offset, 0, buttonItem.frame.size.width, buttonItem.frame.size.height);
//        buttonItem.backgroundColor = [UIColor redColor];
//        tempViewForBarButtonView.userInteractionEnabled = YES;
//        [tempViewForBarButtonView addSubview:buttonItem];
        
//        UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:tempViewForBarButtonView];
        
        
        return buttonItem;
        
    }
//    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:buttonItem];
    
    return buttonItem;
    
}

@end

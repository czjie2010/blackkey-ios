//
//  PostItem.h
//  Secrets
//
//  Created by 胡 庆贺 on 14-6-12.
//  Copyright (c) 2014年 huqinghe. All rights reserved.
//

#import <Foundation/Foundation.h>
#define TopItemFlag  100000
@interface TopBarButtonItem : NSObject
+(UIBarButtonItem *)barButtonItemBackgroundImage:(NSString *)imageName
                                 select:(SEL)aselect
                                     VC:(UIViewController *)avc
                                  frame:(CGRect)afrme
                                  title:(NSString *)title
                              titleFont:(UIFont *)titleFont
                             titleColor:(UIColor *)titleColor
                                 offset:(int)offset;

+(UIButton *)buttonItemBackgroundImage:(NSString *)imageName
                                select:(SEL)aselect
                                    VC:(UIViewController *)avc
                                 frame:(CGRect)afrme
                                 title:(NSString *)title
                             titleFont:(UIFont *)titleFont
                            titleColor:(UIColor *)titleColor
                                offset:(int)offset;
@end

//
//  DeviceInfor.m
//  Secrets
//
//  Created by tiansha on 14-5-26.
//  Copyright (c) 2014年 tiansha. All rights reserved.
//

#import "DeviceInfor.h"

@implementation DeviceInfor


+(float)getMaxViewcontrollerViewHight
{
    return [[UIScreen mainScreen] bounds].size.height - NavigationgHihgt - StateBarHight - TabBarHight;
}

+(float)getMaxHiddenTabBarViewHight
{
    return [[UIScreen mainScreen] bounds].size.height - NavigationgHihgt - StateBarHight;
}

+(CGRect)getMaxViewControllerViewFrame
{
    return CGRectMake(0, 0,kScreenWidth, [DeviceInfor getMaxViewcontrollerViewHight]);
}


+(CGRect)getHiddenBottomMaxViewControllerFrame
{
    return CGRectMake(0, 0,kScreenWidth, [DeviceInfor getMaxHiddenTabBarViewHight]);
}


+(CGRect)getMainWindowIsHaveStateBar:(BOOL)flag
{
    int windowHeight = 0;
    if (flag)
    {
        windowHeight = [[UIScreen mainScreen] bounds].size.height-StateBarHight;
    }
    else
    {
        windowHeight = [[UIScreen mainScreen] bounds].size.height;
    }
    
    int windowWith = [[UIScreen mainScreen] bounds].size.width;
    return CGRectMake(0, 0, windowWith, windowHeight);
    
}

+(int)getIOSVersion
{
    return [[UIDevice currentDevice] systemVersion].intValue;
}

+(NSString *)getLange
{
    //[UIDevice currentDevice];
    return @"zh-hans";
}


+(BOOL)getIsGreateThanIphone4s
{
   return  [[UIScreen mainScreen] bounds].size.height>480;
}
@end

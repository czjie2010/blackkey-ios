//
//  DeviceInfor.h
//  Secrets
//
//  Created by tiansha on 14-5-26.
//  Copyright (c) 2014年 tiansha. All rights reserved.
//

#import <Foundation/Foundation.h>

#define NavigationgHihgt 44
#define TabBarHight 49
#define StateBarHight 20


@interface DeviceInfor : NSObject

+(float)getMaxViewcontrollerViewHight;

+(float)getMaxHiddenTabBarViewHight;

+(CGRect)getMaxViewControllerViewFrame;

+(CGRect)getHiddenBottomMaxViewControllerFrame;

+(CGRect)getMainWindowIsHaveStateBar:(BOOL)flag;
+(int)getIOSVersion;
+(NSString *)getLange;
+(BOOL)getIsGreateThanIphone4s;
@end

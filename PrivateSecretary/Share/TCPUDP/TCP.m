
//  TCP.m
//  Secrets
//
//  Created by 胡 庆贺 on 14-6-30.
//  Copyright (c) 2014年 huqinghe. All rights reserved.
//

#import "TCP.h"
#import "ShareDefine.h"
#import "AsyncSocket.h"
#import "ChatViewController.h"
#import "TCPSegmentation.h"
#import "PlaySound.h"
#import "RequestDataWithHttp.h"
#import "RequestDataWithHttpDelegate.h"
#import "MyInfor.h"

#import "TCPUDPCONFIG.h"
#import "JSONKit.h"


@interface TCP()<AsyncSocketDelegate,RequestDataWithHttpDelegate>
{
    AsyncSocket         *m_tcpAsyncSocket;
    NSMutableString     *m_tcpBackStr;
//    ChataDataManager *chatData;
    PlaySound           *m_playSound;
    
    NSMutableArray      *m_reportReceive;
    NSMutableString     *m_queueId;
    
    NSMutableDictionary *m_tcpWriteData;
    
    int         connectionNum;
    BOOL        isClientDisconnection;
}

-(NSData *)getTcpStreamData;

@end


@implementation TCP

+(TCP *)getTCPSingleton
{
    static TCP *tcp = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        INNLog(@"only TCP");
        tcp = [[self alloc] init];
        tcp->m_playSound = [[PlaySound alloc] init];
    });
    return tcp;
}


-(BOOL)openTcp
{
    [self closeTcp];
    
//    chatData = [[ChataDataManager alloc] init];
  
    m_tcpBackStr = [[NSMutableString alloc] init];
    
    m_tcpAsyncSocket = [[AsyncSocket alloc] initWithDelegate:self];
    
//    if (![[MyInfor shareMyInfor]get_user_id].length>0){
//        return NO;
//    }
    
    NSError *error;
    BOOL flag  = [m_tcpAsyncSocket connectToHost:TCPAddressNew onPort:TCPPortNew error:&error];
    
    INNLog(@"TCPflag==:%d",flag);
    [m_tcpAsyncSocket writeData:[[TCP getTCPSingleton] getTcpStreamData] withTimeout:-1 tag:1235];
    [m_tcpAsyncSocket readDataWithTimeout:-1 tag:1235];
    
    return flag;
    return YES;
}



-(void)closeTcp
{
    if (m_tcpAsyncSocket)
    {
        [m_tcpAsyncSocket setDelegate:nil];
        [m_tcpAsyncSocket disconnect];
        m_tcpAsyncSocket = nil;
    }
    if (m_tcpBackStr)
    {
        m_tcpBackStr = nil;
    }
    isClientDisconnection = YES;
}

-(void)senderTcp:(NSDictionary *)senderDic
{
    
}

-(void)onSocket:(AsyncSocket *)sock willDisconnectWithError:(NSError *)err
{
    INNLog(@"willDisconnectWithError-1");
}

- (void)onSocketDidDisconnect:(AsyncSocket *)sock
{
    
    INNLog(@"onSocketDidDisconnect-1");
    isClientDisconnection = NO;
    if (connectionNum < 5) {
        connectionNum++;
        [self openTcp];
    }  
}
- (BOOL)onSocketWillConnect:(AsyncSocket *)sock
{
    INNLog(@"onSocketWillConnect = %d",sock.isConnected);
    return YES;
}

- (void)onSocket:(AsyncSocket *)sock didConnectToHost:(NSString *)host port:(UInt16)port
{
    INNLog(@"didConnectToHost === %d",sock.isConnected);
}

- (void)onSocket:(AsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag
{
    [m_tcpBackStr deleteCharactersInRange:NSMakeRange(0, m_tcpBackStr.length)];
    INNLog(@"Tcp aferback=:%@",m_tcpBackStr);
    [m_tcpBackStr appendString:[NSString stringWithUTF8String:[data bytes]]];
    
    NSMutableArray *tcpBackDataArray = [m_tcpBackStr objectFromJSONString];


    TCPSegmentation *tcpSegmentation = [[TCPSegmentation alloc] init];
    [tcpSegmentation segmentTCPArrayPackagToOBJ:tcpBackDataArray];

    
    tcpSegmentation = nil;
    tcpBackDataArray = nil;
    

    [m_tcpAsyncSocket readDataWithTimeout:-1 tag:1235];
    
//    [self openTcp];
//    [m_tcpAsyncSocket disconnect];
    
    
    //[[UDPHeartbeat getHeartbeatSingleton] openUDP];
    
 }

-(void)onSocket:(AsyncSocket *)sock didReadPartialDataOfLength:(NSUInteger)partialLength tag:(long)tag{
    INNLog(@"leght======%d",(int)partialLength);
}

- (void)onSocket:(AsyncSocket *)sock didWriteDataWithTag:(long)tag
{
    INNLog(@"didWriteDataWithTag");
    //[[[AppDelegate getDelegate] getMessageVC] refreshData];

}


-(void)successBackData:(id)backNetObj topDownFlag:(BOOL)flag
{

    
}
-(void)failBack:(id)failObj topDownFlag:(BOOL)flag
{

}

-(void)setQueueId:(NSString *)queueId
{
    if (!m_queueId)
    {
        m_queueId = [[NSMutableString alloc] init];
    }
    [m_queueId deleteCharactersInRange:NSMakeRange(0, m_queueId.length)];
    [m_queueId appendString:queueId];
}



-(NSData *)getTcpStreamData
{
    if (!m_tcpWriteData)
    {
        m_tcpWriteData = [[NSMutableDictionary alloc] init];
    }
    [m_tcpWriteData removeAllObjects];
    
    if ([[MyInfor shareMyInfor] get_user_id])
    {
        NSInteger uid = [[[MyInfor shareMyInfor]get_user_id]integerValue];
        NSNumber *num = [NSNumber numberWithInteger:uid];
        [m_tcpWriteData setObject:num forKey:@"user_id"];
    }
//    if (m_queueId.length)
//    {
//        [m_tcpWriteData setObject:m_queueId forKey:@"queue_id"];
//    }
    
    return [m_tcpWriteData JSONData];
    
}

-(BOOL)getTcpContentIng
{
    return m_tcpAsyncSocket.isConnected;
}


@end

//
//  TCPSegmentation.m
//  Secrets
//
//  Created by 胡 庆贺 on 14-7-18.
//  Copyright (c) 2014年 huqinghe. All rights reserved.
//


#import "TCPUDPCONFIG.h"
#import "ShareDefine.h"
#import "TCPSegmentation.h"
#import "ChatFriendsAndMineModel.h"
#import "AppDelegate.h"
#import "PlaySound.h"
#import "ChatViewController.h"
#import "MyInfor.h"

@interface TCPSegmentation()
{
    
}

-(NSMutableDictionary *)codIng:(NSDictionary *)dic;//编码 dic
-(NSMutableDictionary *)moreCodIng:(NSDictionary *)dic;
-(BOOL)createSavePath:(NSString *)path;

@end

@implementation TCPSegmentation

-(void)segmentTCPArrayPackagToOBJ:(NSArray *)tcpArray
{
    @try {
        
//        NSMutableArray *chatSavedArray = [NSMutableArray array];
//        NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:@"content",@"content",@"5",@"message_type",nil];
//        [chatSavedArray addObject:dict];
        
//        tcpArray = chatSavedArray;
        INNLog(@"tcp 消息 ＝＝＝%@",tcpArray);
        int chatNumber = 0;
        
        for (NSDictionary *tcpDic in tcpArray)
        {
            NSString *typeFlag = [NSString stringWithFormat:@"%@",[tcpDic objectForKey:@"message_type"]];
            
            if([typeFlag intValue] == 5)
            {
                //更新ui状态 客服人员正在数据
                
                [[[AppDelegate getDelegate]getChatVC]getServiceInPutStatus];
                /*******
                 无需数据处理
                 直接return 中断数据处理
                 *********/
                return;
                
            }else if([typeFlag intValue] == 6)
            {
                //发送定位
                [[[AppDelegate getDelegate]getChatVC]sendLocation];
                return;
                
            }else if ([typeFlag intValue] == 3)
            {
                /*****
                 订单
                 ChatOrderCoument
                 *********/
                NSMutableDictionary  *orderDict = [self moreCodIng:tcpDic];
                 NSString * saveOrderPath =  [self getSavePath:ChatOrderCoument];
                BOOL fileOrderExis = [self haveFileAtPath:saveOrderPath];
                if (fileOrderExis)
                {
                    
                    NSMutableDictionary *savedOrderDict = [NSMutableDictionary dictionaryWithContentsOfFile:saveOrderPath];
                    [savedOrderDict setObject:orderDict forKey:[orderDict objectForKey:@"id"]];
                    
                    BOOL flag = [savedOrderDict writeToFile:saveOrderPath atomically:YES];
                    if (flag)
                    {
                        INNLog(@"订单写入成功");
                    }else
                    {
                        INNLog(@"订单写入失败");
                    }
                    
                }
                else
                {
                    BOOL createpath =  [self createSavePath:ChatOrderCoument];
                    
                    if (createpath)
                    {
                        NSMutableDictionary *savedOrderDict = [NSMutableDictionary dictionary];
                        [savedOrderDict setObject:orderDict forKey:[orderDict objectForKey:@"id"]];
                        
                        BOOL flag = [savedOrderDict writeToFile:saveOrderPath atomically:YES];
                        
                        if (flag)
                        {
                            INNLog(@"第一次存储订单写入成功");
                        }
                        else
                        {
                            INNLog(@"第一次写入订单失败");
                        }
                    }
                    else
                    {
                        INNLog(@"第一次存储订单的路径创建失败");
                    }
                }
                
                //刷新订单数据对象
                [[[AppDelegate getDelegate]getChatVC]getOrderRloadData:orderDict];
            }
            
            /*************************订单存储********************/
            
            /*****
             聊天
             ********/
            [[[AppDelegate getDelegate]getChatVC]stopInPutStatus];
            NSMutableDictionary *willSaveSecretDic;
            if ([typeFlag intValue] == 3){
                willSaveSecretDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:[tcpDic objectForKey:@"message_type"],@"message_type",[tcpDic objectForKey:@"id"],@"order_id", nil];
                
            }else{
                willSaveSecretDic = [self codIng:tcpDic];
            }
            chatNumber++;
            
            NSString * saveChatPath =  [self getSavePath:ChatCoument];
            BOOL fileExis = [self haveFileAtPath:saveChatPath];
            if (fileExis)
            {
                
                NSMutableArray *savedSecretArray = [NSMutableArray arrayWithContentsOfFile:saveChatPath];
                //防止丢消息
                NSString *mid = [willSaveSecretDic objectForKey:@"mid"];
                if (savedSecretArray.count>10) {
                    for (int i=(int)savedSecretArray.count-10; i< [savedSecretArray count]; i++) {
                        NSDictionary *dict_message = [savedSecretArray objectAtIndex: i];
                        if ([mid isEqualToString:[dict_message objectForKey:@"mid"]]) {
                            return;
                        }
                    }
                    
                }
                
                //记录是服务器来的消息
                [willSaveSecretDic setObject:@"1" forKey:@"from"];
                //标记为未读消息
                [willSaveSecretDic setObject:@"0" forKey:@"read_status"];
                
                [savedSecretArray addObject:willSaveSecretDic];
                
                BOOL flag = [savedSecretArray writeToFile:saveChatPath atomically:YES];
                if (flag)
                {
                    INNLog(@"聊天相关写入成功");
                }else
                {
                    INNLog(@"聊天相关写入失败");
                }
                
            }
            else
            {
                BOOL createpath =  [self createSavePath:ChatCoument];
                
                if (createpath)
                {
                    
                    //记录是服务器来的消息
                    [willSaveSecretDic setObject:@"1" forKey:@"from"];
                    //标记为未读消息
                    [willSaveSecretDic setObject:@"0" forKey:@"read_status"];
                    
                    NSMutableArray *secretSave = [NSMutableArray arrayWithObject:willSaveSecretDic];
                    
                    BOOL flag = [secretSave writeToFile:saveChatPath atomically:YES];
                    
                    if (flag)
                    {
                        INNLog(@"第一次存储聊天写入成功");
                    }
                    else
                    {
                        INNLog(@"第一次写入聊天失败");
                    }
                }
                else
                {
                    INNLog(@"第一次存储聊天的路径创建失败");
                }
            }
            
            //刷新veiw 收到后现实数据
            [[[AppDelegate getDelegate]getChatVC]getMessageRloadData:willSaveSecretDic];
            /**********************else 聊天部分****************************/
            if (chatNumber)
            {
                PlaySound *play=  [[PlaySound alloc] init];
                [play playSound];
            }
        }
        
    }
    @catch (NSException *exception) {
        INNLog(@"消息数据接受异常");
    }
}

-(NSMutableDictionary *)codIng:(NSDictionary *)dic
{
    NSMutableDictionary *condingDic = [NSMutableDictionary dictionary];
    for (NSString *keys in dic.allKeys)
    {
        [condingDic setObject:[NSString stringWithFormat:@"%@",[dic objectForKey:keys]] forKey:keys];
    }
    
    return condingDic;
}

-(NSMutableDictionary *)moreCodIng:(NSDictionary *)dic
{
    NSMutableDictionary *condingDic = [NSMutableDictionary dictionary];
    for (NSString *keys in dic.allKeys)
    {
        NSLog(@"key =====%@",keys);
        if ([[dic objectForKey:keys] isKindOfClass:[NSDictionary class]]) {
            
            [condingDic setObject:[self moreCodIng:[dic objectForKey:keys]] forKey:keys];
            
        }else{
            [condingDic setObject:[NSString stringWithFormat:@"%@",[dic objectForKey:keys]] forKey:keys];
        }
    }
    
    return condingDic;
}

-(NSMutableDictionary *)readFromPath:(NSString *)path
{
    NSMutableDictionary *readDic = [NSMutableDictionary dictionaryWithContentsOfFile:path];
    return readDic;
}

-(NSMutableArray *)readArrayFromPath:(NSString *)path
{
    NSMutableArray *array = [NSMutableArray arrayWithContentsOfFile:path];
    return array;
}

-(BOOL)createSavePath:(NSString *)path
{
    NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *chatPacket = [NSString stringWithFormat:@"%@/%@/%@/%@",documentDir,[[MyInfor shareMyInfor]get_user_id],path,path];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:chatPacket])
    {
        BOOL createFlag =  [fileManager createDirectoryAtPath:chatPacket withIntermediateDirectories:YES attributes:nil error:nil];
        return createFlag;
    }
    else
    {
        return YES;
    }
}


-(NSString *)getSavePath:(NSString *)saveKey
{
    NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *chatPacket = [NSString stringWithFormat:@"%@/%@/%@/%@.plist",documentDir,[[MyInfor shareMyInfor]get_user_id],saveKey,saveKey];
    
    return chatPacket;
}

-(BOOL)haveFileAtPath:(NSString *)path
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    return [fileManager fileExistsAtPath:path];
}

-(BOOL)saveDicKeyArray:(NSMutableDictionary *)array path:(NSString *)path
{
    
    return  [array writeToFile:path atomically:YES];
}

-(BOOL)saveArray:(NSMutableArray *)array path:(NSString *)path
{
    return [array writeToFile:path atomically:YES];
}


-(NSMutableDictionary *)readFromDocunemt:(NSString *)docuKey
{
    NSString *path = [self getSavePath:docuKey];
    return [NSMutableDictionary dictionaryWithContentsOfFile:path];
}

-(void)deleteMarkByFid:(NSString *)fid sourceDic:(NSMutableDictionary *)sourceDic
{
    for (NSString *fidTemp in sourceDic.allKeys)
    {
        NSString* fidsubFidTemp  = [sourceDic objectForKey:fid];
        if ([fidsubFidTemp isEqualToString:fid])
        {
            NSMutableArray* tempArray = [NSMutableArray arrayWithArray:[sourceDic objectForKey:fidTemp]];
            [sourceDic removeObjectForKey:fidTemp];
            [sourceDic setObject:tempArray forKey:fidsubFidTemp];
            break;
        }
        
        
    }
}


-(void)cleanAllUdpData
{
    
    NSString *chatSavePath = [self getSavePath:ChatCoument];
    
    NSArray *tempUdpPath = [NSArray arrayWithObjects:chatSavePath, nil];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    for (NSString *willRemovePath in tempUdpPath)
    {
        NSError *Error = nil;
        BOOL flag =  [fileManager removeItemAtPath:willRemovePath error:&Error];
        INNLog(@"path=:%@;flag=:%d",willRemovePath,flag);
    }
}

@end

//
//  PlaySound.h
//  Secrets
//
//  Created by 胡 庆贺 on 14-8-19.
//  Copyright (c) 2014年 huqinghe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PlaySound : NSObject
-(void)playSound;

@end

//
//  UDPHeartbeat.m
//  MyUdp
//
//  Created by tiansha on 14-6-30.
//  Copyright (c) 2014年 huqinghe. All rights reserved.
//

#import "UDPHeartbeat.h"
#import "AsyncUdpSocket.h"
#import "ShareDefine.h"
#import "TCP.h"
#import "TCPUDPCONFIG.h"
#import "JSONKit.h"

@interface UDPHeartbeat()
{
    
    AsyncUdpSocket      *m_udpSocket;
    NSTimer             *m_hearbeartTimer;
    NSMutableData       *m_heartData;
    NSMutableString     *m_heartStr;
    
    NSMutableString     *m_queueId;
    NSString            *s_message_id;
}
-(void)sendeUDPHeart:(id)sender;

@end


@implementation UDPHeartbeat

+(UDPHeartbeat *)getHeartbeatSingleton
{
    static UDPHeartbeat *udpHeartbeat = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        INNLog(@"only one");
        udpHeartbeat = [[self alloc] init];
    });
    return udpHeartbeat;
}

-(void)openUDP
{
#if NeedUDP
    [self closeUDP];
    
    m_heartData = [[NSMutableData alloc] init];
    m_heartStr = [[NSMutableString alloc] init];
    
    m_udpSocket = [[AsyncUdpSocket alloc] initWithDelegate:self];
    NSError *error = nil;
    BOOL flagBind = [m_udpSocket bindToPort:UDPHeartbeatPort error:&error];
    INNLog(@"绑定标示flagBind=:%d",flagBind);
    [ m_udpSocket enableBroadcast:YES error:&error];
    
    //[m_udpSocket joinMulticastGroup:@"239.255.250.250"error:&error];
    
    if (error)
    {
        INNLog(@"绑定错误error=:%@",error);
    }
    m_hearbeartTimer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(sendeUDPHeart:) userInfo:@"testHeart" repeats:YES];
    [m_hearbeartTimer fire];
    [m_udpSocket receiveWithTimeout:-1 tag:1];
#endif
}

-(void)sendeUDPHeart:(id)sender
{
    [m_udpSocket sendData:[s_message_id dataUsingEncoding:NSUTF8StringEncoding]
              toHost:UDPAddress
                port:UDPHeartbeatPort
         withTimeout:-1 tag:123];
    INNLog(@"UDPAddress=:%@",UDPAddress);
    INNLog(@"UDPPort=;%d",UDPHeartbeatPort);
    
    
}

-(void)closeUDP
{
    if (m_udpSocket)
    {
        m_udpSocket.delegate = nil;
        [m_udpSocket close];
        m_udpSocket = nil;
    }
    
    if (m_hearbeartTimer)
    {
        [m_hearbeartTimer invalidate];
        m_hearbeartTimer= nil;
    }
    
    if (m_heartData)
    {
        m_heartData = nil;
    }
    if (m_heartStr)
    {
        m_heartStr = nil;
    }
}

-(void)setMessageId:(NSString *)mid
{
    s_message_id = mid;
}

- (BOOL)onUdpSocket:(AsyncUdpSocket *)sock didReceiveData:(NSData *)data withTag:(long)tag fromHost:(NSString *)host port:(UInt16)port
{
    //INNLog(@"host----->%@",host);
    [m_udpSocket receiveWithTimeout:-1 tag:0];
    
    [m_heartData setLength:0];
    @try
    {
        [m_heartData appendData:data];
        
        [m_heartStr deleteCharactersInRange:NSMakeRange(0, m_heartStr.length)];

    }
    @catch (NSException *exception)
    {
        
    }
    NSString *tempStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    if (tempStr)
    {
        
        [m_heartStr appendString:tempStr];

    }
    
    //{"amount": 0, "uid": 3847415, "queue_id": "0"}
    //{"amount": 0, "uid": 3847415, "queue_id": "0"}
//    [[HelpServerTest getIntace] setUDPText:m_heartStr];
    
    NSDictionary *hearbeatDic = [m_heartStr objectFromJSONString];
    INNLog(@"hearbeatDic=:%@",hearbeatDic);
    
    [self closeUDP];
    
    return YES;
}

-(void)onUdpSocket:(AsyncUdpSocket *)sock didNotReceiveDataWithTag:(long)tag dueToError:(NSError *)error
{
    
    INNLog(@"Message not received for error: %@", error);
    
}

-(void)onUdpSocket:(AsyncUdpSocket *)sock didNotSendDataWithTag:(long)tag dueToError:(NSError *)error
{
    
    INNLog(@"Message not send for error: %@",error);
    
}

-(void)onUdpSocket:(AsyncUdpSocket *)sock didSendDataWithTag:(long)tag
{
    
    INNLog(@"Message send success!");
    
}

-(void)onUdpSocketDidClose:(AsyncUdpSocket *)sock
{
    
    INNLog(@"socket closed!");
    
}

-(void)dealloc
{
    m_queueId = nil;
}
@end

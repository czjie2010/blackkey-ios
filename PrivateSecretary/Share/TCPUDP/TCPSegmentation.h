//
//  TCPSegmentation.h
//  Secrets
//
//  Created by 胡 庆贺 on 14-7-18.
//  Copyright (c) 2014年 huqinghe. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface TCPSegmentation : NSObject

-(void)segmentTCPArrayPackagToOBJ:(NSArray *)tcpArray;

-(NSString *)getSavePath:(NSString *)saveKey;

-(NSMutableDictionary *)readFromDocunemt:(NSString *)docuKey;

-(BOOL)createSavePath:(NSString *)path;

-(NSMutableDictionary *)codIng:(NSDictionary *)dic;

-(BOOL)haveFileAtPath:(NSString *)path;

-(NSMutableDictionary *)readFromPath:(NSString *)path;

-(NSMutableArray *)readArrayFromPath:(NSString *)path;

-(BOOL)saveDicKeyArray:(NSMutableDictionary *)array path:(NSString *)path;

-(BOOL)saveArray:(NSMutableArray *)array path:(NSString *)path;

-(void)deleteMarkByFid:(NSString *)fid sourceDic:(NSMutableDictionary *)sourceDic;

-(void)cleanAllUdpData;



@end
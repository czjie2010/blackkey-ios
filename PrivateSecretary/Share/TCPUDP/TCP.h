//
//  TCP.h
//  Secrets
//
//  Created by 胡 庆贺 on 14-6-30.
//  Copyright (c) 2014年 huqinghe. All rights reserved.
//

#import <Foundation/Foundation.h>
@class AsyncSocket;

@interface TCP : NSObject
{
    
}
+(TCP *)getTCPSingleton;
-(void)senderTcp:(NSDictionary *)senderDic;
-(BOOL)openTcp;
-(void)closeTcp;

-(void)setQueueId:(NSString *)queueId;

-(BOOL)getTcpContentIng;

@end

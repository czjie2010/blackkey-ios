//
//  PlaySound.m
//  Secrets
//
//  Created by 胡 庆贺 on 14-8-19.
//  Copyright (c) 2014年 huqinghe. All rights reserved.
//

#import "PlaySound.h"
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

@interface PlaySound()
{
    NSURL *musicURL;
    AVAudioPlayer *thePlayer;
}
@end
@implementation PlaySound
-(void)playSound
{
    static SystemSoundID shake_sound_male_id = 0;
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"secret_newmessage" ofType:@"mp3"];
    if (path)
    {
        //注册声音到系统
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)[NSURL fileURLWithPath:path],&shake_sound_male_id);
        AudioServicesPlaySystemSound(shake_sound_male_id);
        //AudioServicesPlaySystemSound(shake_sound_male_id);//如果无法再下面播放，可以尝试在此播放
    }
    
    AudioServicesPlaySystemSound(shake_sound_male_id);//播放注册的声音，（此句代码，可以在本类中的任意位置调用，不限于本方法中）
    
    //    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);   //让手机震动


}

@end

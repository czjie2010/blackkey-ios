//
//  UDPHeartbeat.h
//  MyUdp
//
//  Created by tiansha on 14-6-30.
//  Copyright (c) 2014年 huqinghe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UDPHeartbeat : NSObject

+(UDPHeartbeat *)getHeartbeatSingleton;

-(void)openUDP;

-(void)closeUDP;

-(void)setMessageId:(NSString *)mid;
@end

//
//  NSString+NSString_NULL.m
//  Secrets
//
//  Created by 胡 庆贺 on 14-6-3.
//  Copyright (c) 2014年 huqinghe. All rights reserved.
//

#import "NSString+NULL.h"

@implementation NSString (NSString_NULL)

-(NSString *)NSStringNull
{
    NSString *str;
    str = [self stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
    str = [str stringByReplacingOccurrencesOfString:@"<null>" withString:@""];
    
   return str;
}

-(NSString *)deleteHeadTailSpace
{
   return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

@end

//
//  FormatNeworkObj.m
//  Secrets
//
//  Created by tiansha on 14-6-6.
//  Copyright (c) 2014年 tiansha. All rights reserved.
//

#import "FormatNeworkObj.h"
#import "NSString+NULL.h"
#import "Base64.h"

@implementation FormatNeworkObj

+(NSString *)formatObjFrom:(NSDictionary *)dic byKey:(NSString *)akey
{
    NSString *tempStr = [NSString stringWithFormat:@"%@",[dic objectForKey:akey]];
    tempStr = [tempStr NSStringNull];
    return tempStr;
    
}

+(NSString *)formatObjFrom:(NSDictionary *)dic byKey:(NSString *)akey needBase64:(BOOL)flag
{
    NSString *tempStr = [NSString stringWithFormat:@"%@",[dic objectForKey:akey]];
    tempStr = [tempStr NSStringNull];
    if (flag)
    {
        tempStr = [tempStr base64DecodedString];
    }
    return tempStr;
}

@end

//
//  FormatNeworkObj.h
//  Secrets
//
//  Created by tiansha on 14-6-6.
//  Copyright (c) 2014年 tiansha. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FormatNeworkObj : NSObject

+(NSString *)formatObjFrom:(NSDictionary *)dic byKey:(NSString *)akey;
+(NSString *)formatObjFrom:(NSDictionary *)dic byKey:(NSString *)akey needBase64:(BOOL)flag;

@end

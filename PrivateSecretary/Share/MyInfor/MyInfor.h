//
//  MyInfor.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/28.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyInfor : NSObject

+(MyInfor *) shareMyInfor;
-(NSString *)get_user_id;
-(void)setUserInfo:(NSDictionary *)dict;
- (void)getUserInfor;
-(void)removeUserInfo;

- (void) getUserIdentifyNum:(NSString *)num;
- (void) setPushToken:(NSString *)token;
@end

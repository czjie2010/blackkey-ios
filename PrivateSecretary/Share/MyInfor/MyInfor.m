//
//  MyInfor.m
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/28.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import "MyInfor.h"
#import "NSString+NULL.h"
#import "URLProvider.h"
#import "DeviceIdentifier.h"
#import "BaiDuLocation.h"
#import "NSString+Helper.h"
#import "JSONKit.h"
#import "TCP.h"
#import "RequestDataWithHttp.h"


@interface MyInfor()<RequestDataWithHttpDelegate>{
    
}

@end

@implementation MyInfor

+(MyInfor *) shareMyInfor
{
    static MyInfor *_shareMyInfor = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _shareMyInfor = [[[self class] alloc] init];
    });
    return _shareMyInfor;
}


-(NSString *)get_user_id
{
    NSUserDefaults *uid = [NSUserDefaults standardUserDefaults];
    NSDictionary *userInfoDic = [uid objectForKey:UserInfo];
    NSString *uidTemp = [userInfoDic objectForKey:@"user_id"];
    
    if ([uidTemp NSStringNull].length >0) {
        return [uidTemp NSStringNull];
    }
    
    return @"";
    
}

-(void)removeUserInfo
{
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:UserInfo];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

-(void)setUserInfo:(NSDictionary *)dict{
    
    NSMutableDictionary *dict_userInfo = [NSMutableDictionary dictionary];
    
    [dict_userInfo setObject:[NSString stringWithFormat:@"%@",[dict objectForKey:@"user_id"]] forKey:@"user_id"];
    [[NSUserDefaults standardUserDefaults]setObject:dict_userInfo forKey:UserInfo];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}

- (void) getUserIdentifyNum:(NSString *)num
{
    
    NSString *strUrl = [URLProvider get_activation_verification_code_url];
    
    NSDictionary *dictinary = [NSDictionary dictionaryWithObjectsAndKeys:
                               [[DeviceIdentifier sharedDeviceInfo] theNewUUID],@"uuid2",
                               [[DeviceIdentifier sharedDeviceInfo] getDeviceID],@"uuid1",
                               nil];
    
    NSString *jsonString = [NSString jsonStringWithObject:dictinary];
    NSData *postData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]];
    [request setHTTPMethod:POSTMETHOD];
    [request addValue:[[DeviceIdentifier sharedDeviceInfo]getDeviceID] forHTTPHeaderField:@"uuid1"];
    [request addValue:[[DeviceIdentifier sharedDeviceInfo] theNewUUID] forHTTPHeaderField:@"uuid2"];
    [request setValue:[[DeviceIdentifier sharedDeviceInfo] getDeviceUserAgent] forHTTPHeaderField:@"User-Agent"];
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request addValue:[NSString stringWithFormat:@"%lu",(unsigned long)[jsonString length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse* response, NSData* data, NSError* connectionError)
     {
         NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
         NSDictionary *dictionary = [str objectFromJSONString];
         INNLog(@"dictionary = %@",dictionary);
         NSString *err = [[dictionary objectForKey:@"err"] stringValue];
         
         if([err intValue] == 0)
         {
             if ([self get_user_id].length >0) {
                 //                 if (![[self get_user_id]isEqualToString:[dictionary objectForKey:@"user_id"]]) {
                 //老用户
                 [self setUserInfo:dictionary];
                 //                 }
             }else{
                 //新用户
                 if (dictionary != nil) {
                     [self setUserInfo:dictionary];
//                     [[NSNotificationCenter defaultCenter]postNotificationName:NewUser object:nil];
                     
                 }else{
                     INNLog(@"请求错误 无法获得用户id");
                 }
             }
             
             //             if ([self get_user_id]) {
             //                 if (![[TCP getTCPSingleton]getTcpContentIng]) {
             //                     [[TCP getTCPSingleton]openTcp];
             //                 }
             //             }
         }
         
     }];
}

//判断是否为新用户 根据设备号获取该用户的个人信息
- (void) getUserInfor
{
    
    NSString *strUrl = [URLProvider get_signin_url];
    
    NSDictionary *dictinary = [NSDictionary dictionaryWithObjectsAndKeys:
                               [[DeviceIdentifier sharedDeviceInfo] theNewUUID],@"uuid2",
                               [[DeviceIdentifier sharedDeviceInfo] getDeviceID],@"uuid1",
//                               [[BaiDuLocation shareBaiDuLocation]getProvince],@"province",
//                               [[BaiDuLocation shareBaiDuLocation]getCity],@"city",
//                               [[BaiDuLocation shareBaiDuLocation]getLatitude],@"latitude",
//                               [[BaiDuLocation shareBaiDuLocation]getLongitude],@"longitude",
                               nil];
    
    NSString *jsonString = [NSString jsonStringWithObject:dictinary];
    NSData *postData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]];
    [request setHTTPMethod:POSTMETHOD];
//    [request addValue:[[DeviceIdentifier sharedDeviceInfo] theNewUUID] forHTTPHeaderField:@"mobile_uuid"];
//    [request addValue:[[DeviceIdentifier sharedDeviceInfo] getLocationVersion] forHTTPHeaderField:@"version"];
    [request addValue:[[DeviceIdentifier sharedDeviceInfo]getDeviceID] forHTTPHeaderField:@"uuid1"];
    [request addValue:[[DeviceIdentifier sharedDeviceInfo] theNewUUID] forHTTPHeaderField:@"uuid2"];
    [request setValue:[[DeviceIdentifier sharedDeviceInfo] getDeviceUserAgent] forHTTPHeaderField:@"User-Agent"];
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request addValue:[NSString stringWithFormat:@"%lu",(unsigned long)[jsonString length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse* response, NSData* data, NSError* connectionError)
     {
         NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
         NSDictionary *dictionary = [str objectFromJSONString];
         INNLog(@"dictionary = %@",dictionary);
         NSString *err = [[dictionary objectForKey:@"err"] stringValue];
         
         if([err intValue] == 0 && dictionary!=nil)
         {
             if ([self get_user_id].length >0) {
//                 if (![[self get_user_id]isEqualToString:[dictionary objectForKey:@"user_id"]]) {
                 //老用户
                     [self setUserInfo:dictionary];
//                 }
             }else{
                 //新用户
                 [self setUserInfo:dictionary];
             }
             
//             if ([self get_user_id]) {
//                 if (![[TCP getTCPSingleton]getTcpContentIng]) {
//                     [[TCP getTCPSingleton]openTcp];
//                 }
//             }
         }
         
     }];
}


//推送设备号 /comment/set_push_token/
- (void) setPushToken:(NSString *)token{
    
    NSString *strUrl = [URLProvider get_pushToken_url:token];
    
    NSDictionary *dictinary = [NSDictionary dictionaryWithObjectsAndKeys:
                               token,@"ios_token",
                               nil];
    
    NSString *jsonString = [NSString jsonStringWithObject:dictinary];
    NSData *postData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]];
    [request setHTTPMethod:POSTMETHOD];
    [request addValue:[[DeviceIdentifier sharedDeviceInfo]getDeviceID] forHTTPHeaderField:@"uuid1"];
    [request addValue:[[DeviceIdentifier sharedDeviceInfo] theNewUUID] forHTTPHeaderField:@"uuid2"];
    [request setValue:[[DeviceIdentifier sharedDeviceInfo] getDeviceUserAgent] forHTTPHeaderField:@"User-Agent"];
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request addValue:[NSString stringWithFormat:@"%lu",(unsigned long)[jsonString length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse* response, NSData* data, NSError* connectionError)
     {
         NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
         NSDictionary *dictionary = [str objectFromJSONString];
         INNLog(@"%@",dictionary);
     }];
}

-(void)successBackData:(id)backNetObj topDownFlag:(BOOL)flag{
    INNLog(@"backNetObj =%@",backNetObj);
}

-(void)failBack:(id)failObj topDownFlag:(BOOL)flag{
    INNLog(@"failBack =%@",failObj);
}

@end

//
//  AlertMessageTool.h
//  PrivateSecretary
//
//  Created by tiansha on 14-5-28.
//  Copyright (c) 2014年 tiansha. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AlertMessageTool : NSObject

+(void)showMBProgressInView:(UIView *)willShowMbprogress WithMessage:(NSString *)message withHiddenTime:(int)hiddenTime;// 如果想自动消失传 <0 的数

+(void)showMBDoubleProgressInView:(UIView *)willShowMbprogress WithMessage:(NSString *)message withHiddenTime:(int)hiddenTime;

-(void)showMbWithOnView:(UIView *)willShowOnView Second:(int)second alert:(NSString *)alert;
-(void)hiddenProgress;
@end

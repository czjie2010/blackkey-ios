//
//  AlertMessageTool.m
//  PrivateSecretary
//
//  Created by tiansha on 14-5-28.
//  Copyright (c) 2014年 tiansha. All rights reserved.
//

#import "AlertMessageTool.h"
#import "MBProgressHUD.h"
#import "ShareDefine.h"

@interface AlertMessageTool()
{
    MBProgressHUD *m_mbprogress;
}
@end
@implementation AlertMessageTool

+(void)showMBProgressInView:(UIView *)willShowMbprogress WithMessage:(NSString *)message withHiddenTime:(int)hiddenTime
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:willShowMbprogress animated:YES];
    hud.userInteractionEnabled = NO;
	[willShowMbprogress addSubview:hud];
	hud.labelText = message;
    hud.mode = MBProgressHUDModeText;
 	[hud showAnimated:YES whileExecutingBlock:^{
        if (hiddenTime>0)
        {
            
            sleep(hiddenTime);
            
        }
        else
        {
            
            sleep(1);
            
        }
	} completionBlock:^{
		[hud removeFromSuperview];
	}];

}

+(void)showMBDoubleProgressInView:(UIView *)willShowMbprogress WithMessage:(NSString *)message withHiddenTime:(int)hiddenTime
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:willShowMbprogress animated:YES];
    hud.userInteractionEnabled = NO;
	[willShowMbprogress addSubview:hud];
	hud.detailsLabelText = message;
    hud.detailsLabelFont = [UIFont systemFontOfSize:16.0];
    hud.mode = MBProgressHUDModeText;
 	[hud showAnimated:YES whileExecutingBlock:^{
        if (hiddenTime>0)
        {
            
            sleep(hiddenTime);
            
        }
        else
        {
            
            sleep(1);
            
        }
	} completionBlock:^{
		[hud removeFromSuperview];
	}];
    
}

-(void)showMbWithOnView:(UIView *)willShowOnView Second:(int)second alert:(NSString *)alert
{
    if (!m_mbprogress)
    {
        m_mbprogress = [[MBProgressHUD alloc] initWithView:willShowOnView];
    }
    m_mbprogress.detailsLabelText = alert;
    m_mbprogress.mode = MBProgressHUDModeIndeterminate;
    [willShowOnView addSubview:m_mbprogress];
    [m_mbprogress show:YES];
    
    if (second)
    {
        //[m_mbprogress hide:YES afterDelay:second];
    }

}

-(void)hiddenProgress
{
    [m_mbprogress hide:YES];
    
}

-(void)dealloc
{
    m_mbprogress=nil;
}

@end

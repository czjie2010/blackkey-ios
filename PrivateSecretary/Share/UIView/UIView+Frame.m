//
//  UIView+Frame.m
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/5.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import "UIView+Frame.h"

@implementation UIView (Frame)

-(float)getViewX
{
    return self.frame.origin.x;
}

-(float)getViewY
{
    return self.frame.origin.y;
}

-(float)getViewWidth
{
    return self.frame.size.width;
}

-(float)getViewHigh
{
    return self.frame.size.height;
}


@end

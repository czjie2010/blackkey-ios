//
//  UIView+Frame.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/5.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Frame)

-(float)getViewX;
-(float)getViewY;
-(float)getViewWidth;
-(float)getViewHigh;

@end

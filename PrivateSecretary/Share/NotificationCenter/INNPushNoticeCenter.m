

//
//  INNPushNoticeCenter.m
//  Secrets
//
//  Created by 胡 庆贺 on 14/11/21.
//  Copyright (c) 2014年 huqinghe. All rights reserved.
//

#import "INNPushNoticeCenter.h"
#import "DeviceInfor.h"
#import "AppDelegate.h"
@implementation INNPushNoticeCenter
-(void)openPushNotice
{
    if ([DeviceInfor getIOSVersion]>=8)
    {
            UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge
                                                                                                 |UIUserNotificationTypeSound
                                                                                                 |UIUserNotificationTypeAlert) categories:nil];
            [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    }
    else
    {
            UIRemoteNotificationType myTypes = UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound;
            [[UIApplication sharedApplication] registerForRemoteNotificationTypes:myTypes];
 
    }
}

-(void)closePushNotice
{
    
        if ([DeviceInfor getIOSVersion]>=8)
        {
            UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeNone) categories:nil];
            [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
            
        }
        else
        {
            
            [[UIApplication sharedApplication] unregisterForRemoteNotifications];
            
        }

}
-(UIRemoteNotificationType)getPushState
{
    return  [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
}

@end

//
//  INNPushNoticeCenter.h
//  Secrets
//
//  Created by 胡 庆贺 on 14/11/21.
//  Copyright (c) 2014年 huqinghe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface INNPushNoticeCenter : NSObject
-(void)openPushNotice;
-(void)closePushNotice;
-(UIRemoteNotificationType)getPushState;
@end

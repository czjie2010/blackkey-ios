//
//  RequestDataWithHttp.h
//  Secrets
//
//  Created by tiansha on 14-5-26.
//  Copyright (c) 2014年 tiansha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RequestDataWithHttpDelegate.h"

@interface RequestDataWithHttp : NSObject<NSURLConnectionDataDelegate>
{
//    id<RequestDataWithHttpDelegate> m_delegate;
    BOOL topDownFlag;//判断是下拉还是上题刷新   普通请求不传也可
    BOOL m_automaticAlerFailMessage;
    
    BOOL m_customSelectFlag;//是否自定义回调方法
//    int  m_requestTag;//判断是那个请求
    SEL  m_customSuccessSelect;
    SEL  m_customFailSelect;
    
}

@property(nonatomic,weak)id<RequestDataWithHttpDelegate> m_delegate;
@property(nonatomic,assign)BOOL topDownFlag;
@property(nonatomic,assign)BOOL m_automaticAlerFailMessage;
@property(nonatomic,assign)BOOL m_customSelectFlag;
//@property(nonatomic,assign)int  m_requestTag;
@property(nonatomic,assign)SEL  m_customSuccessSelect;
@property(nonatomic,assign)SEL  m_customFailSelect;
-(BOOL)getNetworkStatus;
-(void)requestDataWithHttphead:(NSDictionary *)httphead
                      httpBody:(NSDictionary *)httpBody
                       withURL:(NSString *)requestUrl
                    HttpMethod:(NSString *)httpMethod;

-(void)cancelRequest;


-(void)requestWihtQueueHttphead:(NSDictionary *)httpHead
                       httpBody:(NSDictionary *)httpbody
                        withURL:(NSString *)aurl
                     httpMethod:(NSString *)httpMethod
                     requestTag:(int)requesTag;

-(void)cancelQueue;
-(BOOL)getQueueStates;


@end

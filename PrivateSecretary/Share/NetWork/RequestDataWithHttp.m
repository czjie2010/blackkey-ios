//
//  RequestDataWithHttp.m
//  Secrets
//
//  Created by 胡 庆贺 on 14-5-26.
//  Copyright (c) 2014年 huqinghe. All rights reserved.
//
/**
 
 */
#import "RequestDataWithHttp.h"
#import "ASIHTTPRequest.h"
#import "JSONKit.h"
#import "AppDelegate.h"
#import "ASINetworkQueue.h"
#import "DeviceIdentifier.h"

@interface RequestDataWithHttp()<ASIHTTPRequestDelegate>
{
    NSURLConnection *connection;
    NSMutableData *backData;
    ASIHTTPRequest *asiHTTPRequest;
    ASINetworkQueue *queue;
}

-(void)setAuthHead:(ASIHTTPRequest *)httpRequest;
-(void)cancelRequest;

@end

@implementation RequestDataWithHttp

@synthesize m_delegate,topDownFlag;
@synthesize m_automaticAlerFailMessage;
@synthesize m_customFailSelect,m_customSelectFlag,m_customSuccessSelect;

-(void)requestDataWithHttphead:(NSDictionary *)httphead
                      httpBody:(NSDictionary *)httpBody
                       withURL:(NSString *)requestUrl
                    HttpMethod:(NSString *)httpMethod
{
    if (asiHTTPRequest)
    {
        [self cancelRequest];
    }
    
    NSURL *requestUrlTemp = [NSURL URLWithString:requestUrl];
    INNLog(@"absoluteString=:%@",requestUrlTemp.absoluteString);
    asiHTTPRequest = [[ASIHTTPRequest alloc] initWithURL:requestUrlTemp];
    asiHTTPRequest.delegate = self;
    [asiHTTPRequest setRequestMethod:httpMethod];
    
    [self setAuthHead:asiHTTPRequest];
    
    if (httphead == nil)
    {
        NSDictionary *dictinary = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [[DeviceIdentifier sharedDeviceInfo] getDeviceID],@"uuid1",
                                   [[DeviceIdentifier sharedDeviceInfo] theNewUUID],@"uuid2",
                                   nil];
        httphead = dictinary;
    }
 
    if ([[httphead allKeys] count])
    {
        for (NSString *headKey in httphead.allKeys)
        {
            [asiHTTPRequest addRequestHeader:headKey value:[httphead objectForKey:headKey]];
        }
    }
    //添加头信息
    //[asiHTTPRequest addRequestHeader:(NSString *) value:(NSString *)];
    
    if ([[httpBody allKeys] count])
    {
        NSString *bodyJSONStr = [httpBody JSONString];
        NSData *bodyData = [bodyJSONStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableData *bodyPostMutablData = [NSMutableData dataWithData:bodyData];
        [asiHTTPRequest setPostBody:bodyPostMutablData];
    }
    
    [asiHTTPRequest startAsynchronous];
}




- (void)requestStarted:(ASIHTTPRequest *)request
{
    
    
    
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    INNLog(@"back=:%@",[request.responseString objectFromJSONString]);
    INNLog(@"code=:%d",request.responseStatusCode);
    INNLog(@"url=:%@",request.url);
//    NSString * alert = [[request.responseString objectFromJSONString] objectForKey:@"alert"];tiansha
    
//    if (alert)
//    {
//        UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"友情提示" delegate:nil cancelButtonTitle:alert destructiveButtonTitle:nil otherButtonTitles: nil];
//        [sheet showInView:[AppDelegate getWindow]];
//        [sheet release];
//    }
     
    switch (request.responseStatusCode)
    {
        case 200:
        {
            NSString *backJSONStr =  request.responseString;

            if (!self.m_customSelectFlag)//默认回调
            {
                if (self.m_delegate&&[self.m_delegate respondsToSelector:@selector(successBackData:topDownFlag:)])
                {
                    [self.m_delegate successBackData:[backJSONStr objectFromJSONString] topDownFlag:self.topDownFlag];
                }
                else
                {
                    INNLog(@"请实现接收数据代理");
                }

            }
            if (self.m_customSelectFlag)//自定义回调
            {
                if (self.m_delegate&&[self.m_delegate respondsToSelector:self.m_customSuccessSelect])
                {
                    [self.m_delegate performSelector:self.m_customSuccessSelect
                                          withObject:[backJSONStr objectFromJSONString]
                                          withObject:[NSNumber numberWithLong:request.tag]];
                }
            }

            
 
        }
            break;
            
        default:
        {
            [self requestFailed:request];
        }
            break;
    }
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    
    NSString *failJSONStr = request.responseString;
    
    if (!self.m_customSelectFlag)//默认回调
    {
        if (self.m_delegate&&[self.m_delegate respondsToSelector:@selector(failBack:topDownFlag:)])
        {
            [self.m_delegate failBack:[failJSONStr objectFromJSONString] topDownFlag:self.topDownFlag];
        }
        else
        {
            INNLog(@"请实现返回数据失败代理");
        }

    }
    if (self.m_customSelectFlag)//自定义回调
    {
        if (self.m_delegate&&[self.m_delegate respondsToSelector:self.m_customFailSelect])
        {
            [self.m_delegate performSelector:self.m_customFailSelect
                                  withObject:[failJSONStr objectFromJSONString]withObject:[NSNumber numberWithLong:request.tag]];
        }
    }

    
    if (self.m_automaticAlerFailMessage)
    {
//         [AlertMessageTool showMBProgressInView:[(AppDelegate *)[[UIApplication sharedApplication] delegate] window] WithMessage:NetwrokErrorMessage withHiddenTime:1];
 
    }

}

-(void)requestWihtQueueHttphead:(NSDictionary *)httpHead
                       httpBody:(NSDictionary *)httpbody
                        withURL:(NSString *)aurl
                     httpMethod:(NSString *)httpMethod
                     requestTag:(int)requesTag
{
    ASIHTTPRequest *_questTemp = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:aurl]];
    _questTemp.tag = requesTag;
    _questTemp.delegate = self;
    [self setAuthHead:_questTemp];
    if (httpHead.allKeys.count)
    {
        for (NSString *httpHeadValue in httpHead.allKeys)
        {
            [_questTemp addRequestHeader:[httpHead objectForKey:httpHeadValue]
                                   value:httpHeadValue];
        }
    }
    
    if (httpbody.allKeys.count)
    {
        NSString *bodyJSONStr = [httpbody JSONString];
        NSData *bodyData = [bodyJSONStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableData *bodyPostMutablData = [NSMutableData dataWithData:bodyData];
        [_questTemp setPostBody:bodyPostMutablData];
     }
    if (!queue)
    {
        queue = [[ASINetworkQueue alloc] init];
        queue.maxConcurrentOperationCount = 1;
    }
    [queue addOperation:_questTemp];
    
    [queue go];
    
}


-(void)cancelQueue
{
    if (queue)
    {
        [queue cancelAllOperations];
    }
}


-(BOOL)getNetworkStatus
{
    if (asiHTTPRequest==nil)
    {
        return YES;
    }
    return [asiHTTPRequest complete];
}

-(void)cancelRequest
{
    if (asiHTTPRequest)
    {
        [asiHTTPRequest clearDelegatesAndCancel];
        asiHTTPRequest = nil;
    }

}

-(BOOL)getQueueStates
{
    BOOL flag = YES;
    for (ASIHTTPRequest* tempRequest in queue.operations)
    {
      flag &=  [tempRequest complete];
    }
    return flag;
}

-(void)setAuthHead:(ASIHTTPRequest *)httpRequest
{
    [httpRequest addRequestHeader:@"Content-Type" value:@"audio/x-flac;rate=16000"];
    [httpRequest addRequestHeader:@"lang" value:@"zh-Hans"];
    INNLog(@"Scheme=:%@",httpRequest.url.absoluteString);
}

-(void)dealloc
{
    [self cancelRequest];
    [self cancelQueue];
    if (queue)
    {
        queue = nil;
    }
    self.m_delegate = nil;
}
@end

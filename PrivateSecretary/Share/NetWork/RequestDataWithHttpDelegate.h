//
//  RequestDataWithHttpDelegate.h
//  Secrets
//
//  Created by tiansha on 14-5-26.
//  Copyright (c) 2014年 tiansha. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RequestDataWithHttpDelegate <NSObject>
@optional
-(void)successBackData:(id)backNetObj topDownFlag:(BOOL)flag;
-(void)failBack:(id)failObj topDownFlag:(BOOL)flag;

@end

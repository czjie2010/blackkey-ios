//
//  INNOTableView.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/7/29.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGORefreshTableHeaderView.h"
#import "INNOTableViewToDownRefreshDelegate.h"

@interface INNOTableView : UITableView<EGORefreshTableHeaderDelegate,UITableViewDelegate,UITableViewDataSource>
{
//    id<UITableViewDataSource,UITableViewDelegate,INNOTableViewToDownRefreshDelegate> tableDalegate;
    EGORefreshTableHeaderView *_refreshHeaderView;
    BOOL _reloading;
    @public
    BOOL isEditingTableCell;
}
@property(unsafe_unretained)id<UITableViewDataSource,UITableViewDelegate,INNOTableViewToDownRefreshDelegate> tableDalegate;

-(void)egoFirstLoad;//[_refreshHeaderView setState:EGOOPullRefreshLoading];
- (id)initWithFrame:(CGRect)frame
              style:(UITableViewStyle)style
           adelegate:(id<UITableViewDataSource,UITableViewDelegate,INNOTableViewToDownRefreshDelegate>)adelegate
         topRefresh:(BOOL)topRefresh
      bottomRefresh:(BOOL)bottomRefresh;

- (void)reloadTableViewDataSource;//下拉刷新
- (void)doneLoadingTableViewData;//完成刷新
-(void)haveMoreData:(BOOL)flag;//没有更多数据
-(void)haveNOData;//无数据
-(void)hiddenTableFootTopLine:(BOOL)flag;
-(BOOL)getIsHasMore;
-(void)setIsHasMore:(BOOL)flag;
@end

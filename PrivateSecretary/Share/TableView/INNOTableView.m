//
//  INNOTableView.m
//  PrivateSecretary
//
//  Created by 田沙 on 15/7/29.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import "INNOTableView.h"
#define LoadNextPageOffsetY 5
#import "INNOTableRefreshFoot.h"

@interface INNOTableView (){
    INNOTableRefreshFoot  *innoTableRefreshFoot;
    BOOL    isHasMore;
    BOOL    isNextPageLoad; //
}

@end

@implementation INNOTableView


- (id)initWithFrame:(CGRect)frame
              style:(UITableViewStyle)style
          adelegate:(id<UITableViewDataSource,UITableViewDelegate,INNOTableViewToDownRefreshDelegate>)adelegate
         topRefresh:(BOOL)topRefresh
      bottomRefresh:(BOOL)bottomRefresh
{
    self = [super initWithFrame:frame style:style];
    if (self)
    {
        _tableDalegate = adelegate;
        // Initialization code
        if (topRefresh)
        {
            if (_refreshHeaderView == nil)
            {
                
                EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - self.bounds.size.height, self.frame.size.width, self.bounds.size.height)];
                view.delegate = self;
                [self addSubview:view];
                _refreshHeaderView = view;
            }
            
            //  update the last update date
            [_refreshHeaderView refreshLastUpdatedDate];
            [self egoFirstLoad];
        }
        
        if (bottomRefresh && innoTableRefreshFoot == nil)
        {
            innoTableRefreshFoot = [[[NSBundle mainBundle] loadNibNamed:@"INNOTableRefreshFoot" owner:nil options:nil] firstObject];
//            tableFootView.backgroundColor = [UIColor grayColor];
            self.tableFooterView = innoTableRefreshFoot;
            
        }
        self.dataSource = self;
        self.delegate = self;
        //[self performSelector:@selector(egoFirstLoad) withObject:nil afterDelay:DelayTimeEGOStatus];
        //[self performSelectorInBackground:@selector(egoFirstLoad) withObject:@"yes"];
        [self performSelectorOnMainThread:@selector(egoFirstLoad) withObject:@"yes" waitUntilDone:YES];
        
        [self haveNOData];
        isNextPageLoad = YES;
    }
    return self;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (_tableDalegate&&[_tableDalegate respondsToSelector:@selector(tableView:numberOfRowsInSection:)])
    {
       return [_tableDalegate tableView:tableView numberOfRowsInSection:section];
    }
    
    return 0;
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (_tableDalegate&&[_tableDalegate respondsToSelector:@selector(numberOfSectionsInTableView:)])
    {
        return [_tableDalegate numberOfSectionsInTableView:tableView];
    }
    
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (_tableDalegate&&[_tableDalegate respondsToSelector:@selector(tableView:heightForHeaderInSection:)])
    {
        return [_tableDalegate tableView:tableView heightForHeaderInSection:section];
    }
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (_tableDalegate&&[_tableDalegate respondsToSelector:@selector(tableView:viewForHeaderInSection:)])
    {
        return [_tableDalegate tableView:tableView viewForHeaderInSection:section];
    }
    return nil;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (_tableDalegate&&[_tableDalegate respondsToSelector:@selector(tableView:cellForRowAtIndexPath:)])
    {
        return [_tableDalegate tableView:tableView cellForRowAtIndexPath:indexPath];
    }
    return nil;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_tableDalegate&&[_tableDalegate respondsToSelector:@selector(tableView:heightForRowAtIndexPath:)])
    {
        return [_tableDalegate tableView:tableView heightForRowAtIndexPath:indexPath];
    }
    return 45;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_tableDalegate&&[_tableDalegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)])
    {
        [_tableDalegate tableView:tableView didSelectRowAtIndexPath:indexPath];
    }
}


-(void)egoFirstLoad
{
    [self setContentOffset:CGPointMake(self.contentOffset.x, 0) animated:YES];
    [_refreshHeaderView setState:EGOOPullRefreshLoading];
    _reloading = YES;

}
- (void)reloadTableViewDataSource
{
	
	//  should be calling your tableviews data source model to reload
	//  put here just for demo
	_reloading = YES;
    if (_tableDalegate&&[_tableDalegate respondsToSelector:@selector(reloadTableViewDataSource)])
    {
        [_tableDalegate reloadTableViewDataSource];
    }
	
}

- (void)doneLoadingTableViewData
{
	
	//  model should call this when its done loading
//    self.contentOffset = ;
//    [self setContentOffset:CGPointMake(self.contentOffset.x, 0) animated:YES];
    //[_refreshHeaderView setState:EGOOPullRefreshNormal];
	_reloading = NO;
	[_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self];
	
}
#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
//	INNLog(@"scrollView.contentOffset.y=:%f",scrollView.contentOffset.y);
//    INNLog(@"scrollView.contentOffset.x=;%f",scrollView.contentOffset.x);
    if ((scrollView.contentOffset.y>LoadNextPageOffsetY)&&self.tableFooterView)
    {
        BOOL flag =    scrollView.contentSize.height - self.frame.size.height - scrollView.contentOffset.y<41;
        if (flag&&_tableDalegate&&[_tableDalegate respondsToSelector:@selector(loadNextPage)]&&isNextPageLoad)
        {
            
            [_tableDalegate loadNextPage];
            isNextPageLoad = NO;
            [self performSelector:@selector(pleaseWait) withObject:nil afterDelay:1.0];
        }
    }
	[_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
	
	[_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
	
}


#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view
{
	
	[self reloadTableViewDataSource];
	//[self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:3.0];//测试代码
	
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view
{
	
	return _reloading; // should return if data source model is reloading
	
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view
{
	
	return [NSDate date]; // should return date data source was last changed
	
}

-(void)haveMoreData:(BOOL)flag
{

    for (INNOTableRefreshFoot *tempView in self.tableFooterView.subviews)
    {
        if (tempView.tag==0)
        {
            isNextPageLoad = flag;
            if (flag) {
                [innoTableRefreshFoot  setLoading];
            }else{
                [innoTableRefreshFoot setNoData];
            }
        }
    }
    
}

-(void)haveNOData{
    
    [innoTableRefreshFoot hiddenAll];
}

-(void)hiddenTableFootTopLine:(BOOL)flag{
    
    [innoTableRefreshFoot hiddenTopLine:flag];
}

//没有更多 禁止页面在增加
-(void)setIsHasMore:(BOOL)flag{
    isHasMore = flag;
}

-(BOOL)getIsHasMore{
    return isHasMore;
}

-(void)pleaseWait{
    isNextPageLoad = YES;
}

-(void)dealloc{
    _tableDalegate = nil;
}
@end

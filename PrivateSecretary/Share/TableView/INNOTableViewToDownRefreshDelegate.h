//
//  INNOTableViewToDownRefreshDelegate.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/7/29.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol INNOTableViewToDownRefreshDelegate <NSObject>
@required
- (void)reloadTableViewDataSource;//下拉刷新
@optional
- (void)loadNextPage;
- (void)doneLoadingINNOTableViewData;//完成刷新
@end

//
//  INNOTableRefreshFoot.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/7/29.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface INNOTableRefreshFoot : UIView

-(void)setLoading;
-(void)setNoData;
-(void)hiddenAll;
-(void)hiddenTopLine:(BOOL)flag;
@end

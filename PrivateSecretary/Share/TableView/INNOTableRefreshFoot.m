//
//  INNOTableRefreshFoot.m
//  PrivateSecretary
//
//  Created by 田沙 on 15/7/29.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import "INNOTableRefreshFoot.h"

@interface INNOTableRefreshFoot()
{
    IBOutlet UILabel *mimi_load;
    IBOutlet UIView     *mimi_topLine;
    IBOutlet UIActivityIndicatorView        *mimi_loading;
    IBOutlet    UIView      *mimi_noDataBgV;
    IBOutlet    UIView      *mimi_noDataLeftLine;
    IBOutlet    UIView      *mimi_noDataRightLine;
    IBOutlet    UILabel      *mimi_noDataEndLabel;
}
@end

@implementation INNOTableRefreshFoot

-(void)awakeFromNib
{
}

-(void)setNoData{
    mimi_noDataBgV.hidden = NO;
}

-(void)setLoading{
    mimi_noDataBgV.hidden = YES;
    mimi_load.text = @"加载中...";
    mimi_loading.hidden = NO;
}

-(void)hiddenAll{
    mimi_noDataBgV.hidden = YES;
    mimi_load.text = @"";
    mimi_loading.hidden = YES;
}

-(void)hiddenTopLine:(BOOL)flag{
    mimi_topLine.hidden = flag;
}

@end

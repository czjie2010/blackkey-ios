//
//  URLProvider.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/18.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#define TESTLIUXIN 1  //1 liuxin

#if TESTLIUXIN
//http://42.121.145.76:9998
static NSString *hostAdress = @"http://test.secretmimi.com";//刘欣
static NSString *messageAddress = @"http://test.secretmimi.com";//刘欣
#else
static NSString *hostAdress = @"http://apprequest.secretmimi.com";
static NSString *messageAddress =@"http://appmsg.secretmimi.com";
#endif

#define S_LocationVersionVersion   @"1.0.0"


#import <Foundation/Foundation.h>

@interface URLProvider : NSObject


/**
 *  发送消息
 *  /message/send/
 **/
+(NSString *)getsend_chat_message;


+(NSString *)get_signin_url;
+(NSString *)get_activation_verification_code_url;

//推送设备号 /member/api/set_push_token/
+(NSString *) get_pushToken_url:(NSString *)token;

+(NSString *) get_read_messages:(NSString *)mid;

+(NSString *) get_member_lbs:(NSString *)uid;
@end

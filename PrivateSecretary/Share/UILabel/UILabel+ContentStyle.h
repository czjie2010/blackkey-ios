//
//  UILabel+ContentStyle.h
//  Secrets
//
//  Created by 田沙 on 15/2/4.
//  Copyright (c) 2015年 tiansha. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (ContentStyle)

-(NSAttributedString *)reCustomizationContentStyle:(NSString *)str aFont:(UIFont *)font;
@end

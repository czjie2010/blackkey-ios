//
//  UILabel+ContentStyle.m
//  Secrets
//
//  Created by 田沙 on 15/2/4.
//  Copyright (c) 2015年 tiansha. All rights reserved.
//

#import "UILabel+ContentStyle.h"

#define ContentLineSpacing 6

@implementation UILabel (ContentStyle)

/**
 *  行间距设置
 *
 *  @param str <#str description#>
 *
 *  @return <#return value description#>
 */
-(NSAttributedString *)reCustomizationContentStyle:(NSString *)str aFont:(UIFont *)font{
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    paragraphStyle.lineSpacing = ContentLineSpacing;
    paragraphStyle.lineBreakMode = NSLineBreakByCharWrapping;
//    paragraphStyle.maximumLineHeight = 35.f;
//    paragraphStyle.minimumLineHeight = 25.f;
    paragraphStyle.alignment = NSTextAlignmentLeft;
    NSDictionary *attributes = @{ NSFontAttributeName:font, NSParagraphStyleAttributeName:paragraphStyle};
    
    NSAttributedString * attributedString = [[NSAttributedString alloc] initWithString:str attributes:attributes];
    
    return attributedString;
}

-(NSAttributedString *)reCustomizationContentStyle:(NSString *)str aFont:(UIFont *)font lineSpacing:(int)lineSpacing 
{
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    paragraphStyle.lineSpacing = ContentLineSpacing;
    paragraphStyle.lineBreakMode = NSLineBreakByCharWrapping;
    //    paragraphStyle.maximumLineHeight = 35.f;
    //    paragraphStyle.minimumLineHeight = 25.f;
    paragraphStyle.alignment = NSTextAlignmentLeft;
    NSDictionary *attributes = @{ NSFontAttributeName:font, NSParagraphStyleAttributeName:paragraphStyle};
    
    NSAttributedString * attributedString = [[NSAttributedString alloc] initWithString:str attributes:attributes];
    
    return attributedString;
}
@end

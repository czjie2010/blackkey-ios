//
//  TimeTool.m
//  Secrets
//
//  Created by tiansha on 14-7-7.
//  Copyright (c) 2014年 tiansha. All rights reserved.
//

#import "TimeTool.h"
#import "NSDate+Helper.h"
#import "ShareDefine.h"

@implementation TimeTool

+(NSString *)time:(NSString *)time
{
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:time.doubleValue];
    NSDateFormatter* fmt = [[NSDateFormatter alloc] init];
    fmt.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];
    fmt.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSString* dateString = [fmt stringFromDate:date];
    NSInteger hour =  [date hour];
    NSInteger mimute = [date minute];
    NSInteger year = [date year];
    NSInteger daysAgo = [date daysAgo];
    NSInteger daysArgoT = [date daysAgoAgainstMidnight];
    NSInteger weekday = [date weekday];
    NSString    *weekStr;
    NSString *mimuteStr = nil;
    if (mimute<10)
    {
        mimuteStr = [NSString stringWithFormat:@"0%d",(int)mimute];
    }
    else
    {
        mimuteStr = [NSString stringWithFormat:@"%ld",(long)mimute];
    }
    
    INNLog(@"hour=:%d;minute=:%ld;year=:%ld;daysAgo=:%ld;daysAgot=:%ld",(int)hour,(long)mimute,(long)year,(long)daysAgo,(long)daysArgoT);
    
    INNLog(@"dateString=:%@", dateString);
    

    
        switch (daysArgoT)
        {
            case 0://
            {
                return [NSString stringWithFormat:@"%d:%@",(int)hour,mimuteStr];
            }
                break;
            case 1:
            {
                return [NSString stringWithFormat:@"昨天 %d:%@",(int)hour,mimuteStr];
            }
                break;
                
            default:
            {
                if(daysArgoT < 7)
                {
                    switch ((int)weekday-1) {
                        case 2:
                            weekStr = @"星期二";
                            break;
                        case 3:
                            weekStr = @"星期三";
                            break;
                        case 4:
                            weekStr = @"星期四";
                            break;
                        case 5:
                            weekStr = @"星期五";
                            break;
                        case 6:
                            weekStr = @"星期六";
                            break;
                        default:
                            break;
                    }
                    
                   return [NSString stringWithFormat:@"%@ %d:%@",weekStr,(int)hour,mimuteStr];
                    
                }else if (year==[[NSDate date] year])
                {
                    return [dateString substringWithRange:NSMakeRange(5, 11)];
                }
            }
                break;
        }
        return [dateString substringToIndex:10];
//    }
//    return [dateString substringToIndex:10];
}


+(NSString *)timeToNow:(NSString *)time
{
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:time.doubleValue];
    NSDateFormatter* fmt = [[NSDateFormatter alloc] init];
    fmt.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];
    fmt.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSString* dateString = [fmt stringFromDate:date];
    NSInteger hour =  [date hour];
    NSInteger mimute = [date minute];
    NSInteger year = [date year];
    NSInteger daysAgo = [date daysAgo];
    NSInteger daysArgoT = [date daysAgoAgainstMidnight];
    
    NSString *mimuteStr = nil;
    if (mimute<10)
    {
        mimuteStr = [NSString stringWithFormat:@"0%d",(int)mimute];
    }
    else
    {
        mimuteStr = [NSString stringWithFormat:@"%ld",(long)mimute];
    }
    
    INNLog(@"hour=:%d;minute=:%ld;year=:%ld;daysAgo=:%ld;daysAgot=:%ld",(int)hour,(long)mimute,(long)year,(long)daysAgo,(long)daysArgoT);
    
    INNLog(@"dateString=:%@", dateString);
    
    NSDate *NowDate = [NSDate date];
    double secondNow = [NowDate timeIntervalSince1970];
//    if (ABS(secondNow-time.doubleValue)<60*60)
//    {
//        INNLog(@"一小时以内");
        int minuteAge = ABS(secondNow-time.doubleValue)/60;
        
        return [NSString stringWithFormat:@"%d",minuteAge];
//    }
}


+(NSString *)getTimeFromSecondFrom1970:(NSString *)second
{
    NSDate *data = [NSDate dateWithTimeIntervalSince1970:second.doubleValue];
    
    return [NSString stringWithFormat:@"%lu",(unsigned long)[data year]];
}

//+(NSString *)getTimeFromSecondFrom1970:(NSString *)second
//{
//    NSDate *NowDate = [NSDate date];
//    double secondNow = [NowDate timeIntervalSince1970];
//    
//    NSDate *data = [NSDate dateWithTimeIntervalSince1970:second.doubleValue];
//    
//    return [NSString stringWithFormat:@"%lu",(unsigned long)[data year]];
//}


+(NSString *)getNowSecondsTo1970
{
    NSDate *nowDate = [NSDate date];
    double sencondTo1970 =  [nowDate timeIntervalSince1970];
    
    return [NSString stringWithFormat:@"%f",sencondTo1970];
}
+(NSString *)getAge:(NSString *)secondForm1970
{
     NSString *data =   [TimeTool getTimeFromSecondFrom1970:secondForm1970];
     int nowYear = (int)[[NSDate date] year];
     return [NSString stringWithFormat:@"%d",nowYear-data.intValue];
}
@end

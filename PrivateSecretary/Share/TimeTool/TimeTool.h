//
//  TimeTool.h
//  Secrets
//
//  Created by tiansha on 14-7-7.
//  Copyright (c) 2014年 tiansha. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TimeTool : NSObject

+(NSString *)time:(NSString *)time;
//据当前时间的时间差
+(NSString *)timeToNow:(NSString *)time;

+(NSString *)getTimeFromSecondFrom1970:(NSString *)second;
+(NSString *)getNowSecondsTo1970;
+(NSString *)getAge:(NSString *)secondForm1970;

@end

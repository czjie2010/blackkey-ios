//
//  UploadingImageToUpYun.m
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/18.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import "UploadingImageToUpYun.h"
#import "UpYun.h"
#import "RequestDataWithHttp.h"
#import "URLProvider.h"
#import "MyInfor.h"

#define UPaiYun_DOMAIN @".b0.upaiyun.com"

@interface UploadingImageToUpYun() <RequestDataWithHttpDelegate,UIAlertViewDelegate>
{
}

@end

@implementation UploadingImageToUpYun

-(void)getUploadingFile:(id)file inView:(UIView *)view;
{
    UpYun *uy = [[UpYun alloc] init];
    
    if (uploadType == UploadSounds) {
        uy.bucket = @"blackkey-sound";
        uy.passcode = @"shExWfr3cRoWXxelq6AF67nWzBk=";
    }else if (uploadType == UploadImage){
        uy.bucket = @"blackkey-image";
        uy.passcode = @"cvfoLaz6/QS9XnzSIho7lmmhIjY=";
    }
//    uy.bucket = @"blackkey-sound";
    uy.progressBlocker =  ^(CGFloat progess ,long long requestDidSendBytes){
        INNLog(@"UploadingImageToUpYun progess = %f",progess);
        if ([_m_delegate respondsToSelector:@selector(getUploadingImageToUpYunProgress:sendBytes:inView:)]) {
            [_m_delegate getUploadingImageToUpYunProgress:progess sendBytes:requestDidSendBytes inView:view];
        }
    };
    uy.successBlocker = ^(id data)
    {
        NSDictionary *dict = [NSDictionary dictionaryWithDictionary:data];
        //        NSString *uPaiYunUrlString =[NSString stringWithFormat:@"http://%@%@%@",DEFAULT_BUCKET,UPaiYun_DOMAIN,[dict objectForKey:@"url"]];
//        [self uploadHead:[dict objectForKey:@"url"]];
        
        if ([_m_delegate respondsToSelector:@selector(UploadingImageToUpYunSuccess:inView:)]) {
            [_m_delegate UploadingImageToUpYunSuccess:dict inView:view];
        }

        
    };
    uy.failBlocker = ^(NSError * error)
    {
        if (DebugFlag) {
        
//            NSString *message = [error.userInfo objectForKey:@"message"];
//            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"" message:@"发送失败，重新发送" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
//            [alert show];
        }
        if ([_m_delegate respondsToSelector:@selector(UploadingImageToUpYunFail:inView:)]) {
            [_m_delegate UploadingImageToUpYunFail:error.userInfo inView:view];
        }
        
    };
    
    /**
     *	@brief	根据 UIImage 上传
     */
    [uy uploadFile:file saveKey:[self getSaveKey]];
    /**
     *	@brief	根据 文件路径 上传
     */
    //    NSString* resourcePath = [[NSBundle mainBundle] resourcePath];
    //    NSString* filePath = [resourcePath stringByAppendingPathComponent:@"fileTest.file"];
    //    [uy uploadFile:filePath saveKey:[self getSaveKey]];
    
    /**
     *	@brief	根据 NSDate  上传
     */
    //    NSData * fileData = [NSData dataWithContentsOfFile:filePath];
    //    [uy uploadFile:fileData saveKey:[self getSaveKey]];
    
    
    
}

-(NSString * )getSaveKey {
    /**
     *	@brief	方式1 由开发者生成saveKey
     */
    if (uploadType == UploadSounds) {
        return [NSString stringWithFormat:@"%@_%.0f.amr",[[MyInfor shareMyInfor]get_user_id],[[NSDate date] timeIntervalSince1970]];
    }else if (uploadType == UploadImage){
        return [NSString stringWithFormat:@"%@_%.0f.jpg",[[MyInfor shareMyInfor]get_user_id],[[NSDate date] timeIntervalSince1970]];
    }
    return [NSString stringWithFormat:@"%@_%.0f",[[MyInfor shareMyInfor]get_user_id],[[NSDate date] timeIntervalSince1970]];
    /**
     *	@brief	方式2 由服务器生成saveKey
     */
    //        return [NSString stringWithFormat:@"/{year}/{mon}/{filename}{.suffix}"];
    
    /**
     *	@brief	更多方式 参阅 http://wiki.upyun.com/index.php?title=Policy_%E5%86%85%E5%AE%B9%E8%AF%A6%E8%A7%A3
     */
}

-(void)dealloc
{
    _m_delegate = nil;
}
@end

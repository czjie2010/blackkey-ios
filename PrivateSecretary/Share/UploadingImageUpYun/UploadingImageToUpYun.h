//
//  UploadingImageToUpYun.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/18.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum{
    UploadImage,
    UploadSounds
}UploadingType;

@protocol UploadingImageToUpYunDelegate <NSObject>

@required
-(void)getUploadingImageToUpYunProgress:(float)progress sendBytes:(long long)requestDidSendBytes inView:(UIView *)view;

@optional
-(void)UploadingImageToUpYunFail:(NSDictionary *)dict inView:(UIView *)view;
-(void)UploadingImageToUpYunSuccess:(NSDictionary *)dict inView:(UIView *)view;


@end

@interface UploadingImageToUpYun : NSObject{
    @public
    UploadingType  uploadType;
}

@property(nonatomic, weak)id<UploadingImageToUpYunDelegate>m_delegate;

-(void)getUploadingFile:(id)file inView:(UIView *)view;
@end

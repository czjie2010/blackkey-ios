//
//  SaveFile.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/13.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SaveFile : NSObject

-(NSString *)getDocumentDir:(NSString *)saveKey; //文件所在文件夹目录
-(NSString *)getSaveFilePath:(NSString *)saveKey FileType:(NSString *)type FileName:(NSString *)name;

-(void)setFileName;
-(void)setFileName:(NSString *)name;
-(NSString *)getFileName;

-(BOOL)haveFileAtPath:(NSString *)path;
-(BOOL)createSavePath:(NSString *)path;

-(BOOL)saveData:(NSData *)data path:(NSString *)path;
@end

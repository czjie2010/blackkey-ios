//
//  SaveFile.m
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/13.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import "SaveFile.h"

@interface SaveFile()
{
    NSString        *m_fileName;
}

@end

@implementation SaveFile

-(void)setFileName
{
    NSDate *time = [NSDate date];
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc]init];
    [timeFormatter setDateFormat:@"yyyy-MM-dd-HH-mm-ss"];
    m_fileName = [timeFormatter stringFromDate:time];
}

-(void)setFileName:(NSString *)name
{
    m_fileName = name;
}

-(NSString *)getFileName{
    return m_fileName;
}

-(NSString *)getDocumentDir:(NSString *)saveKey
{
     NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
     NSString *dir = [NSString stringWithFormat:@"%@/%@",documentDir,saveKey];
    return dir;
}

-(NSString *)getSaveFilePath:(NSString *)saveKey FileType:(NSString *)type FileName:(NSString *)name
{
    NSString *documentDir = [self getDocumentDir:saveKey];
    NSString *path = [NSString stringWithFormat:@"%@/%@.%@",documentDir,name,type];
    return path;
}

-(BOOL)haveFileAtPath:(NSString *)path
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    return [fileManager fileExistsAtPath:path];
}

-(BOOL)createSavePath:(NSString *)path
{
    NSString *chatPacket =[self getDocumentDir:path];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:chatPacket])
    {
        BOOL createFlag =  [fileManager createDirectoryAtPath:chatPacket withIntermediateDirectories:YES attributes:nil error:nil];
        return createFlag;
    }
    else
    {
        return YES;
    }
}

-(BOOL)saveData:(NSData *)data path:(NSString *)path
{
    return [data writeToFile:path atomically:YES];
}

-(BOOL)saveDicKeyArray:(NSMutableDictionary *)array path:(NSString *)path
{
    return  [array writeToFile:path atomically:YES];
}

-(BOOL)saveArray:(NSMutableArray *)array path:(NSString *)path
{
    return [array writeToFile:path atomically:YES];
}

-(NSMutableDictionary *)codIng:(NSDictionary *)dic
{
    NSMutableDictionary *condingDic = [NSMutableDictionary dictionary];
    for (NSString *keys in dic.allKeys)
    {
        [condingDic setObject:[NSString stringWithFormat:@"%@",[dic objectForKey:keys]] forKey:keys];
    }
    
    return condingDic;
}

-(NSMutableDictionary *)readFromPath:(NSString *)path
{
    NSMutableDictionary *readDic = [NSMutableDictionary dictionaryWithContentsOfFile:path];
    return readDic;
}

-(NSMutableArray *)readArrayFromPath:(NSString *)path
{
    NSMutableArray *array = [NSMutableArray arrayWithContentsOfFile:path];
    return array;
}


//-(NSMutableDictionary *)readFromDocunemt:(NSString *)docuKey
//{
//    NSString *path = [self getSavePath:docuKey];
//    return [NSMutableDictionary dictionaryWithContentsOfFile:path];
//}

@end

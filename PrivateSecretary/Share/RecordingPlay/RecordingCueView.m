//
//  RecordingCueView.m
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/7.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import "RecordingCueView.h"

#define  RecordingTagFontSize 15.0f

@interface RecordingCueView(){
    UILabel         *m_recordingTag;
    UIImageView     *m_recordingImage;
    UIImageView     *m_volChangeView;
}

@end

@implementation RecordingCueView


-(instancetype)init
{
    self = [super init];
    if (self) {
        self.frame = CGRectMake(kScreenWidth/2-126/2, kScreenHeight/2-126, 126, 126);
        self.backgroundColor = [UIColor blackColor];
        self.alpha = 0.7;
        self.layer.cornerRadius = 10;
        self.layer.masksToBounds = YES;
        [self addSoundsSizeView];
    }
    return self;
}

-(void)addSoundsSizeView
{
  
    m_recordingImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"chat_voice_ tube"]];
    m_recordingImage.tag = 10004;
//    m_recordingImage.frame = CGRectMake(self.frame.size.width/2-41/2-20,self.frame.size.height/2-63/2, 41, 63);
    m_recordingImage.frame = CGRectMake(self.frame.size.width/2-41/2,self.frame.size.height/2-63/2-10, 41, 63);
    [self addSubview:m_recordingImage];
    
    /*********************
    //初始化音量声波view，就是会动的那个view，这里通过改变view的高度来显示动态声波的效果
    m_volChangeView = [[UIImageView alloc]initWithFrame:CGRectMake([m_recordingImage getViewX]+[m_recordingImage getViewWidth]+10, [m_recordingImage getViewY]+[m_recordingImage getViewHigh]-10, 20, 10)];
    m_volChangeView.backgroundColor = [UIColor yellowColor];
    m_volChangeView.tag = 10005;
    [self addSubview:m_volChangeView];
     *********************************/
    
    m_recordingTag = [[UILabel alloc]initWithFrame:CGRectMake(15, [self getViewHigh]-30, [self getViewWidth]-30, 20)];
//    m_recordingTag.text = @"手指上滑，取消发送";
    m_recordingTag.text = @"00:00";
    m_recordingTag.textAlignment = NSTextAlignmentCenter;
    m_recordingTag.textColor = [UIColor whiteColor];
    m_recordingTag.font = [UIFont systemFontOfSize:RecordingTagFontSize];
    m_recordingTag.layer.cornerRadius = 2;
    m_recordingTag.layer.masksToBounds = YES;
    [self addSubview:m_recordingTag];
    
}

-(void)setRecordingCueViewFrame:(CGRect)frame{
    self.frame = frame;
}

-(void)continueRecodingView{
//    m_recordingImage.frame = CGRectMake(self.frame.size.width/2-60/2-20,self.frame.size.height/2-60/2, 60, 60);
    m_recordingImage.frame = CGRectMake(self.frame.size.width/2-41/2,self.frame.size.height/2-63/2-10, 41, 63);
    m_volChangeView.hidden = NO;
    m_recordingTag.backgroundColor = self.backgroundColor;
//    m_recordingTag.text = @"手指上滑，取消发送";
}

-(void)cancelRecodingView{
    m_recordingImage.frame = CGRectMake(self.frame.size.width/2-41/2,self.frame.size.height/2-63/2-10, 41, 63);
    m_volChangeView.hidden = YES;
//    m_recordingTag.backgroundColor = [UIColor redColor];
//    m_recordingTag.text = @"松开手指，取消发送";
}

-(void)setRecordingLabelText:(NSString *)message
{
    m_recordingTag.text = message;
}

-(void)setRecordingTimeLabel:(int)time
{
    if (time <10) {
        m_recordingTag.text = [NSString stringWithFormat:@"00:0%d",time];
    }else{
        m_recordingTag.text = [NSString stringWithFormat:@"00:%d",time];
    }
   
}

-(void)setSelfHiddenAnimate{
    self.hidden = NO;
    [UIView animateWithDuration:0.5 animations:^{
        self.alpha = 0.0;
    } completion:^(BOOL finished) {
        self.hidden = YES;
        self.alpha = 1.0;
    }];
}

@end

//
//  RecordingPlay.m
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/3.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import "RecordingPlay.h"
#import <AVFoundation/AVFoundation.h>
#import "AppDelegate.h"
#import "Base64.h"
#import "RecordingCueView.h"
#import "MyInfor.h"
#import "amrFileCodec.h"

@interface RecordingPlay()<AVAudioPlayerDelegate>
{
    NSURL           *m_recordedFile;
    AVAudioPlayer   *m_player;
    AVAudioRecorder *m_recorder;
    NSString        *m_voicePath;
    NSTimer         *m_soundSizeTimer;
    NSTimer         *m_recordingTimer;
    int             recordingTime;
    NSString        *voiceName;
    
    RecordingCueView    *m_recordingCueView;
}
@end

@implementation RecordingPlay


-(void)prepareRecord
{
    AVAudioSession *session = [AVAudioSession sharedInstance];
    NSError *sessionError;
    //AVAudioSessionCategoryPlayback
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:&sessionError];
    
    if(session == nil)
    {
        INNLog(@"Error creating session: %@", [sessionError description]);
    }
    else
    {
        BOOL flag =    [session setActive:YES error:nil];
        INNLog(@"flag=:%d",flag);
    }

}

-(void)record
{
    [self prepareRecord];
    [self setVoiceName];
  
    
    NSString *dir = [self getVoiceDocumentDir];
    //如果文件目录不存在 创建
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:dir]) {
        BOOL createFlag =  [fileManager createDirectoryAtPath:dir withIntermediateDirectories:YES attributes:nil error:nil];
        INNLog(@"createflag = %d",createFlag);
    }
    m_voicePath = [NSString stringWithFormat: @"%@/%@.%@",dir,voiceName,@"wav"];
    
    m_recordedFile  = [NSURL fileURLWithPath:m_voicePath];
    m_recorder = [[AVAudioRecorder alloc] initWithURL:m_recordedFile settings:[self getRecordingDic] error:nil];
    m_recorder.meteringEnabled = YES;


    BOOL flag =  [m_recorder prepareToRecord];
    if (flag)
    {
        recordingTime = 0;
        [m_recorder record];
        m_recordingTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(recordingTime) userInfo:nil repeats:YES];
        if (!isHiddenRecordingCueView) {
            if (!m_recordingCueView) {
                m_recordingCueView = [[RecordingCueView alloc]init];
                [recordingView addSubview:m_recordingCueView];
            }
            m_recordingCueView .hidden = NO;
            [m_recordingCueView setRecordingTimeLabel:recordingTime];
            [m_recordingCueView continueRecodingView];
//              m_soundSizeTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(ChangeSoundSize) userInfo:nil repeats:YES];
        }
    }
    else
    {
//   TODO: 录音失败
    }
}

-(void)stopRecord
{
    [m_recorder stop];
    [m_recordingTimer invalidate];
    m_recordingTimer = nil;
    [m_soundSizeTimer invalidate];
    m_soundSizeTimer = nil;
    if (!isHiddenRecordingCueView) {
        m_recordingCueView .hidden = YES;
    }
}

-(void)play//内存
{
    if ([m_player isPlaying])
    {
        [m_player stop];
    }
    else
    {
        NSError *playerError;
        m_player = [[AVAudioPlayer alloc] initWithContentsOfURL:m_recordedFile error:&playerError];
        m_player.delegate = self;
        if (m_player == nil)
        {
            INNLog(@"ERror creating player: %@", [playerError description]);
        }
        [m_player play];
    }
}

-(void)play:(NSString *)path//内存
{
    NSString *dir = [self getVoiceDocumentDir];
    path = [NSString stringWithFormat: @"%@/%@.%@",dir,path,@"wav"];
    if ([m_player isPlaying])
    {
        [m_player stop];
    }
    else
    {
        //红外感应
        NSError *playerError;
        m_player = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:&playerError];
        m_player.delegate = self;
        if (m_player == nil)
        {
            INNLog(@"ERror creating player: %@", [playerError description]);
        }
        [m_player play];
        
        [[UIDevice currentDevice] setProximityMonitoringEnabled:YES];
        [[NSNotificationCenter defaultCenter]postNotificationName:UIDeviceProximityStateDidChangeNotification object:nil];
    }
}

-(void)stopPlay
{
    [m_player stop];
    
    if([_m_delegate respondsToSelector:@selector(audioPlayerDidFinishPlaying)])
    {
        [_m_delegate audioPlayerDidFinishPlaying];
    }
}

#pragma mark -播放网络上的声音
-(void)playWithUrl:(NSString *)soundUrl
{
    
//        soundUrl = @"http://secret-sound.b0.upaiyun.com/5139895_1427857859579.amr";
    if ([m_player isPlaying])
    {
        [m_player stop];
    }
    NSError *error = nil;
    NSData *soundData = [NSData dataWithContentsOfURL:[NSURL URLWithString:soundUrl]];
    NSData *wavData = DecodeAMRToWAVE(soundData);
    m_player = [[AVAudioPlayer alloc] initWithData:wavData error:&error];
    
    m_player.delegate = self;
    if (!error)
    {
        [m_player play];
        
        [[UIDevice currentDevice] setProximityMonitoringEnabled:YES];
        [[NSNotificationCenter defaultCenter]postNotificationName:UIDeviceProximityStateDidChangeNotification object:nil];
    }
    else
    {
        INNLog(@"播放错误=:%@",error);
//        [[NSNotificationCenter defaultCenter] postNotificationName:FinishPlay object:nil];
        
    }
    
}

#pragma -mark 记录录音时间
-(void)recordingTime
{
    ++recordingTime;
    [m_recordingCueView setRecordingTimeLabel:recordingTime];
    [self canSendRecording];
}

/*****
 设置语音时间权限
 ************/
-(BOOL)canSendRecording{
    if (recordingTime < 1) {
        [m_recordingCueView setRecordingLabelText:@"时间太短"];
        m_recordingCueView.hidden = NO;
        [self performSelector:@selector(pleaseWait) withObject:nil afterDelay:0.5];
        
        return NO;
    }else if(recordingTime > 58){
        [self stopRecord];
        if ([_m_delegate respondsToSelector:@selector(audioTimeOutAutomaticSend)]) {
            [_m_delegate audioTimeOutAutomaticSend];
            return NO;
        }
    }else{
        
    }
    return YES;
}

-(void)pleaseWait{
    [m_recordingCueView setSelfHiddenAnimate];
}

#pragma mark - get方法
-(NSString *)getPath
{
    return m_voicePath;
}

-(NSString *)getVoiceDocumentDir
{
    NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *dir = [NSString stringWithFormat:@"%@/%@",documentDir,[[MyInfor shareMyInfor]get_user_id]];
    
    return dir;
}

-(NSData *)getVoiceData
{
    
    NSData *upDataTemp = [NSData dataWithContentsOfURL:m_recordedFile];
    
//    NSData *uploadArmData =  [[AmrDataConverter shareAmrDataConverter] EncodeWAVEToAMR:upDataTemp channel:1 nBitsPerSample:16];
    return EncodeWAVEToAMR(upDataTemp ,1 ,16);
}

-(void)setVoiceName
{
    NSDate *time = [NSDate date];
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc]init];
    [timeFormatter setDateFormat:@"yyyy-MM-dd-HH-mm-ss"];
    voiceName = [timeFormatter stringFromDate:time];
}

-(NSString *)getVoiceName
{
    return voiceName;
}

-(NSString *)getRecordingTime
{
//    TODO:获取录音时间
    return [NSString stringWithFormat:@"%d\"",(int)recordingTime];
}

-(NSDictionary *)getRecordingDic
{
    // 配置音频格式达到 与android适配
    NSDictionary *  recordSetting = @{AVFormatIDKey: @(kAudioFormatLinearPCM),
                                      AVSampleRateKey: @8000.00f,
                                      AVNumberOfChannelsKey: @1,
                                      AVLinearPCMBitDepthKey: @16,
                                      AVLinearPCMIsNonInterleaved: @NO,
                                      AVLinearPCMIsFloatKey: @NO,
                                      AVLinearPCMIsBigEndianKey: @NO};
    return recordSetting;
    
}

-(BOOL)getIsPlaying{
    return  [m_player isPlaying];
}

#pragma mark - AVAudioPlayerDelegate

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    
    [self stopPlay];
//    [[UIDevice currentDevice]setProximityMonitoringEnabled:NO];
//    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    
    //播放停止后 ui方面动画停止
    if([_m_delegate respondsToSelector:@selector(audioPlayerDidFinishPlaying)])
    {
        [_m_delegate audioPlayerDidFinishPlaying];
    }
}

- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error
{
    INNLog(@"播放错误");
}

#pragma mark - 录音声音ui展示
#pragma mark -录音音量大小展示
-(void)ChangeSoundSize
{
    [m_recorder updateMeters];//刷新音量数据
    //#warning 这里本来是想要动态显示音量的声波的，但是有bug，不知道为什么初始化录音是1，录音时却是0。这个bug有待研究。
    float vol = pow(10, (0.05 * [m_recorder peakPowerForChannel:0]));
    NSLog(@"波动值：%f",vol);
    NSArray *array = m_recordingCueView.subviews;
    UIView *volView = nil;
    for (UIView *view in array) {
        if (view.tag == 10005) {
            volView = view;
        }
    }
    
    volView.frame = CGRectMake([volView getViewX], [volView getViewY], 20, 100*vol);
}

-(void)setRecordingCueViewFrame:(CGRect)frame
{
    [m_recordingCueView setRecordingCueViewFrame:frame];
}

//取消录制
-(void)cancelRecodingView
{
    [m_recordingCueView cancelRecodingView];
}

//继续录制
-(void)continueRecodingView
{
    [m_recordingCueView continueRecodingView];
}

-(void)dealloc{
    _m_delegate = nil;
}
@end

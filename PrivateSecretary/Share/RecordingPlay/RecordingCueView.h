//
//  RecordingCueView.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/7.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecordingCueView : UIView

-(void)addSoundsSizeView;
-(void)setRecordingCueViewFrame:(CGRect)frame;
-(void)cancelRecodingView;
-(void)continueRecodingView;

-(void)setRecordingTimeLabel:(int)time;
-(void)setRecordingLabelText:(NSString *)message;

-(void)setSelfHiddenAnimate;
@end


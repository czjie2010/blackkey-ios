//
//  RecordingPlay.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/3.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol RecordingPlayDelegate <NSObject>

@optional
-(void)audioPlayerDidFinishPlaying;

//超时自动发送
-(void)audioTimeOutAutomaticSend;

@end

@interface RecordingPlay : NSObject{
    @public
    UIView  *recordingView;
    BOOL    isHiddenRecordingCueView;
}

@property (nonatomic,assign)id<RecordingPlayDelegate> m_delegate;
-(void)prepareRecord;

-(void)record;

-(void)stopRecord;

-(void)play;
-(void)play:(NSString *)path;
-(void)playWithUrl:(NSString *)soundUrl;
//-(void)removeRecoding;

-(NSString *)getPath;
-(NSData *)getVoiceData;
-(NSString *)getVoiceName;
-(NSString *)getRecordingTime;
-(BOOL)getIsPlaying;

-(void)stopPlay;
/***
 刷新声音大小
 ********/
-(void)ChangeSoundSize;

-(void)setRecordingCueViewFrame:(CGRect)frame;
-(void)cancelRecodingView;
-(void)continueRecodingView;

/*****
 设置语音时间权限
 ************/
-(BOOL)canSendRecording;

@end

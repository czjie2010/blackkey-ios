//
//  UIButton+RoundButton.m
//  Secrets
//
//  Created by tiansha on 14-10-17.
//  Copyright (c) 2014年 huqinghe. All rights reserved.
//

#import "UIButton+RoundButton.h"
#import "UIButton+WebCache.h"

@implementation UIButton (RoundButton)

-(void)setRoundButton
{
    self.layer.cornerRadius = self.frame.size.height/2;
    self.layer.masksToBounds = YES;
    self.layer.borderWidth = 0;
}

-(void)setRoundButtonImage:(NSString *)imageStr
{
    if (imageStr.length)
    {
        if ([imageStr hasPrefix:@"http"]||[imageStr hasPrefix:@"https"])
        {
            [self setRoundButton];
            [self sd_setImageWithURL:[NSURL URLWithString:imageStr] forState:UIControlStateNormal placeholderImage:nil];
            
        }
        else
        {
            UIImage *tempHead =  [UIImage imageNamed:imageStr];
            if (tempHead)
            {
                
                [self setImage:[UIImage imageNamed:imageStr] forState:UIControlStateNormal];
                
            }
            else
            {
                
                [self setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
                
            }
            
            INNLog(@"imageStr=:%@",imageStr);
            
        }
    }
    else
    {
        [self setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    }
}

-(void)setButtonImage:(NSString *)imageStr
{
    if (imageStr.length)
    {
        if ([imageStr hasPrefix:@"http"]||[imageStr hasPrefix:@"https"])
        {
            [self sd_setImageWithURL:[NSURL URLWithString:imageStr] forState:UIControlStateNormal placeholderImage:nil];
            
        }
        else
        {
            UIImage *tempHead =  [UIImage imageNamed:imageStr];
            if (tempHead)
            {
                
                [self setImage:[UIImage imageNamed:imageStr] forState:UIControlStateNormal];
                
            }
            else
            {
                
                
            }
            
            INNLog(@"imageStr=:%@",imageStr);
            
        }
    }
    else
    {
    }
}

@end

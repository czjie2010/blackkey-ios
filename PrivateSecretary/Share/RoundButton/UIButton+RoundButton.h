//
//  UIButton+RoundButton.h
//  Secrets
//
//  Created by tiansha on 14-10-17.
//  Copyright (c) 2014年 huqinghe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (RoundButton)

-(void)setRoundButton;

/*****
 现实头像 本地头像或者自定义圆形头像
 *****/
-(void)setRoundButtonImage:(NSString *)imageName;

/*****
 网络图片添加夜间模式 非头像 非圆形
 ******/
-(void)setButtonImage:(NSString *)imageStr;
@end


//
//  UIImage+Scale.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/14.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Scale)

- (UIImage *)imageByScalingToMaxSize;
- (UIImage *)imageByScalingToMaxSize:(CGFloat)maxWidth;
@end

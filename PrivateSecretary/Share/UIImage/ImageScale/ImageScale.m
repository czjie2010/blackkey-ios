//
//  ImageScale.m
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/14.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import "ImageScale.h"

@interface ImageScale()

@end

@implementation ImageScale

@synthesize m_scrollView, imageView;
@synthesize m_delegate;

- (id)initWithImage:(UIImage *)image {
    self = [super init];
    
    if (self) {
        self.frame = CGRectMake(0.0, 0, kScreenWidth,kScreenHeight);
        
        m_scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0.0, 0, kScreenWidth,kScreenHeight)];
        [m_scrollView setBackgroundColor:[UIColor blackColor]];
        [m_scrollView setDelegate:self];
        [m_scrollView setShowsHorizontalScrollIndicator:NO];
        [m_scrollView setShowsVerticalScrollIndicator:NO];
        [m_scrollView setMaximumZoomScale:3.0];
        
        imageView = [[UIImageView alloc] init];
        imageView.backgroundColor = TestColor;
        INNLog(@"imageView = %@",imageView);
        CGRect rect;
        rect.size.width = image.size.width;
        rect.size.height = image.size.height;
        [imageView setImage:image];
        [imageView setFrame:rect];
    
        
        [m_scrollView setContentSize:[imageView frame].size];
        [m_scrollView setMinimumZoomScale:[m_scrollView frame].size.width / [imageView frame].size.width];
        [m_scrollView setZoomScale:[m_scrollView minimumZoomScale]];
        [m_scrollView addSubview:imageView];
        [self addSubview:m_scrollView];
        
        imageView.center = m_scrollView.center;
        
        //图片过长 则打开自动从头开始
        float y = kScreenHeight/2-[m_scrollView contentSize].height/2;
        if (y > 0) {
            imageView.frame = CGRectMake([imageView getViewX], y , [imageView getViewWidth], [imageView getViewHigh]);
        }else{
            imageView.frame = CGRectMake([imageView getViewX],0, [imageView getViewWidth], [imageView getViewHigh]);
        }
        
        UITapGestureRecognizer  *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTapGesture:)];
        [self addGestureRecognizer:tap];
    }
    
    return self;
}

-(void)handleTapGesture:(UIGestureRecognizer*)sender
{
    if ([m_delegate respondsToSelector:@selector(imageScaleDidCancel:)]) {
        [m_delegate imageScaleDidCancel:self];
    }
}


//- (void)finishCropping {
//    float zoomScale = 1.0 / [scrollView zoomScale];
//    
//    CGRect rect;
//    rect.origin.x = [scrollView contentOffset].x * zoomScale;
//    rect.origin.y = [scrollView contentOffset].y * zoomScale;
//    rect.size.width = [scrollView bounds].size.width * zoomScale;
//    rect.size.height = [scrollView bounds].size.height * zoomScale;
//    
//    CGImageRef cr = CGImageCreateWithImageInRect([[imageView image] CGImage], rect);
//    
//    UIImage *cropped = [UIImage imageWithCGImage:cr];
//    
//    CGImageRelease(cr);
//    
//    [delegate imageScale:self didFinishScalingWithImage:cropped];
//}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    float y = kScreenHeight/2-[scrollView contentSize].height/2;
    if (y > 0) {
        imageView.frame = CGRectMake([imageView getViewX], y , [imageView getViewWidth], [imageView getViewHigh]);
    }else{
        imageView.frame = CGRectMake([imageView getViewX],0, [imageView getViewWidth], [imageView getViewHigh]);
    }
    
}
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return imageView;
}

@end

//
//  ImageScale.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/14.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ImageScaleDelegate;

@interface ImageScale : UIView <UIScrollViewDelegate> {
    UIScrollView *m_scrollView;
    UIImageView *imageView;
}

@property (nonatomic, retain) UIScrollView *m_scrollView;
@property (nonatomic, retain) UIImageView *imageView;

@property (nonatomic, assign) id <ImageScaleDelegate> m_delegate;

- (id)initWithImage:(UIImage *)image;

@end


@protocol ImageScaleDelegate <NSObject>

- (void)imageScale:(ImageScale *)cropper didFinishScalingWithImage:(UIImage *)image;
- (void)imageScaleDidCancel:(ImageScale *)cropper;
@end
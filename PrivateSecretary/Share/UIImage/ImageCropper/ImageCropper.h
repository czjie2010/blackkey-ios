//
//  ImageCropper.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/14.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ImageCropperDelegate;

@interface ImageCropper : UIView <UIScrollViewDelegate> {
	UIScrollView *scrollView;
	UIImageView *imageView;
    id<ImageCropperDelegate> m_delegate;
}

@property (nonatomic, retain) UIScrollView *scrollView;
@property (nonatomic, retain) UIImageView *imageView;

@property (nonatomic, assign) id <ImageCropperDelegate> delegate;

- (id)initWithImage:(UIImage *)image;

@end

@protocol ImageCropperDelegate <NSObject>
- (void)imageCropper:(ImageCropper *)cropper didFinishCroppingWithImage:(UIImage *)image;
- (void)imageCropperDidCancel:(ImageCropper *)cropper;
@end
//
//  ColorDefine.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/7/30.
//  Copyright (c) 2015年 田沙. All rights reserved.
//


#define TestColor    [UIColor redColor]

#define RGBA(r,g,b,a)   [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]

#define VCBackgroundColor     RGBA(244,244,244,1)


/**
 *  应用锁
 */
#define  LockAppNomal                    RGBA(250,250,250,1)
#define  LockButtonTextColor            RGBA(56, 56, 56, 1)
#define  LockButtonSelectedTextColor    RGBA(56,56,56,1)

//身份验证
#define     UserIdentityEorrTagColor    RGBA(241,56,56,1)



/*************
 *
 *  订单
 **********************/

//订单 等待支付按钮字体颜色
#define  WaitingOrderTextColor                    RGBA(253,253,253,1)
//订单 完成支付按钮字体颜色
#define  FinishOrderTextColor                    RGBA(56,56,56,1)
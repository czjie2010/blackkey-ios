//
//  FrameDefine.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/7/30.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#ifndef PrivateSecretary_FrameDefine_h
#define PrivateSecretary_FrameDefine_h

#define kScreenWidth        [[UIScreen mainScreen]bounds].size.width
#define kScreenHeight       [[UIScreen mainScreen]bounds].size.height

#endif

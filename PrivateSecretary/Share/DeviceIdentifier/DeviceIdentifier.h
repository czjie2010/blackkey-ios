//
//  DeviceIdentifier.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/26.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DeviceIdentifier : NSObject

/******
 设备uuid 信息
 **********/
+(DeviceIdentifier *) sharedDeviceInfo;
-(NSString *)theNewUUID;
-(NSString *)getDeviceID;
-(NSInteger)iOSVersion;

/********
 获取设备 user-agent
 *****************/
- (NSString *)getDeviceUserAgent;

- (NSString *)getLocationVersion;

@end

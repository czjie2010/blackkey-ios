//
//  DeviceIdentifier.m
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/26.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import "DeviceIdentifier.h"
#import "SSKeychain.h"
#import "UIDevice+PL.h"
#import "URLProvider.h"

#define SECRET_APP_DEVICEID_SERVICE @"com.inno.secret.deviceid.service"
#define SECRET_APP_DEVICEID_ACCOUNT @"com.inno.secret.deviceid.account"

@implementation DeviceIdentifier

+(DeviceIdentifier *) sharedDeviceInfo
{
    static DeviceIdentifier *_sharedDeviceInfo = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedDeviceInfo = [[[self class] alloc] init];
    });
    return _sharedDeviceInfo;
}

- (NSString*)theNewUUID
{
    NSError* error;
    NSString* savedDeviceID = [SSKeychain passwordForService:SECRET_APP_DEVICEID_SERVICE
                                                     account:SECRET_APP_DEVICEID_ACCOUNT
                                                       error:&error];
    if (savedDeviceID)
    {
        //NSLog(@"savedDeviceID:%@",savedDeviceID);
        //        savedDeviceID = @"ios_257DDDBB99QEC2DD4FFE43W9D8EAWWA94#ios_BQ589DD0-94WW-48E1-9B7A-C4B218EBAJ99";
        return savedDeviceID;
    }
    else
    {
        INNLog(@"can not fetch device id in keychain with error : %@",[error localizedDescription]);
        NSString* deviceID = [self getDeviceID];
        if (! [SSKeychain setPassword:deviceID
                           forService:SECRET_APP_DEVICEID_SERVICE
                              account:SECRET_APP_DEVICEID_ACCOUNT
                                error:&error]) {
            INNLog(@"failed to save device id in keychain with error : %@", [error localizedDescription]);
        }
        INNLog(@"deviceID: %@",deviceID);
        return deviceID;
    }
}

- (NSString*)getDeviceID
{
    static NSString* result;
    //    static dispatch_once_t onceToken;
    //    dispatch_once(&onceToken, ^{
    NSString* venderID;
    NSInteger sysVersion = [self iOSVersion];
    if (sysVersion < 6) {
        result = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
    }else if (sysVersion == 6){
        venderID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        result = [NSString stringWithFormat:@"%@#ios_%@",[[UIDevice currentDevice] uniqueGlobalDeviceIdentifier],venderID];
    }else{
        venderID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        result = venderID;
    }
    result = [NSString stringWithFormat:@"ios_%@",result];
    //    });
    //    result = @"ios_C8852356E6EC2213EEE43F5Q3EE05B94#ios_C4E57D10-E7BD-41C4-BD6C-DE4B198E378H";
    //     NSLog(@"result = %@",result);
    return result;
}

- (NSInteger)iOSVersion
{
    static NSInteger version = 0;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSArray* numbers = [[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."];
        version = [numbers[0] integerValue];
    });
    return version;
}

/********
 获取设备 user-agent
 *****************/
- (NSString *)getDeviceUserAgent
{
    NSString *userAgent = nil;
#if defined(__IPHONE_OS_VERSION_MIN_REQUIRED)
    userAgent = [NSString stringWithFormat:@"%@/%@ (%@; iOS %@; Scale/%0.2f)", [[[NSBundle mainBundle] infoDictionary] objectForKey:(__bridge NSString *)kCFBundleExecutableKey] ?: [[[NSBundle mainBundle] infoDictionary] objectForKey:(__bridge NSString *)kCFBundleIdentifierKey], (__bridge id)CFBundleGetValueForInfoDictionaryKey(CFBundleGetMainBundle(), kCFBundleVersionKey) ?: [[[NSBundle mainBundle] infoDictionary] objectForKey:(__bridge NSString *)kCFBundleVersionKey], [[UIDevice currentDevice] model], [[UIDevice currentDevice] systemVersion], [[UIScreen mainScreen] scale]];
#elif defined(__MAC_OS_X_VERSION_MIN_REQUIRED)
    userAgent = [NSString stringWithFormat:@"%@/%@ (Mac OS X %@)", [[[NSBundle mainBundle] infoDictionary] objectForKey:(__bridge NSString *)kCFBundleExecutableKey] ?: [[[NSBundle mainBundle] infoDictionary] objectForKey:(__bridge NSString *)kCFBundleIdentifierKey], [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] ?: [[[NSBundle mainBundle] infoDictionary] objectForKey:(__bridge NSString *)kCFBundleVersionKey], [[NSProcessInfo processInfo] operatingSystemVersionString]];
#endif
#pragma clang diagnostic pop
    if (userAgent) {
        if (![userAgent canBeConvertedToEncoding:NSASCIIStringEncoding]) {
            NSMutableString *mutableUserAgent = [userAgent mutableCopy];
            if (CFStringTransform((__bridge CFMutableStringRef)(mutableUserAgent), NULL, (__bridge CFStringRef)@"Any-Latin; Latin-ASCII; [:^ASCII:] Remove", false)) {
                userAgent = mutableUserAgent;
            }
        }
    }
    return userAgent;
}

-(NSString *)getLocationVersion
{
    return S_LocationVersionVersion;
}

@end

//
//  KeyDefine.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/20.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#ifndef PrivateSecretary_KeyDefine_h
#define PrivateSecretary_KeyDefine_h


/*******************
 *  聊天数据字段存储
 *********************/
#define  ReadStatus    @"read_status"


/*******************
 *  UserDefault key
 *********************/
#define LockKey          @"LockKey"
#define UserInfo         @"UserInfo"

//定位
#define     BaiDuLongitude          @"BaiDuLongitude"
#define     BaiDuLatitude           @"BaiDuLatitude"
#define     BaiDuProvince           @"BaiDuProvince"
#define     BaiDuCity               @"BaiDuCity"
#define     BaiDuAdress             @"BaiDuAdress"

#define     RefreshUserLocationInfor      @"RefreshUserLocationInfor"

/*******************
 *  通知 key
 *********************/
#define     NewUser         @"NewUser"

#endif

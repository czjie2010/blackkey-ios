//
//  URLProvider.m
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/18.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import "URLProvider.h"

@implementation URLProvider


/**
 *  发送消息
 *  /message/send/
 **/
+(NSString *)getsend_chat_message
{
    NSString *url= @"/message/api/send/";
    return [NSString stringWithFormat:@"%@%@",hostAdress,url];
}

+(NSString *)get_signin_url
{
    NSString *url= @"/member/api/signin/";
    return [NSString stringWithFormat:@"%@%@",hostAdress,url];
}

///member/api/activation_verification_code/
+(NSString *)get_activation_verification_code_url
{
    NSString *url= @"/member/api/activation_verification_code/";
    return [NSString stringWithFormat:@"%@%@",hostAdress,url];
}


//推送设备号 /member/api/set_push_token/
+(NSString *) get_pushToken_url:(NSString *)token
{
    NSString *url= @"/member/api/set_push_token/";
    return [NSString stringWithFormat:@"%@%@",hostAdress,url];
}

///message/read_messages/
+(NSString *) get_read_messages:(NSString *)mid
{
    NSString *url= @"/message/api/read_messages/";
    return [NSString stringWithFormat:@"%@%@?mid=%@",hostAdress,url,mid];
}

// /member/api/set_member_lbs/ {user_id} /
+(NSString *) get_member_lbs:(NSString *)uid
{
    NSString *url= @"/member/api/set_member_lbs/";
    return [NSString stringWithFormat:@"%@%@%@/",hostAdress,url,uid];
}

@end

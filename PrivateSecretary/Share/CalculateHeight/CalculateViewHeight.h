//
//  CalculateViewHeight.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/5.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CalculateViewHeight : NSObject

+(CGSize)striSizeHeight:(NSString *)astr
               withFont:(UIFont *)afont maxWith:(int)amaxWidth;

@end

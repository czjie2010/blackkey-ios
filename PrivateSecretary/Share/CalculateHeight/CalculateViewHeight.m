//
//  CalculateViewHeight.m
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/5.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import "CalculateViewHeight.h"

@implementation CalculateViewHeight

+(CGSize)striSizeHeight:(NSString *)astr
              withFont:(UIFont *)afont maxWith:(int)amaxWidth
{
    CGSize size = CGSizeMake(amaxWidth, 10000);
    CGSize sizeForFeature  = [astr boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:afont} context:nil].size;
    return sizeForFeature;
    
}

@end

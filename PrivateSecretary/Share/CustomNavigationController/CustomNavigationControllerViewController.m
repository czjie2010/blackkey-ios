//
//  CustomNavigationControllerViewController.m
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/6.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import "CustomNavigationControllerViewController.h"

#define NaviFont [UIFont systemFontOfSize:19]
#define NaviTextColor RGBA(250,250,250,1)

@interface CustomNavigationControllerViewController (){
}

@end

@implementation CustomNavigationControllerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSMutableDictionary *navigationAttributes = [[NSMutableDictionary alloc] init];
    [navigationAttributes setObject:NaviFont forKey:NSFontAttributeName];
    [navigationAttributes setObject:NaviTextColor forKey:NSForegroundColorAttributeName];
    self.navigationBar.translucent = NO;
    [self.navigationBar setTitleTextAttributes:navigationAttributes];
//    navgationbar_bg
//    [self.navigationBar setBackgroundImage:[UIImage imageNamed:@""] forBarMetrics:UIBarMetricsDefault];
//    [self.navigationBar setShadowImage:[[UIImage alloc]init]];
    [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    [self.navigationBar setBarTintColor:VCBackgroundColor];
}

@end

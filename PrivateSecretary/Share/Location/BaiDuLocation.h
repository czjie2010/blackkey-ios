//
//  BaiDuLocation.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/31.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaiDuLocation : UIView

+(BaiDuLocation *)shareBaiDuLocation;


-(void)reLocation;

-(NSString *)getLocation;
-(void)setLocation:(NSString *)location;
-(NSString *)getCity;
-(NSString *)getProvince;
-(NSString *)getDistrict;
-(NSString *)getAddress;

-(NSString *)getLatitude;
-(NSString *)getLongitude;


@end

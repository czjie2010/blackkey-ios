//
//  BaiDuLocation.m
//  PrivateSecretary
//
//  Created by 田沙 on 15/8/31.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import "BaiDuLocation.h"
#import <BaiduMapAPI/BMapKit.h>
#import <BaiduMapAPI/BMKLocationService.h>
#import "NSString+NULL.h"



@interface BaiDuLocation()<BMKLocationServiceDelegate,BMKGeneralDelegate,BMKGeoCodeSearchDelegate>
{
    
    NSMutableString *s_location;    //指定为到城市 或者区
    NSMutableString *s_city;
    NSMutableString *s_province;
    NSMutableString *s_district;
    NSMutableString *s_address;     //详细地址
    
    NSMutableString *s_latitude;
    NSMutableString *s_longitude;
    CLLocationManager * locationManager;
    
    
    BMKLocationService *s_locationServiece;//定位
    
    BMKMapManager *s_mapManager;//启动百度地图
    
    BMKGeoCodeSearch *s_geocodesearch;//反向定位
    
    
}
-(void)deleteMutableStringChar:(NSMutableString *)muString;
-(void)saveLatitudeLongitude:(NSString *)aVlaue key:(NSString *)aKey;
-(void)setObj:(NSString *)obj key:(NSString *)aKey;
-(NSString *)getObjFromUd:(NSString *)akey;

@end


@implementation BaiDuLocation


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // Initialization code
    }
    return self;
}

+(BaiDuLocation *)shareBaiDuLocation
{
    
    static BaiDuLocation *_shareBaiDuLocation = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        INNLog(@"only one");
        _shareBaiDuLocation = [[self alloc] init];
        
        _shareBaiDuLocation->s_location = [[NSMutableString alloc] initWithString:@""];
        _shareBaiDuLocation->s_city = [[NSMutableString alloc] initWithString:@""];
        _shareBaiDuLocation->s_province = [[NSMutableString alloc] initWithString:@""];
        _shareBaiDuLocation->s_district = [[NSMutableString alloc] initWithString:@""];
        _shareBaiDuLocation->s_address = [[NSMutableString alloc] initWithString:@""];
        
        _shareBaiDuLocation->s_longitude = [[NSMutableString alloc] initWithString:@"0"];
        _shareBaiDuLocation->s_latitude =[[NSMutableString alloc] initWithString:@"0"];
        
    });
    return _shareBaiDuLocation;
}


-(void)reLocation
{
    /////////////////////百度地图
    if (!s_mapManager)
    {
        s_mapManager = [[BMKMapManager alloc]init];
    }
    BOOL ret = [s_mapManager start:BAIDUMAMPKEY generalDelegate:self];
    if (ret)
    {
        if (!s_locationServiece)
        {
            s_locationServiece = [[BMKLocationService alloc]init];
            s_locationServiece.delegate = self;
        }
        [s_locationServiece startUserLocationService];
        NSLog(@"mangae start success ");
    }
    else
    {
        NSLog(@"manager start failed!");
    }
    [self prepareProperty];
    /////////////////百度地图
}


////////////////////////////////////百度地图
/**
 *返回网络错误
 *@param iError 错误号
 */
- (void)onGetNetworkState:(int)iError
{
    INNLog(@"NetIError=:%d",iError);
}

/**
 *返回授权验证错误
 *@param iError 错误号 : 为0时验证通过，具体参加BMKPermissionCheckResultCode
 */
- (void)onGetPermissionState:(int)iError
{
    INNLog(@"PermissinIError=:%d",iError);
    
}


- (void)didUpdateBMKUserLocation:(BMKUserLocation *)userLocation
{
    
    INNLog(@"%f",userLocation.location.coordinate.latitude);
    INNLog(@"%f",userLocation.location.coordinate.longitude);
    double latitudeTemp = userLocation.location.coordinate.latitude;
    double longitudeTemp =userLocation.location.coordinate.longitude;
    @try {
        [s_locationServiece stopUserLocationService];
        [self seaveLatitudeLongitude:userLocation.location.coordinate];
        CLLocationCoordinate2D pt;
        pt.latitude = latitudeTemp;
        pt.longitude = longitudeTemp;
        
        BMKReverseGeoCodeOption *reverseGeocodeSearchOption = [[BMKReverseGeoCodeOption alloc]init];
        reverseGeocodeSearchOption.reverseGeoPoint = pt;
        
        if (!s_geocodesearch)
        {
            s_geocodesearch = [[BMKGeoCodeSearch alloc]init];
            s_geocodesearch.delegate = self;
            
        }
        
        BOOL flag = [s_geocodesearch reverseGeoCode:reverseGeocodeSearchOption];
        if(flag)
        {
            NSLog(@"反geo检索发送成功");
        }
        else
        {
            NSLog(@"反geo检索发送失败");
        }
        
    }
    @catch (NSException *exception) {
        INNLog(@"定位异常");
    }
    
}

-(void) onGetReverseGeoCodeResult:(BMKGeoCodeSearch *)searcher result:(BMKReverseGeoCodeResult *)result errorCode:(BMKSearchErrorCode)error
{
    switch (error)
    {
        case BMK_SEARCH_NO_ERROR:
        {
            [self prepareProperty];
            INNLog(@"Provice=:%@",result.addressDetail.province);
            INNLog(@"City=;%@",result.addressDetail.city);
            INNLog(@"StreetName=:%@",result.addressDetail.streetName);
            INNLog(@"StreetName=:%@",result.addressDetail.streetNumber);
            INNLog(@"District=:%@",result.addressDetail.district);
            INNLog(@"Address=:%@",result.address); //北京市海淀区海淀大街3
            [s_province appendString:result.addressDetail.province];
            [s_city appendString:result.addressDetail.city];
            
            [s_address appendString:result.address];
            
            if ([s_province isEqualToString:s_city])
            {
                
                [s_location appendFormat:@"%@%@",result.addressDetail.city,result.addressDetail.district];
                [self deleteMutableStringChar:s_city];
                
            }
            else
            {
                [s_location appendFormat:@"%@%@",s_province,s_city];
            }
            
            //            if ([s_province isEqualToString:s_city])
            //            {
            //                [self deleteMutableStringChar:s_province];
            //            }
            
            [self setObj:s_city key:BaiDuCity];
            [self setObj:s_province key:BaiDuProvince];
            [self setObj:result.address key:BaiDuAdress];
            
            [s_district appendString:result.addressDetail.district];
            
            [[NSNotificationCenter defaultCenter]postNotificationName:RefreshUserLocationInfor object:nil];
        }
            break;
            
        default:
        {
            [s_location appendFormat:@"定位失败"];
        }
            break;
    }
}

- (void)onGetGeoCodeResult:(BMKGeoCodeSearch *)searcher result:(BMKGeoCodeResult *)result errorCode:(BMKSearchErrorCode)error
{
    INNLog(@"error = %d",error);
}

///////////////////////////////百度地图

-(NSString *)getLocation
{
    return s_location;
}

-(void)setLocation:(NSString *)location{
    s_location.string = location;
}

-(NSString *)getCity
{
    
    return [self getObjFromUd:BaiDuCity];//s_city;
    
}

-(NSString *)getProvince
{
    return [self getObjFromUd:BaiDuProvince];
}

-(NSString *)getDistrict
{
    if ([s_district isKindOfClass:[NSString class]]&&(s_district.length>0))
    {
        return s_district;
    }
    else
    {
        return nil;
    }
}

-(NSString *)getAddress
{
    return [self getObjFromUd:BaiDuAdress];
}


-(NSString *)getLatitude
{
    if (s_latitude==nil||s_latitude.length==0)
    {
        return @"";
    }
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSString *latitudeTemp = [ud objectForKey:BaiDuLatitude];
    
    if (latitudeTemp.intValue!=0)
    {
        return latitudeTemp;
    }
    
    return s_latitude;
}

-(NSString *)getLongitude
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSString *longitudeTemp = [ud objectForKey:BaiDuLongitude];
    if (longitudeTemp!=nil)
    {
        return longitudeTemp;
    }
    if (s_longitude==nil||s_longitude.length==0)
    {
        return @"";
    }
    return s_longitude;
}

-(void)deleteMutableStringChar:(NSMutableString *)muString
{
    [muString deleteCharactersInRange:NSMakeRange(0, muString.length)];
    
}

-(void)saveLatitudeLongitude:(NSString *)aVlaue key:(NSString *)aKey
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:aVlaue forKey:aKey];
    [ud synchronize];
}

-(void)setObj:(NSString *)obj key:(NSString *)aKey
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:obj forKey:aKey];
    [ud synchronize];
}

-(NSString *)getObjFromUd:(NSString *)akey
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSString *flag = [ud objectForKey:akey];
    unsigned long length = [[flag NSStringNull] length];
    if (length)
    {
        return [ud objectForKey:akey];
    }
    else
    {
        return nil;
    }
    
}

-(void)prepareProperty//初始化属性
{
    
    
    if (!s_location)
    {
        s_location = [[NSMutableString alloc] init];
    }
    [self deleteMutableStringChar:s_location];
    if (!s_city)
    {
        s_city = [[NSMutableString alloc] initWithCapacity:0];
    }
    [self deleteMutableStringChar:s_city];
    
    if (!s_province)
    {
        s_province = [[NSMutableString alloc] initWithCapacity:0];
    }
    [self deleteMutableStringChar:s_province];
    
    if (!s_district)
    {
        s_district = [[NSMutableString alloc] initWithCapacity:0];
    }
    [self deleteMutableStringChar:s_district];
    
    if (!s_address)
    {
        s_address = [[NSMutableString alloc] initWithCapacity:0];
    }
    [self deleteMutableStringChar:s_address];
    
    
}

-(void)seaveLatitudeLongitude:(CLLocationCoordinate2D)coordinate//存储经纬度
{
    
    if (!s_latitude)
    {
        s_latitude = [[NSMutableString alloc] initWithCapacity:0];
    }
    [self deleteMutableStringChar:s_latitude];
    
    
    if (!s_longitude)
    {
        s_longitude = [[NSMutableString alloc] initWithCapacity:0];
    }
    
    [self deleteMutableStringChar:s_longitude];
    
    
    
    [self deleteMutableStringChar:s_latitude];
    [s_latitude appendFormat:@"%10.16f",coordinate.latitude];
    NSString *tmp = [[NSString alloc] initWithFormat:@"%f",coordinate.latitude];
    INNLog(@"temp=:%@",tmp);
    
    [self deleteMutableStringChar:s_longitude];
    [s_longitude appendFormat:@"%10.16f",coordinate.longitude];
    
    
    [self saveLatitudeLongitude:s_latitude key:BaiDuLatitude];
    [self saveLatitudeLongitude:s_longitude key:BaiDuLongitude];
    
    
}




-(void)dealloc
{
    s_locationServiece.delegate = nil;
    s_locationServiece = nil;
    s_mapManager = nil;
    s_geocodesearch.delegate =nil;
    locationManager = nil;
}

@end

//
//  CustomKeyBoardAnimation.h
//  PrivateSecretary
//
//  Created by 田沙 on 15/7/30.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol CustomKeyBoardAnimationDelegate <NSObject>

-(void)keyboardWillShowViewFrame:(float)keyboardHeight;
-(void)keyboardWillHiddenViewFrame:(float)keyboardHeight;

@end

@interface CustomKeyBoardAnimation : NSObject
{
    @public
    UIView  *showKeyBoardView;
}

@property(nonatomic, assign)id<CustomKeyBoardAnimationDelegate>m_delegate;

@end

//
//  CustomKeyBoardAnimation.m
//  PrivateSecretary
//
//  Created by 田沙 on 15/7/30.
//  Copyright (c) 2015年 田沙. All rights reserved.
//

#import "CustomKeyBoardAnimation.h"

@implementation CustomKeyBoardAnimation

-(id)init{
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    }
    return self;
}

#pragma mark - keyboard notification

- (void)keyboardWillShow:(NSNotification *)notification
{
    NSDictionary *userInfo = notification.userInfo;
    NSNumber *durationValue = userInfo[UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration = durationValue.doubleValue;
    
    NSNumber *curveValue = userInfo[UIKeyboardAnimationCurveUserInfoKey];
    UIViewAnimationCurve animationCurve = curveValue.intValue;
    
    //
    // Create animation.
    
    CGFloat height = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    //
//    CGRect replyViewFrame = showKeyBoardView.frame;
    
    //移动动画
    {
        [UIView beginAnimations:@"CommentUp" context:nil];
        [UIView setAnimationDuration: animationDuration];
        [UIView setAnimationCurve:animationCurve];

        if ([_m_delegate respondsToSelector:@selector(keyboardWillShowViewFrame:)]) {
            [_m_delegate keyboardWillShowViewFrame:height];
        }
        
        
        [UIView commitAnimations];
    }
}

- (void)keyboardWillHide:(NSNotification *)notification {
    
    NSDictionary *userInfo = notification.userInfo;
    NSNumber *durationValue = userInfo[UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration = durationValue.doubleValue;
    
    NSNumber *curveValue = userInfo[UIKeyboardAnimationCurveUserInfoKey];
    UIViewAnimationCurve animationCurve = curveValue.intValue;
    
    CGFloat height = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    [UIView beginAnimations:@"CommentDown" context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    
//    showKeyBoardView.frame = CGRectMake(0, kScreenHeight-showKeyBoardView.frame.size.height, showKeyBoardView.frame.size.width, showKeyBoardView.frame.size.height);
//    showKeyBoardView.frame = CGRectMake(0, showKeyBoardView.frame.origin.y, showKeyBoardView.frame.size.width, [showKeyBoardView getViewHigh]+height);
    
    if ([_m_delegate respondsToSelector:@selector(keyboardWillHiddenViewFrame:)]) {
        [_m_delegate keyboardWillHiddenViewFrame:height];
    }
    
    [UIView commitAnimations];
    
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    showKeyBoardView = nil;
    _m_delegate = nil;
}

@end

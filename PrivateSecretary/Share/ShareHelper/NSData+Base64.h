//
//  NSData+Base64.h
//  ZAE
//
//  Created by C YH on 11-5-3.
//  Copyright 2011 ZOL. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSData (Base64)

- (NSString *) base64EncodedString;

@end
